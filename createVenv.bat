@echo off 
:: To create a venv folder, you must be in the target repository folder and run the following command:
virtualenv venv
::linux: python3 -m venv env

:: REM Activate the venv right after its creation
call venv\scripts\activate
::linux: source venv/bin/activate
