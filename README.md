### CryptoKarMa ###

Personal Cryptocurrency portfolio manager.
Offers overview of your portfolio in real time, history data like transaction history, calculation of return of investment for each coin related to USD, BTC, CZK or EUR.
Data are aquired with API requests from available exchanges or generated csv files from wallets.
Currently supported exchanges and wallets:
 - Bitfinex
 - Coinbase
 - Coinbase pro
 - Binance
 - Bittrex
 - Coinmate
 - Kucoin
 - Ledger (csv)
 - Trezor (csv)
 - Crypto.com (csv)

### Installation (Windows) ###

*You can skip the first 3 steps if you are using executable version*
 1. Clone or download the repository
 2. Create python virtualenvironment
		 a) From cmd, run: 	`pip install virtualenv`
	     b) run  `/CryptoKarMa/createVenv.bat`
 3. Install python packages from requirements.txt (venv must be activated)
		a) `pip install -r requirements.txt`
		b) Find and install "Twisted" package from the web (installation fails through pip on windows) See: https://stackoverflow.com/a/62276435 for details
4. Create folders "csv" and "safefiles" in `/CryptoKarMa/user/`
	a) Add generated csv files you have (for example from Ledger or Trezor) in csv folder. Keep the original file name for automatic processing by CryptoKarMa.
	b) safefiles folder is filled automatically after running CryptoKarMa
5. Create file apiKeys.json and fill it API keys from your exchanges (use apiKeysTemplate.json as template)
		a) Go to cryptocompare.com and generate an API key there. This is the only obligatory Key that must be inserted because all general public values (like prices, daily changes etc.) are loaded from here.
		b) You can delete exchanges/wallets that you will not be using from this file

6. Run CryptoKarMa, Let it do its initial bussiness.
7. Add coins to your portfolio with plus button on the left side of MyPortfolio tab (You can see all coins with non 0 balances from wallets in the log)
8. Don´t forget to save

## Executable file

To create CryptoKarMa executable file (.exe for windows) run `/CryptoKarMa/createExe.bat`