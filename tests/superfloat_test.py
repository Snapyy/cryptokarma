from com.common import superfloat
import unittest


class SuperfloatTest(unittest.TestCase):
    known_values = (
        ("1.1", 1.1),
        (".1", 0.1),
        ("1", 1.0),
        ("1.", 1.0),
        ("-1.1", -1.1),
        ("+1.1", 1.1),
        ("1.1 XZY", 1.1),
        ("1.1s", 1.1),
        ("29.57 Kč", 29.57),
        ("29. Kč", 29.0),
        ("xsxf sd + 29.57 Kč", 29.57),
        ("+29.57 Kč", 29.57),
        ("- 29.57 Kč", -29.57),
        ("+-29.57 Kč", -29.57),
        ("- 294646616.57 Kč surprise madafaka", -294646616.57),
        ("2e+3 Kč", 2000),
        ("2e3 Kč", 2000),
        ("- 294 646 616.57 Kč surprise madafaka", -294646616.57),
    )

    def test_superfloat_known_values(self):
        """ superfloat must return known results with known input """
        for input, output in self.known_values:
            result = superfloat(input)
            self.assertEqual(output, result)


# if this file is run - run unittest
if __name__ == "__main__":
    unittest.main()
