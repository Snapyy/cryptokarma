import requests
import json
import hashlib
import hmac

from datetime import datetime
from com.common import LoadingError


class BitfinexClient(object):
    AUTH_URL = "https://api.bitfinex.com/"
    PUBL_URL = "https://api-pub.bitfinex.com/"

    BITFINEX_STUPID_NAMING = {
        "BAB": "BCH",
        "BCHN": "BCH",
        "ATO": "ATOM",
        "ALG": "ALGO",
        "UST": "USDT",
    }

    def __init__(self, KEY, SECRET):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET

    def _nonce(self):
        """
        Returns a nonce
        Used in authentication
        """
        return str(int(round(datetime.now().timestamp() * 1000)))

    def _headers(self, path, nonce, body):

        signature = "/api/" + path + nonce + body
        h = hmac.new(self.SECRET.encode("utf8"), signature.encode("utf8"), hashlib.sha384)
        signature = h.hexdigest()

        return {
            "bfx-nonce": nonce,
            "bfx-apikey": self.KEY,
            "bfx-signature": signature,
            "content-type": "application/json"
        }

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        nonce = self._nonce()
        path = "v2/auth/r/wallets"
        body = {}
        rawBody = json.dumps(body)
        # prepare request headers
        headers = self._headers(path, nonce, rawBody)

        # send the post request
        r = requests.post(self.AUTH_URL + path, headers=headers, data=rawBody, verify=True)

        # if request response is OK (200)
        if r.status_code == 200:
            results = r.json()
            # data loaded, return non 0 balances
            balances = {}
            for wallet in results:
                # change BAB or BCHN to BCH
                if wallet[1] in self.BITFINEX_STUPID_NAMING:
                    wallet[1] = self.BITFINEX_STUPID_NAMING[wallet[1]]

                balances[wallet[1]] = wallet[2]
            return balances
        else:
            raise LoadingError

    def loadLedgers(self, timestampFrom):
        """ load all ledgers (~ transactions) """
        nonce = self._nonce()
        path = "v2/auth/r/ledgers/hist"
        body = {}
        body["start"] = str(timestampFrom)
        body["end"] = int(datetime.now().timestamp() * 1000)
        body["limit"] = "2500"
        rawBody = json.dumps(body)
        # prepare request headers
        headers = self._headers(path, nonce, rawBody)

        # send the post request
        r = requests.post(self.AUTH_URL + path, headers=headers, data=rawBody, verify=True)

        if r.status_code == 200:
            results = r.json()
        else:
            results = None
            body["end"] = None

        return results, body["end"]

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions from the specified timestamp
            args:
                timestampFrom (int) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int) - timestamp of the time the transactions were synced
        """
        # format of aquired ledgers:
        # [
        #    0 - ID (not useful),
        #    1 - CURRENCY (string),
        #    2 - PLACEHOLDER (always None),
        #    3 - MTS, (ms timestamp)
        #    4 - PLACEHOLDER (always None),
        #    5 - AMOUNT (float),
        #    6 - BALANCE (float),
        #    7 - PLACEHOLDER (always None),
        #    8 - DESCRIPTION (string)
        # ],
        # ...
        ledgers, timestampSync = self.loadLedgers(timestampFrom)

        newTransactions = {}
        # go through all loaded transactions
        for transaction in ledgers:
            # create member for this timestamp if there already isn't one
            if transaction[3] not in newTransactions:
                newTransactions[transaction[3]] = []
            # transfers are between bitfinex wallet and are not CryptoKarma transactions
            if "Transfer" not in transaction[8]:
                # change BAB or BCHN to BCH and more
                if transaction[1] in self.BITFINEX_STUPID_NAMING:
                    transaction[1] = self.BITFINEX_STUPID_NAMING[transaction[1]]

                newTransactions[transaction[3]].append({
                    "coin": transaction[1],
                    "amount": transaction[5],
                })

                if "Staking" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "STAKING"
                elif "Trading fees" in transaction[8] or "Withdrawal fee" in transaction[8] or "Deposit Fee" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "FEE"
                elif "Exchange" in transaction[8] and "@" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "BUY/SELL"
                elif "Deposit" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "DEPOSIT"
                elif "Withdrawal" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "WITHDRAWAL"
                elif "airdrop" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "REWARD"
                elif "fork credit" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "REWARD"
                elif "fork clear" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "FEE"
                elif "Settlement" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "REWARD"
                elif "Delist" in transaction[8]:
                    newTransactions[transaction[3]][-1]["operation"] = "LOSS"
                else:
                    print("ERROR!!!: Bitfinex not supported transaction type loaded: {}".format(transaction[8]))

                # add opposite amount and coin for buy/sell transactions
                if newTransactions[transaction[3]][-1]["operation"] == "BUY/SELL":
                    # ['Exchange', '2.240229', 'LTC', 'for', 'BTC', '@', '0.003811', 'on', 'wallet', 'exchange']
                    description = transaction[8].split(" ")
                    # if coin equals the coin listed first in description
                    if transaction[1] == description[2]:
                        # add the second coin from description as opposite coin for this transaction
                        if description[4] in self.BITFINEX_STUPID_NAMING:
                            description[4] = self.BITFINEX_STUPID_NAMING[description[4]]
                        newTransactions[transaction[3]][-1]["coinOppo"] = description[4]
                        # add price * amount as opposite coin amount
                        newTransactions[transaction[3]][-1]["amountOppo"] = -float(description[6]) * transaction[5]
                    # if original coin is the on listed as second in description
                    else:
                        # add the first coin from description as opposite coin for this transaction
                        if description[2] in self.BITFINEX_STUPID_NAMING:
                            description[2] = self.BITFINEX_STUPID_NAMING[description[2]]
                        newTransactions[transaction[3]][-1]["coinOppo"] = description[2]
                        # add price * amount as opposite coin amount
                        newTransactions[transaction[3]][-1]["amountOppo"] = -transaction[5] / float(description[6])

        # erase all emty timestamps (result of ignoring bitfinex transfers)
        newTransactions = {k: v for (k, v) in newTransactions.items() if v != []}

        return newTransactions, timestampSync

    def getOpenOrders(self):
        """ gets and return all open orders with additional data (prices data for the used coinPairs)
            returns:
                openOrders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                              price(float), type(str), currentPrice(float), high(float), low(float)}]
        """
        nonce = self._nonce()
        path = "v2/auth/r/orders/"
        body = {}
        rawBody = json.dumps(body)
        headers = self._headers(path, nonce, rawBody)
        # send the post request
        r = requests.post(self.AUTH_URL + path, headers=headers, data=rawBody, verify=True)

        openOrders = []
        if r.status_code == 200:
            results = r.json()

            # load current prices and high,lows for all symbols
            tickers = self._loadCurrentPricesAndHighLow()

            for order in results:
                # get currencyPair to format {coin1}_{coin2}
                # it is in format t{coin1}{coin2} or t{coin1}:{coin2} if coin1 has more than 3 letters
                if "t" in order[3]:             # order[3] = symbol
                    # add it to the set of used symbols
                    symbol = order[3][1:]       # erase the "t" from the symbol
                    if ":" in symbol:
                        symbol = symbol.replace(":", "_")
                    elif len(symbol) == 6:
                        symbol = symbol[:3] + "_" + symbol[3:]
                    else:
                        print("bitfinex trading symbol cant be parsed into coins {}".format(order[3]))
                else:
                    print("bitfinex trading symbol cant be parsed into coins {}".format(order[3]))

                # change BAB or BCHN to BCH and more
                for coinStupidName, coinRealName in self.BITFINEX_STUPID_NAMING.items():
                    if coinStupidName in symbol:
                        symbol = coinRealName + "_" + symbol.split("_")[1]

                openOrders.append(
                    {
                        "timestamp": order[5],  # timestamp updated (not created)
                        "coinPair": symbol,
                        "amountBuy": order[6] if order[6] >= 0 else -order[6] * order[16],
                        "amountSell": order[6] if order[6] < 0 else -order[6] * order[16],
                        "price": order[16],
                        "type": "BUY" if order[6] >= 0 else "SELL",     # amount > 0 or not
                        "currentPrice": tickers[order[3]]["price"],
                        "high": tickers[order[3]]["high"],
                        "low": tickers[order[3]]["low"],
                    }
                )

                # ust -> usdt
                # dsh -> dash
                # iot -> iota

        else:
            raise LoadingError

        return openOrders

    def _loadCurrentPricesAndHighLow(self):
        """ load tickers from api and return them in format:
            returns:
                tickers (dict): {symbol: {price: xxx, high: xxx, low: xxx}}
        """
        # send the post request
        r = requests.get(self.PUBL_URL + "v2/tickers", params={"symbols": "ALL"})

        tickers = {}
        if r.status_code == 200:
            results = r.json()

            for ticker in results:
                tickers[ticker[0]] = {
                    "price": ticker[7],
                    "high": ticker[9],
                    "low": ticker[10],
                }

        return tickers
