import requests


class NeonClient(object):
    BASE_URL = "https://api.neoscan.io/"   # https://api.neoscan.io/api/main_net/v1/get_balance/ALWtXtBopJz3tX1SDqEhcXE5zJPCrMfvmR

    def __init__(self, WALLET_ADDRESS):
        super().__init__()
        self.WALLET_ADDRESS = WALLET_ADDRESS

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        balances = {}

        # get all balances from neon wallet address (all balances are on the same address)
        request = requests.get(self.BASE_URL + "api/main_net/v1/get_balance/" + self.WALLET_ADDRESS)
        # try to parse the request as json
        results = request.json()

        # return them all in dictionary format "COIN":"BALANCE"
        for balance in results["balance"]:
            if balance["asset"] == "Ontology Token":    # rename ontology
                balance["asset"] = "ONT"
            balances[balance["asset"]] = balance["amount"]

        return balances
