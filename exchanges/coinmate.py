import requests
import hmac
import hashlib
import json

from datetime import datetime
from com.common import LoadingError


class CoinmateClient(object):
    BASE_URL = "https://coinmate.io/api/"

    def __init__(self, CLIENT_ID, PUBLIC_KEY, PRIVATE_KEY):
        super().__init__()
        self.CLIENT_ID = CLIENT_ID
        self.PUBLIC_KEY = PUBLIC_KEY
        self.PRIVATE_KEY = str.encode(PRIVATE_KEY)  # convert to bytes

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        # nonce must be an increasing number with every request so time is used
        nonce = int(datetime.now().timestamp())
        path = self.BASE_URL + "balances"
        signature = self.createSignature(self.CLIENT_ID, self.PUBLIC_KEY, self.PRIVATE_KEY, nonce)

        # create data for POST request
        dataDictionary = {
            "clientId": self.CLIENT_ID,
            "publicKey": self.PUBLIC_KEY,
            "nonce": nonce,
            "signature": signature
        }

        request = requests.post(path, dataDictionary)
        # try to get results from coinmate servers
        results = request.json()
        if results["error"] is True:
            raise LoadingError
        else:
            # data loaded, return non 0 balances
            balances = {}
            for coin in results["data"]:
                if results["data"][coin]["balance"] > 0:
                    balances[coin] = results["data"][coin]["balance"]
            return balances

    def createSignature(self, clientId, apiKey, privateKey, nonce):
        """
        Calculate a signature for coinmate API requests
        """
        message = str(nonce) + str(clientId) + apiKey
        signature = hmac.new(privateKey, message.encode(), digestmod=hashlib.sha256).hexdigest()
        return signature.upper()

    def loadPrices(self, bases, symbols):
        """ loads prices for all combinations of bases and symbols

        args:
            bases - base coins as the 1st currency in the trading pair
            symbols - 2nd currency

        returns dictionaries with [base]:{symbol:price} (only for all combinations between base and my coins)
            i.e.: [USD]: "CZK": 7500.8
        """
        # dictionary with pairs [base]:{symbol:price}
        prices = {}

        path = self.BASE_URL + "ticker"
        # go through all requested pairs
        for base in bases:
            prices[base] = {}
            for symbol in symbols:
                # request the ticker for the current base-symbol pair
                request = requests.get(path, params={"currencyPair": "{}_{}".format(symbol, base)})
                # try to get results from coinmate servers
                try:
                    results = request.json()
                except json.JSONDecodeError:
                    print("Coinmate {} price NOT loaded".format(base + symbol))
                else:
                    print("Coinmate {} price loaded OK".format(base + symbol))

                prices[base][symbol] = results["data"]["last"]

        return prices

    def getTransactionHistory(self, timestampFrom):
        """ gets the whole transaction history from the specified timestampFrom
            returns:
                results (dict): dict with all info got from coinmate transaction history request (complicated format, debug to see)
                timestampSync (int timestamp in ms - 13 digit)
        """
        path = self.BASE_URL + "transactionHistory"
        # nonce must be an increasing number with every request so time is used
        nonce = int(datetime.now().timestamp())
        signature = self.createSignature(self.CLIENT_ID, self.PUBLIC_KEY, self.PRIVATE_KEY, nonce)

        # create data for POST request
        params = {
            "clientId": self.CLIENT_ID,
            "publicKey": self.PUBLIC_KEY,
            "nonce": nonce,
            "signature": signature,
            "offset": 0,
            "timestampFrom": timestampFrom,
            "timestampTo": int(datetime.now().timestamp() * 1000),
        }

        request = requests.post(path, params)

        # if request response is OK (200)
        if request.status_code == 200:
            results = request.json()
        else:
            results = None
            params["timestampTo"] = None

        return results, params["timestampTo"]

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        transactions, timestampSync = self.getTransactionHistory(timestampFrom)

        newTransactions = {}
        if "data" in transactions:
            for transaction in transactions["data"]:
                # change amount sign for withdrawals and sells
                if transaction["transactionType"] in ["WITHDRAWAL", "SELL", "QUICK_SELL"]:
                    transaction["amount"] = -transaction["amount"]

                # create list for this timestamp's transactions
                if transaction["timestamp"] not in newTransactions:
                    newTransactions[transaction["timestamp"]] = []
                # append the transaction of the main coin (that was sold, bought, withdrawn etc.)
                newTransactions[transaction["timestamp"]].append({
                    "coin": transaction["amountCurrency"],
                    "amount": transaction["amount"],
                    "operation": ("BUY/SELL" if transaction["transactionType"] in ["BUY", "SELL", "QUICK_SELL"]
                                  else transaction["transactionType"]),
                    "coinOppo": (transaction["priceCurrency"] if transaction["priceCurrency"] is not None else None),
                    "amountOppo": (transaction["price"] * -transaction["amount"] if transaction["price"] is not None else None),
                })

                # append the transaction's fee (will be treated as separate transaction)
                if transaction["fee"] > 0:   # do not add empty transactions
                    newTransactions[transaction["timestamp"]].append({
                        "coin": transaction["feeCurrency"],
                        "amount": -transaction["fee"],
                        "operation": "FEE"
                    })
                # append the transaction of the coin that was used as a payment
                # (deposit and withdrawal has no price)
                if transaction["price"] is not None:
                    newTransactions[transaction["timestamp"]].append({
                        "coin": transaction["priceCurrency"],
                        "amount": (- transaction["amount"] * transaction["price"]),
                        "operation": "BUY/SELL",
                        "coinOppo": transaction["amountCurrency"],
                        "amountOppo": transaction["amount"],
                    })
        else:   # no data loaded
            raise LoadingError

        return newTransactions, timestampSync

    def getOpenOrders(self):
        """ gets and return all open orders with additional data (prices data for the used coinPairs)
            returns:
                openOrders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                              price(float), type(str), currentPrice(float), high(float), low(float)}]
        """
        # create data for POST request
        path = self.BASE_URL + "openOrders"
        nonce = int(datetime.now().timestamp())
        signature = self.createSignature(self.CLIENT_ID, self.PUBLIC_KEY, self.PRIVATE_KEY, nonce)
        params = {"clientId": self.CLIENT_ID, "publicKey": self.PUBLIC_KEY, "nonce": nonce, "signature": signature}
        request = requests.post(path, params)

        # if request response is OK (200)
        if request.status_code == 200:
            try:
                loadedOpenOrders = request.json()
            except json.JSONDecodeError:
                print("Coinmate open orders loading failed")
                raise LoadingError
        else:
            loadedOpenOrders = None

        openOrders = []
        if "data" in loadedOpenOrders:

            for order in loadedOpenOrders["data"]:
                # prepare data to correct format
                openOrders.append(
                    {
                        "timestamp": order["timestamp"],
                        "coinPair": order["currencyPair"],
                        "amountBuy": order["amount"] if order["type"] == "BUY" else order["amount"] * order["price"],
                        "amountSell": -order["amount"] if order["type"] == "SELL" else -order["amount"] * order["price"],
                        "price": order["price"],
                        "type": order["type"],
                    }
                )

            # get set of all used coin pairs in all openOrders
            usedPairs = set([order["coinPair"] for order in openOrders])

            # go through all requested pairs
            for pair in usedPairs:
                # load the current price of the pair
                currentPrice, high, low = self._loadCurrentPriceAndHighLow(pair)
                # set the loaded price as the current price for this order, save high, low values
                for order in openOrders:
                    if order["coinPair"] == pair:
                        order["currentPrice"] = currentPrice
                        order["high"] = high
                        order["low"] = low

        else:
            raise LoadingError

        return openOrders

    def _loadCurrentPriceAndHighLow(self, pair):
        """ loads the current price of the selected trading currency pair
            args:
                pair (str) - currency pair, format {}_{}, example BTC_EUR
            returns:
                prices (3x float): currentPrice, high, low"""
        path = self.BASE_URL + "ticker"

        # request the ticker for the current base-symbol pair
        request = requests.get(path, params={"currencyPair": pair})
        # try to get results from coinmate servers
        try:
            results = request.json()
        except json.JSONDecodeError:
            print("Coinmate loading price for pair {} failed".format(pair))
            raise LoadingError
        else:
            if "data" in results:
                return results["data"]["last"], results["data"]["high"], results["data"]["low"],
            else:
                raise LoadingError
