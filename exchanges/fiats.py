""" api to https://exchangeratesapi.io/ for gathering fiat coins exchange rates"""
import requests
import json


class Fiats(object):
    BASE_URL = "https://api.exchangeratesapi.io/"

    def loadPrices(self, bases, symbols):
        """ loads prices for all combinations of bases and symbols

        args:
            bases - base coins as the 1st currency in the trading pair
            symbols - 2nd currency

        returns dictionaries with [base]:{symbol:price} (only for all combinations between base and my coins)
            i.e.: [USD]: "CZK": 7500.8
        """
        # create an endpoint URL to gather all current market prices
        path = self.BASE_URL + "latest"
        # convert list to string
        symbolsString = ",".join(symbols)

        # dictionary with pairs tradingPair:value
        prices = {}

        for base in bases:
            request = requests.get(path, params={"base": base, "symbols": symbolsString})
            # try to format the request according to json format
            try:
                results = request.json()
            except json.JSONDecodeError:
                print("{} fiat prices NOT loaded".format(base))
            else:
                # data loaded correctly
                print("{} fiat prices loaded OK".format(base))
                # save loaded values in dictionary
                prices[base] = results["rates"]

        return prices
