import time

from datetime import datetime
import dateutil.parser
from coinbase.wallet.client import Client
from coinbase.wallet.error import InternalServerError


class CoinbaseClient(object):
    API_URL = 'https://api.pro.coinbase.com/'

    def __init__(self, KEY, SECRET):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET
        self.client = Client(self.KEY, self.SECRET)

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        try:
            accounts = self.client.get_accounts()
        except InternalServerError:
            return {}

        balances = {}

        for account in accounts["data"]:
            # don't do anything for empty accounts
            if float(account["balance"]["amount"]) > 0:
                # rename coinbase CGLD to normal CELO
                account["currency"] = "CELO" if account["currency"] == "CGLD" else account["currency"]

                balances[account["currency"]] = float(account["balance"]["amount"])

        return balances

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                transactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        newTransactions = {}
        # must be before loading transactions so it doesn't miss any transaction
        timestampSync = int(time.time() * 1000)

        # get all accounts (1 account = 1 coin) and get all non 0 account transactions
        accounts = self.client.get_accounts()
        for account in accounts["data"]:
            if account["currency"] == "BTC":
                pass
            # don't do anything for empty accounts
            if float(account["balance"]["amount"]) > 0:
                # rename coinbase CGLD to normal CELO
                account["currency"] = "CELO" if account["currency"] == "CGLD" else account["currency"]
                # get transactions for this account
                oneCoinTransactions = account.get_transactions()

                for transaction in oneCoinTransactions["data"]:
                    timestamp = int(datetime.timestamp(dateutil.parser.isoparse(transaction["created_at"])) * 1000)
                    # work only with new transactions
                    if timestamp > timestampFrom:
                        # zatim mam pouze vydelky z coinbase earn, takze moc nemuzu udelat jine typy transakci
                        # create list for this timestamp's transactions
                        if timestamp not in newTransactions:
                            newTransactions[timestamp] = []

                        # coinbase earn and forks are rewards - no way to distinguish forks generally
                        if (transaction["details"]["subtitle"] == "From Coinbase Earn"
                                or (transaction["description"] and "Fork Balance Credit" in transaction["description"])
                                or transaction["details"]["title"] == "Received Bitcoin Cash"):
                            operation = "REWARD"
                        elif float(transaction["amount"]["amount"]) > 0:
                            operation = "DEPOSIT"
                        else:
                            operation = "WITHDRAWAL"

                        # append the transaction of the main coin (that was sold, bought, withdrawn etc.)
                        newTransactions[timestamp].append({
                            "coin": account["currency"],
                            "amount": float(transaction["amount"]["amount"]),
                            "operation": operation,
                        })

        return newTransactions, timestampSync
