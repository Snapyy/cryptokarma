import hmac
import hashlib
import requests
import json
import copy

from operator import itemgetter
from datetime import datetime

from PyQt5.QtWidgets import QApplication

from com import common
from com.progressInformer import ProgressInformer
import com.styles as styles

from binance.client import Client


def syncDecorator(func):
    """ wrapping a function with this decorator will cause it to be triggered twice
        and if the first try fails with BinanceAPIException, timeSync will be done """
    def wrapper(self):
        # counts nr. of attempts to get binance stuff
        # (after 1st not successful attempt, time syncing happens and then the 2nd attempt is made)
        counter = 0
        while counter < 2:
            # 1 attemt is made to get balances
            counter += 1
            try:
                return func(self)
            except BinanceAPIException:
                self.syncTime()
    return wrapper


class BinanceClient(object):

    API_URL = 'https://api.binance.{}/api'
    WITHDRAW_API_URL = 'https://api.binance.{}/wapi'
    WITHDRAW_API_VERSION = 'v3'
    PRIVATE_API_VERSION = 'v3'

    def __init__(self, KEY, SECRET, ui):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET
        self.ui = ui

        self.API_URL = self.API_URL.format("com")
        self.WITHDRAW_API_URL = self.WITHDRAW_API_URL.format("com")
        self.session = self._init_session()
        # variable to compensate the difference between my system time and binance server time
        self.timeSync = 0
        try:
            self.binanceProfiClient = Client(self.KEY, self.SECRET)
        except requests.exceptions.ConnectionError:
            print("Connection error - Binance Client")

    def _init_session(self):
        session = requests.session()
        session.headers.update({'Accept': 'application/json',
                                'User-Agent': 'binance/python',
                                'X-MBX-APIKEY': self.KEY})
        return session

    @syncDecorator
    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        account = self._get('account', True, data={})
        balances = {}
        try:
            for coin in account["balances"]:
                # on this exchange, you must add free and locked balances
                coinBalance = float(coin["free"]) + float(coin["locked"])
                # if the balance is not 0, add it to balances dictionary
                if coinBalance > 0:
                    balances[coin["asset"]] = coinBalance

            return balances
        except TypeError:
            print("Binance load balances problem")

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        trades, withdraws, deposits, dustConverts, timestampSync = self.getTransactionHistory(timestampFrom)

        assets = self.loadMyBinanceAssets()

        newTransactions = {}

        # trades
        for symbolTrades in trades:
            for trade in symbolTrades:
                timestamp = trade["time"]
                # from symbol (i.e. LTCBTC) parse both assets: LTC and BTC
                for i in range(-3, -7, -1):
                    if trade["symbol"][i:] in assets:
                        coin1 = trade["symbol"][i:]
                        break
                try:
                    coin2 = trade["symbol"].replace(coin1, "")
                except ValueError:
                    print("Binance - symbol cant be parsed to 2 assets")
                else:
                    # create list for this timestamp's transactions
                    if timestamp not in newTransactions:
                        newTransactions[timestamp] = []

                    if trade["isBuyer"] is True:
                        amount1 = -float(trade["quoteQty"])
                        amount2 = float(trade["qty"])
                    else:
                        amount1 = float(trade["quoteQty"])
                        amount2 = -float(trade["qty"])

                    # append the transaction of the base coin (that was sold, bought, withdrawn etc.)
                    newTransactions[timestamp].append({
                        "coin": coin1,
                        "amount": amount1,
                        "operation": "BUY/SELL",
                        "coinOppo": coin2,
                        "amountOppo": amount2,
                    })
                    # append the transaction of the other coin
                    newTransactions[timestamp].append({
                        "coin": coin2,
                        "amount": amount2,
                        "operation": "BUY/SELL",
                        "coinOppo": coin1,
                        "amountOppo": amount1,
                    })
                    # append the transaction's fee (will be treated as separate transaction)
                    newTransactions[timestamp].append({
                        "coin": trade["commissionAsset"],
                        "amount": -float(trade["commission"]),
                        "operation": "FEE",
                    })

        for withdraw in withdraws:
            timestamp = withdraw["applyTime"]
            # create list for this timestamp's transactions
            if timestamp not in newTransactions:
                newTransactions[timestamp] = []

            newTransactions[timestamp].append({
                "coin": withdraw["asset"],
                "amount": -float(withdraw["amount"]),
                "operation": "WITHDRAWAL",
            })

            newTransactions[timestamp].append({
                "coin": withdraw["asset"],
                "amount": -float(withdraw["transactionFee"]),
                "operation": "FEE",
            })

        for deposit in deposits:
            timestamp = deposit["insertTime"]
            # create list for this timestamp's transactions
            if timestamp not in newTransactions:
                newTransactions[timestamp] = []

            newTransactions[timestamp].append({
                "coin": deposit["asset"],
                "amount": float(deposit["amount"]),
                "operation": "DEPOSIT",
            })

        for dustConvert in dustConverts:
            timestamp = int(datetime.strptime(dustConvert["operate_time"], "%Y-%m-%d %H:%M:%S").timestamp() * 1000)
            # create list for this timestamp's transactions
            if timestamp not in newTransactions:
                newTransactions[timestamp] = []

            newTransactions[timestamp].append({
                "coin": "BNB",
                "amount": float(dustConvert["transfered_total"]),
                "operation": "BUY/SELL",
            })

            for log in dustConvert["logs"]:
                newTransactions[timestamp].append({
                    "coin": log["fromAsset"],
                    "amount": -float(log["amount"]),
                    "operation": "BUY/SELL",
                })

        return newTransactions, timestampSync

    def getTransactionHistory(self, timestampFrom):
        """ gets the whole transaction history from the specified timestampFrom
            returns:
                results (dict): dict with all info got from transaction history request (complicated format, debug to see)
                timestampSync (int timestamp in ms - 13 digit)
        """
        # for binance, every symbol (trading pair) is a separate api request
        # so to save time, only known used symbols are searched for new transacitons
        myBinanceSymbols = self.loadMyBinanceSymbols()

        timestampTo = int(datetime.utcnow().timestamp() * 1000)
        delta90DaysTimeStamp = 7770000000

        # these 3 lists of transactions will be filled and returned
        trades = []
        withdraws = []
        deposits = []

        # get new trades
        params = {"startTime": timestampFrom}
        for symbol in myBinanceSymbols:
            params["symbol"] = symbol
            result = self._get('myTrades', True, data=params)
            if len(result) > 0:
                trades.append(result)

        # get new withdraws and deposits
        currentStartTime = timestampFrom
        while currentStartTime < timestampTo:
            params = {"startTime": currentStartTime, "endTime": currentStartTime + delta90DaysTimeStamp}
            if params["endTime"] > timestampTo:
                params["endTime"] = timestampTo

            newWithdraws = self._request_withdraw_api('get', 'withdrawHistory.html', True, data=params)
            newDeposits = self._request_withdraw_api('get', 'depositHistory.html', True, data=params)

            if "depositList" in newDeposits:
                deposits.extend(newDeposits["depositList"])
            if "withdrawList" in newWithdraws:
                withdraws.extend(newWithdraws["withdrawList"])
            currentStartTime = currentStartTime + delta90DaysTimeStamp

        # get all conversion od dust to BNB
        dustConverts = self._request_withdraw_api('get', 'userAssetDribbletLog.html', True, data=params)["results"]["rows"]
        newDustConverts = []
        for dustConvert in dustConverts:
            convertTimestamp = int(datetime.strptime(dustConvert["operate_time"], "%Y-%m-%d %H:%M:%S").timestamp() * 1000)
            if convertTimestamp > timestampFrom:
                newDustConverts.append(dustConvert)

        return trades, withdraws, deposits, newDustConverts, timestampTo

    def loadMyBinanceSymbols(self):
        """ loads symbols from file
            binance symbol is a trading pair: napr.: LTCBTC """
        try:
            with open(common.PATH + "/user/safefiles/myBinanceSymbols.json", "r", encoding="utf-8") as loadedFile:
                # try to decode the file with json format
                return json.load(loadedFile)
        except FileNotFoundError:
            print("myBinanceSymbols.json file not found")
            return []
        except json.JSONDecodeError as e:
            print("Problem reading myBinanceSymbols.json: " + str(e))
            return []

    def saveMyBinanceSymbols(self, myBinanceSymbols):
        """ saves symbols to file
            binance symbol is a trading pair: napr.: LTCBTC """
        try:
            with open(common.PATH + "/user/safefiles/myBinanceSymbols.json", "w", encoding="utf-8") as savedFile:
                # dump dictionary to the chosen file
                json.dump(myBinanceSymbols, savedFile, indent=4)
        except PermissionError:
            print("Problem writting in myBinanceSymbols.json, permission error")

    def loadMyBinanceAssets(self):
        """ loads and returns assets from binance api
            binance asset is a single currency: napr.: LTC """
        assets = self._request_withdraw_api('get', 'assetDetail.html', True, data={})
        if "success" in assets and assets["success"] is True:
            return [asset for asset in assets["assetDetail"].keys()]
        else:
            print("Binance asset could not be loaded, Binance getNewTransactions function will NOT work properly")
            return []

    def refreshBinanceSymbolsAssets(self):
        """ tries to load all binance symbols used in transactions
        new symbols are added to json file with known symbols """
        # get list of all symbols
        exchangeInfo = self._get('exchangeInfo')
        allBinanceSymbols = [symbol["symbol"] for symbol in exchangeInfo["symbols"]]
        results = []

        # initialize new progressInformer
        self.progressInfo_symbols = ProgressInformer(styles.PB_BINANCE, self.ui.frame_progressInformers)

        # filter those that have at least one trade/transaction
        for i, symbol in enumerate(allBinanceSymbols):
            params = {"symbol": symbol}
            result = self._get('myTrades', True, data=params)
            if len(result) > 0:
                results.append(result)
            if i % 10 == 0:
                # update progress informer
                self.progressInfo_symbols.setText("Refreshing Binance symbols: " + str(i) + " out of " + str(len(allBinanceSymbols)))
                self.progressInfo_symbols.setValue(i / len(allBinanceSymbols) * 100)
                QApplication.processEvents()
        # add the new symbols to the myBinanceSymbols file
        if len(results) > 0:
            myBinanceSymbols = self.loadMyBinanceSymbols()
            myBinanceSymbols.extend(result[0]["symbol"] for result in results if result[0]["symbol"] not in myBinanceSymbols)

        # update progress informer
        self.progressInfo_symbols.setText("Refreshing Binance symbols: ... done!!!")
        self.progressInfo_symbols.setValue(100)

        self.saveMyBinanceSymbols(myBinanceSymbols)

    def getOpenOrders(self):
        """ gets and return all open orders with additional data (prices data for the used coinPairs)
            returns:
                openOrders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                              price(float), type(str), currentPrice(float), high(float), low(float)}]
        """
        # get all binance assets (coinNames) to correctly split symbols (coinPair) of loaded orders
        assets = self.loadMyBinanceAssets()
        # get tickers - containing current price, high, low
        tickers = self._get('ticker/24hr', data={}, version=self.PRIVATE_API_VERSION)

        openOrders = []
        # load openOrders of the account
        try:
            loadedOpenOrders = self._get('openOrders', True, data={})
        except BinanceAPIException as e:
            print("Binance request failure: " + str(e))
        else:
            for order in loadedOpenOrders:
                # from symbol (i.e. LTCBTC) parse both assets: LTC and BTC
                for i in range(-3, -7, -1):
                    if order["symbol"][i:] in assets:
                        base = order["symbol"][i:]
                        break
                try:
                    coin = order["symbol"].replace(base, "")
                except ValueError:
                    print("Binance - symbol cant be parsed to 2 assets")
                else:
                    # prepare data to correct format
                    openOrders.append(
                        {
                            "timestamp": order["updateTime"],
                            "coinPair": coin + "_" + base,
                            "amountBuy": (float(order["origQty"]) if order["side"] == "BUY"
                                          else float(order["origQty"]) * float(order["price"])),
                            "amountSell": (-float(order["origQty"]) if order["side"] == "SELL"
                                           else -float(order["origQty"]) * float(order["price"])),
                            "price": float(order["price"]),
                            "type": order["side"],
                        }
                    )

                    # add info extracted from tickers and market summaries
                    for ticker in tickers:
                        if ticker["symbol"] == order["symbol"]:
                            openOrders[-1]["currentPrice"] = float(ticker["lastPrice"])
                            openOrders[-1]["high"] = float(ticker["highPrice"])
                            openOrders[-1]["low"] = float(ticker["lowPrice"])
                            break

        return openOrders

    def syncTime(self):
        print("Binance timestamp error, synchronizing server time with my time...")
        serverTime = self.get_server_time()
        myTime = datetime.utcnow().timestamp()
        self.timeSync = myTime * 1000 - serverTime["serverTime"] + 500

    def _get(self, path, signed=False, version='v3', **kwargs):
        return self._request_api('get', path, signed, version, **kwargs)

    def _request_api(self, method, path, signed=False, version='v3', **kwargs):
        uri = self._create_api_uri(path)

        return self._request(method, uri, signed, **kwargs)

    def _create_api_uri(self, path):
        return self.API_URL + '/v3/' + path

    def _request_withdraw_api(self, method, path, signed=False, **kwargs):
        uri = self._create_withdraw_api_uri(path)

        return self._request(method, uri, signed, True, **kwargs)

    def _create_withdraw_api_uri(self, path):
        return self.WITHDRAW_API_URL + '/' + self.WITHDRAW_API_VERSION + '/' + path

    def _request(self, method, uri, signed, force_params=False, **kwargs):

        originalKwargs = copy.deepcopy(kwargs)
        localKwargs = copy.deepcopy(kwargs)
        # do the following code twice if the first one fails with timestamp exception
        tries = 0
        while tries < 2:
            tries += 1
            localKwargs = copy.deepcopy(originalKwargs)
            # set default requests timeout
            localKwargs['timeout'] = 10

            data = localKwargs.get('data', None)
            if data and isinstance(data, dict):
                localKwargs['data'] = data
                # find any requests params passed and apply them
                if 'requests_params' in localKwargs['data']:
                    # merge requests params into localKwargs
                    localKwargs.update(localKwargs['data']['requests_params'])
                    del(localKwargs['data']['requests_params'])

            if signed:
                # generate signature
                localKwargs['data']['timestamp'] = int(datetime.utcnow().timestamp() * 1000 - self.timeSync)
                localKwargs['data']['signature'] = self._generate_signature(localKwargs['data'])

            # sort get and post params to match signature order
            if data:
                # sort post params
                localKwargs['data'] = self._order_params(localKwargs['data'])
                # Remove any arguments with values of None.
                null_args = [i for i, (key, value) in enumerate(localKwargs['data']) if value is None]
                for i in reversed(null_args):
                    del localKwargs['data'][i]

            # if get request assign data array to params value for requests lib
            if data and (method == 'get' or force_params):
                localKwargs['params'] = '&'.join('%s=%s' % (data[0], data[1]) for data in localKwargs['data'])
                del(localKwargs['data'])

            self.response = getattr(self.session, method)(uri, **localKwargs)

            try:
                return self._handle_response()
            except BinanceAPIException as e:
                if e.code == -1021:
                    self.syncTime()
                else:
                    raise e

    def _handle_response(self):
        """Internal helper for handling API responses from the Binance server.
        Raises the appropriate exceptions when necessary; otherwise, returns the
        response.
        """
        if not str(self.response.status_code).startswith('2'):
            raise BinanceAPIException(self.response)
        try:
            return self.response.json()
        except ValueError:
            raise BinanceRequestException('Invalid Response: %s' % self.response.text)

    def _generate_signature(self, data):

        ordered_data = self._order_params(data)
        query_string = '&'.join(["{}={}".format(d[0], d[1]) for d in ordered_data])
        m = hmac.new(self.SECRET.encode('utf-8'), query_string.encode('utf-8'), hashlib.sha256)
        return m.hexdigest()

    def _order_params(self, data):
        """Convert params to list with signature as last element """
        has_signature = False
        params = []
        for key, value in data.items():
            if key == 'signature':
                has_signature = True
            else:
                params.append((key, value))
        # sort parameters by key
        params.sort(key=itemgetter(0))
        if has_signature:
            params.append(('signature', data['signature']))
        return params

    # User Stream Endpoints
    def get_server_time(self):
        """Test connectivity to the Rest API and get the current server time. """
        return self._get('time')


class BinanceAPIException(Exception):
    def __init__(self, response):
        self.code = 0
        try:
            json_res = response.json()
        except ValueError:
            self.message = 'Invalid JSON error message from Binance: {}'.format(response.text)
        else:
            self.code = json_res['code']
            self.message = json_res['msg']
        self.status_code = response.status_code
        self.response = response
        self.request = getattr(response, 'request', None)

    def __str__(self):  # pragma: no cover
        return 'APIError(code=%s): %s' % (self.code, self.message)


class BinanceRequestException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'BinanceRequestException: %s' % self.message
