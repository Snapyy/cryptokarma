import cbpro
from datetime import datetime
import time
import json
import dateutil

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication

import com.common as common
from com.common import LoadingError
from com.progressInformer import ProgressInformer
import com.styles as styles


class CoinbaseProClient(object):

    def __init__(self, KEY, SECRET, PASSPHRASE, ui):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET
        self.PASSPHRASE = PASSPHRASE
        self.ui = ui

        # create CoinbasePro object
        self.my_CoinbasePro = cbpro.AuthenticatedClient(self.KEY, self.SECRET, self.PASSPHRASE)

        # used to limit max. nr. of requests per second to coinbase pro api
        self.requestCounter = 0
        # initialize timer for auto refreshing wallets and Values of My portfolio
        self.requestLimitTimer = QTimer()
        self.requestLimitTimer.timeout.connect(self._requestLimitTimerTimeout)

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        balances = {}
        results = self.my_CoinbasePro.get_accounts()

        for account in results:
            # data loaded, return non 0 balances
            if float(account["balance"]) > 0:
                # CELO is named CGLD in CoinbasePro
                if account["currency"] == "CGLD":
                    balances["CELO"] = float(account["balance"])
                else:
                    balances[account["currency"]] = float(account["balance"])

        return balances

    def _requestLimitTimerTimeout(self):
        """ This is an event handler for timeout of the timer limiting nr. of requests per second
            resets the request counter to 0 """
        self.requestCounter = 0

    def _waitDueToRequestLimit(self):
        """ wait here until the request limit is reset """
        while self.requestCounter >= self.limitRequestsPerSecond:
            QApplication.processEvents()
        # add one request to the request counter
        self.requestCounter += 1

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        newTransactions = {}
        # must be before loading transactions so it doesn't miss any transaction
        timestampSync = int(time.time() * 1000)

        # load acounts (1 account per coin) and products (trading pairs) that this user used
        # the lists are refreshed in refresh functions usually once a day and it take around 1 minute
        myCoinbaseProAccounts = self.loadFromJson(common.PATH + "/user/safefiles/", "myCoinbaseProAccounts.json")
        myCoinbaseProProducts = self.loadFromJson(common.PATH + "/user/safefiles/", "myCoinbaseProProducts.json")

        # starts timer which ensures that no more than X request will be done to CB pro api
        # limit nr. of requests to coinbase pro api to 5 request per second
        self.requestLimitTimer.start(1000)
        self.limitRequestsPerSecond = 5

        # go through all used accounts (coins)
        for coin, accountId in myCoinbaseProAccounts.items():
            self._waitDueToRequestLimit()
            # get list of all account's transactions
            # accountTransactions = list(self.my_CoinbasePro.get_account_history(accountId)) for debugging
            accountTransactions = self.my_CoinbasePro.get_account_history(accountId)

            for transaction in accountTransactions:
                # create timestamp from time when transaction was made
                timestamp = int(datetime.timestamp(datetime.strptime(transaction["created_at"],
                                "%Y-%m-%dT%H:%M:%S.%fZ")) * 1000)
                # work only with new transactions
                if timestamp > timestampFrom:
                    if transaction["type"] == "fee":
                        operation = "FEE"
                    elif transaction["type"] == "transfer" and transaction["details"]["transfer_type"] == "deposit":
                        operation = "DEPOSIT"
                    elif transaction["type"] == "transfer" and transaction["details"]["transfer_type"] == "withdraw":
                        operation = "WITHDRAWAL"
                    else:
                        continue

                    # change CGLD to CELO
                    currency = "CELO" if coin == "CGLD" else coin

                    # create list for this timestamp's transactions
                    if timestamp not in newTransactions:
                        newTransactions[timestamp] = []

                    # append deposit, withdrawal and fee transactions
                    newTransactions[timestamp].append({
                        "coin": currency,
                        "amount": float(transaction["amount"]),
                        "operation": operation,
                        "coinOppo": None,
                        "amountOppo": None,
                    })

        # go through all used products (coin pairs)
        for product_id in myCoinbaseProProducts:
            self._waitDueToRequestLimit()
            # go through all fills (trades/filled orders)
            for fill in self.my_CoinbasePro.get_fills(product_id=product_id):
                # raise LoadingError if "message" in fills, message means api request limit reached
                # this problem is handled by the timer but in case of pagination, it may appea again
                if fill == "message":
                    raise LoadingError("couldn't load fills - request limit reached, slow down apie request frequency")

                timestamp = int(datetime.timestamp(datetime.strptime(fill["created_at"],
                                "%Y-%m-%dT%H:%M:%S.%fZ")) * 1000)
                # work only with new transactions
                if timestamp > timestampFrom:
                    # create list for this timestamp's transactions
                    if timestamp not in newTransactions:
                        newTransactions[timestamp] = []

                    price = float(fill["price"])
                    size = float(fill["size"])
                    side = fill["side"]  # buy or sell transaction

                    # change CGLD to CELO
                    coin = "CELO" if product_id.split("-")[0] == "CGLD" else product_id.split("-")[0]
                    coin2 = "CELO" if product_id.split("-")[1] == "CGLD" else product_id.split("-")[1]

                    # append BUY/SELL transaction - main coin
                    newTransactions[timestamp].append({
                        "coin": coin,
                        "amount": (size if side == "buy" else -size),
                        "operation": "BUY/SELL",
                        "coinOppo": coin2,
                        "amountOppo": (price * -size if side == "buy" else price * size),
                    })

                    # append BUY/SELL transaction - oppposite coin
                    newTransactions[timestamp].append({
                        "coin": coin2,
                        "amount": (price * -size if side == "buy" else price * size),
                        "operation": "BUY/SELL",
                        "coinOppo": coin,
                        "amountOppo": (size if side == "buy" else -size),
                    })

        # stops the timer that limits max. nr. of requests to coinbase pro api
        self.requestLimitTimer.stop()

        return newTransactions, timestampSync

    def refreshCoinBaseProAccountsProducts(self):
        """ goes through all accounts (coins) and products (trading pairs) and saves used ones into files """
        # load already used accounts and products so they are not loaded from api again
        myCoinbaseProAccounts = self.loadFromJson(common.PATH + "/user/safefiles/", "myCoinbaseProAccounts.json")
        myCoinbaseProProducts = self.loadFromJson(common.PATH + "/user/safefiles/", "myCoinbaseProProducts.json")

        # initialize myCoinbaseProAccounts as dict in case of returned as list (from general loadFromJson function)
        if myCoinbaseProAccounts == []:
            myCoinbaseProAccounts = {}

        # starts timer which ensures that no more than X request will be done to CB pro api
        # limit nr. of requests to coinbase pro api to 5 request per second
        self.requestLimitTimer.start(1000)
        self.limitRequestsPerSecond = 5

        accounts = self.my_CoinbasePro.get_accounts()
        products = self.my_CoinbasePro.get_products()

        maxProgress = len(accounts) + len(products) - len(myCoinbaseProAccounts) - len(myCoinbaseProProducts)
        # initialize new progressInformer
        self.progressInfo_accounts = ProgressInformer(styles.PB_COINBASE_PRO, self.ui.frame_progressInformers)

        counter = 0
        # go through all coinbasePro accounts
        for idx, account in enumerate(accounts):
            # ignore those that already are used and saved
            if account["currency"] not in myCoinbaseProAccounts:
                self._waitDueToRequestLimit()
                # get list of all account's transactions
                # accountTransactions = list(self.my_CoinbasePro.get_account_history(account["id"])) for debugging
                accountTransactions = self.my_CoinbasePro.get_account_history(account["id"])
                # save any non 0 lenght transaction lists
                if len(list(accountTransactions)) > 0:
                    myCoinbaseProAccounts[account["currency"]] = account["id"]

                # give info every fifth account/product
                if idx % 5 == 0:
                    # update progress informer
                    self.progressInfo_accounts.setText(
                        "Refreshing Coinbase Pro account/products: " + str(idx) + " out of " + str(maxProgress))
                    self.progressInfo_accounts.setValue(idx / maxProgress * 100)
                    QApplication.processEvents()

                # count and print
                counter += 1
                print("Coinbase Pro account refresh: went through {} accounts".format(counter))

        for idx, product in enumerate(products, idx):
            if product["id"] not in myCoinbaseProProducts:
                self._waitDueToRequestLimit()
                fills = self.my_CoinbasePro.get_fills(product_id=product["id"])
                # save any non 0 lenght fill lists
                if len(list(fills)) > 0:
                    myCoinbaseProProducts.append(product["id"])

                # give info every fifth account/product
                if idx % 5 == 0:
                    self.progressInfo_accounts.setText(
                        "Refreshing Coinbase Pro account/products: " + str(idx) + "out of " + str(maxProgress))
                    self.progressInfo_accounts.setValue(idx / maxProgress * 100)
                    QApplication.processEvents()

                # count and print
                counter += 1
                print("Coinbase Pro account refresh: went through {} accounts".format(counter))

        # stops the timer that limits max. nr. of requests to coinbase pro api
        self.requestLimitTimer.stop()

        # update progress informer
        self.progressInfo_accounts.setText("Refreshing Coinbase Pro account/products: ... done!!!")
        self.progressInfo_accounts.setValue(100)

        self.saveToJson(common.PATH + "/user/safefiles/", "myCoinbaseProAccounts.json", myCoinbaseProAccounts)
        self.saveToJson(common.PATH + "/user/safefiles/", "myCoinbaseProProducts.json", myCoinbaseProProducts)

    def saveToJson(self, path, fileName, data):
        """ saves data to .json file.
            Used to save myCoinBaseProAccounts and Products
            args:
                path (str) path to the saved file (example: common.PATH + "/user/safefiles/
                fileName (str) name of the file (example: myCoinbaseProAccounts.json)
                data (json compatible object)"""
        try:
            with open(path + fileName, "w", encoding="utf-8") as savedFile:
                # dump dictionary to the chosen file
                json.dump(data, savedFile, indent=4)
        except PermissionError:
            print("Problem writting in {}, permission error".format(fileName))

    def loadFromJson(self, path, fileName):
        """ loads data from .json file
            Used to load myCoinBaseProAccounts and Products """
        try:
            with open(path + fileName, "r", encoding="utf-8") as loadedFile:
                # try to decode the file with json format
                return json.load(loadedFile)
        except FileNotFoundError:
            print("{} file not found".format(fileName))
            return []
        except json.JSONDecodeError as e:
            print("Problem reading {}: ".format(fileName) + str(e))
            return []

    def getOpenOrders(self):
        """ """
        loadedOpenOrders = self.my_CoinbasePro.get_orders()

        openOrders = []
        usedProducts = set()

        for order in loadedOpenOrders:
            # save productId to get price, high, low for this order later
            usedProducts.add(order["product_id"])
            # prepare data to correct format
            openOrders.append(
                {
                    "timestamp": int(datetime.timestamp(dateutil.parser.isoparse(order['created_at'])) * 1000),
                    "coinPair": order["product_id"].replace("-", "_"),
                    "amountBuy": (float(order["size"]) if order["side"].upper() == "BUY"
                                  else float(order["size"]) * float(order["price"])),
                    "amountSell": (-float(order["size"]) if order["side"].upper() == "SELL"
                                   else -float(order["size"]) * float(order["price"])),
                    "price": float(order["price"]),
                    "type": order["side"].upper(),
                }
            )

        self.requestLimitTimer.start(1000)
        self.limitRequestsPerSecond = 3

        for product in usedProducts:
            # limit na public requesty je 3/s
            self._waitDueToRequestLimit()
            stats = self.my_CoinbasePro.get_product_24hr_stats(product)

            for order in openOrders:
                if order["coinPair"] == product.replace("-", "_"):
                    order["currentPrice"] = float(stats["last"])
                    order["high"] = float(stats["high"])
                    order["low"] = float(stats["low"])

        # stops the timer that limits max. nr. of requests to coinbase pro api
        self.requestLimitTimer.stop()

        return openOrders
