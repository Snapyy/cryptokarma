import requests
import json
import time
from urllib import parse

from datetime import datetime
from PyQt5.QtCore import QThread, pyqtSignal, QTimer
from PyQt5.QtWidgets import QApplication
from requests_toolbelt.threaded import pool
from com.common import (saveLastSynced, loadLastSynced, OLDEST_TIMESTAMP,
                        errorMessage, roundTimestampToDay, readApiKeys)
from collections import OrderedDict


class CryptoCompareError(Exception):
    ''' Bad or no responses from cryptoCompare APIs'''


class CryptoCompareClient(object):
    BASE_URL = "https://min-api.cryptocompare.com/data/"

    def __init__(self, KEY1):
        super().__init__()
        self.KEY1 = KEY1

    def _get(self, path, params):
        """ performs a get request
            args:
                path (str): url path for example:
                params (dict): pairs {paramName: paramValue} for example "toTs": 1601906863400
        """
        # make a request
        request = requests.get(path, params=params)
        # parse the request as json
        try:
            data = request.json()
        except json.JSONDecodeError:
            errorMessage("JSONDecodeError", "CryptoCompare request failed: (path: {}, params: {})".format(path, params))
            raise CryptoCompareError
        return data

    def loadCurrentPrices(self, bases, coins):
        """ loads all current price data for all pair base - coin and stores them in dict
            args:
                bases(iterable of strings): names of the base currency (i.e.: "USD")
                coins(iterable of strings): list of coin names of which the prices data will be extracted
        """
        path = self.BASE_URL + "pricemultifull"
        # create request parameter: strings of bases and coins separated by comma
        basesParam = ",".join(bases["options"])
        coinsCopy = [coin if coin != "IOT" else "MIOTA" for coin in coins]
        coinsParam = ",".join(coinsCopy)

        # create params dict, api_key must go last
        params = OrderedDict([("fsyms", coinsParam), ("tsyms", basesParam), ("api_key", self.KEY1)])

        self.currentPriceFullData = self._get(path, params)

        # # check that CZK changes are calculated correctly
        # dataCheck = {}

        # for coin, dataCoin in self.currentPriceFullData["RAW"].items():
        #     dataCheck[coin] = {}
        #     for base, dataBase in dataCoin.items():
        #         dataCheck[coin][base] = {
        #             "CONVERSIONTYPE": dataBase["CONVERSIONTYPE"],
        #             "CONVERSIONSYMBOL": dataBase["CONVERSIONSYMBOL"],
        #             "CHANGE24HOUR": dataBase["CHANGE24HOUR"],
        #             "CHANGEPCT24HOUR": dataBase["CHANGEPCT24HOUR"],
        #         }

    def getCurrentPrices(self, base, coins, headers):
        """ extract certain data for base-coin pairs from the loaded CryptoCompare data
        args:
            base (str) - 1 base currency (the 1st currency in a trading pair)
            coins (list of strings) - list of the 2nd coins in the trading pair
            headers (iterable of strings) - names of information to be extracted (i.e.: ["PRICE", "LOW24HOUR"])

        returns dictionary with [column][base]:{coin:price(float)} (only for one base)
            i.e.: [PRICE][BTC]: "USD": 7500.8
            returns only pairs that were found in the cryptocompare API (i.e.: VEEUSD might not be there - too obscure)
        """
        currentPrices = {}
        # go through all infromation that should be extracted
        for header in headers:
            currentPrices[header] = {}
            for coin in coins:
                # "IOTA" is "MIOTA" in cryptocompare
                coinInApi = "MIOTA" if coin == "IOT" else coin
                try:    # dict might not be loaded yet, or might not include coin
                    # try to extract this information from currentPriceFullData
                    currentPrices[header][coin] = self.currentPriceFullData["RAW"][coinInApi][base][header]
                except KeyError:
                    try:
                        # look if there is the inverse value and try to get it as a price
                        if header in ["PRICE", "LOW24HOUR", "HIGH24HOUR"]:
                            currentPrices[header][coin] = 1 / self.currentPriceFullData["RAW"][base][coinInApi][header]
                    except KeyError:    # base or coin does not exist in fullData
                        pass
                except AttributeError:  # currentPriceFullData dictionary was not created yet
                    pass

        return currentPrices


class LoadHistPricesThread(QThread):
    """ Creates a separate thread and in it loads historical daily prices of all pairs coin-base
        Every pair has its own timestamp when it was synchronized for the last time in lastSynced.json
        when loading is done, emits loadingDone signal in which gives the main thread newPrices and error
    """
    progressChanged = pyqtSignal(int)
    loadingDone = pyqtSignal(dict, bool)

    # the way to pass arguments to thread is through init function (constructor)
    def __init__(self, bases, coins, parent=None):
        QThread.__init__(self, parent)
        self.bases = bases
        self.coins = coins
        self.KEY2 = readApiKeys()["CryptoCompare"]["KEY2"]

    # this function is run after calling .start() function in the main thread (main window)
    def run(self):
        """ loads all prices of coin-base pair since the last synced timestamp
            args:
                base (str) - 1 base currency (the 1st currency in a trading pair)
                coins (str) - 1 coin (the 2nd coins in the trading pair)
            returns
                newPrices (dict) {base: {coin: {timestamp(10 digit): price(float)}}
                timestampTo (int timestamp in ms - 13 digit)
        """
        # dict to store all new prices in format {base:{coin:{timestamp:price}}}
        newPrices = {}
        for base in self.bases:
            newPrices[base] = {}
            for coin in self.coins:
                if coin != base:
                    newPrices[base][coin] = {}

        # get all urls and join them to a pool
        urls = self._prepareUrls()
        maxProgress = len(urls)
        progress = 0

        # flag that an error happend during loading or not - is sent back to main thread
        error = False

        # API request limit by server (limit is 20, rezerva ne asi)
        MAX_REQUESTS_PER_SEC = 17
        # start timer to controll request per second
        self.requestLimitTimer = QTimer()
        self.requestLimitTimer.timeout.connect(self._requestLimitTimerTimeout)
        self.requestLimitTimer.start(1000)
        # prepare progress tracking
        self.requestCounter = 0

        # process groups of urls until there are no left
        while urls:
            print("prices request left: " + str(len(urls)))
            # wait for the request limit timer to timeaout and reset counter
            while self.requestCounter >= MAX_REQUESTS_PER_SEC:
                time.sleep(0.05)
                QApplication.processEvents()

            p = pool.Pool.from_urls(urls[:MAX_REQUESTS_PER_SEC])
            p.join_all()
            # RESPONSES ARE IN SEMI RANDOM ORDER, U CANT USE THE ORDER IN WHICH YOU CREATED THE URLs
            for response in p.responses():
                # parse responses url to get base, coin
                url = response.request_kwargs["url"]
                params = parse.parse_qs(parse.urlparse(url).query)
                base = params["tsym"][0]
                coin = params["fsym"][0] if params["fsym"][0] != "MIOTA" else "IOT"

                # get all prices from this response and add them to newPrices
                jsonResponse = response.json()
                if jsonResponse["Response"] == "Success":
                    for priceInfo in jsonResponse["Data"]["Data"]:
                        timestamp = priceInfo["time"]
                        newPrices[base][coin][timestamp] = (priceInfo["open"] + priceInfo["close"]) / 2
                    # give info to progress bar in main thread
                    self.requestCounter += 1
                    progress += 1
                else:
                    print("Warning: Price loading failed for pair {}-{}".format(coin, base))
                    print(jsonResponse["Message"])
                    error = True

                self.progressChanged.emit(int(progress / maxProgress * 100))

            # erase the processed request
            urls = urls[MAX_REQUESTS_PER_SEC:]

        self.requestLimitTimer.stop()
        # signal for main thread - loading done
        self.loadingDone.emit(newPrices, error)

    def _requestLimitTimerTimeout(self):
        """ This is an event handler for timeout of the timer limiting nr. of requests per second
            resets the request counter to 0 """
        self.requestCounter = 0

    def _prepareUrls(self):
        """ prepares list of al urls that will soon be requested
            returns:
                urls(list of strings)
        """
        # load timestamp of the time when these prices were last synced
        syncedPrices = loadLastSynced("prices")

        urls = []
        for base in self.bases:
            for coin in self.coins:
                if coin != base:
                    # create an endpoint URL and gather ALL market current data for pairs set by parameters
                    path = CryptoCompareClient.BASE_URL + "v2/histoday"
                    # change IOT to MIOTA
                    coinCC = "MIOTA" if coin == "IOT" else coin

                    try:
                        timestampFrom = int(syncedPrices[base][coin] / 1000)    # 10-bit format
                        timestampFrom = roundTimestampToDay(timestampFrom)    # round up to the beginning of the day
                    except KeyError:
                        timestampFrom = int(OLDEST_TIMESTAMP / 1000)    # 10-bit format
                    # sync to now - timestampTo is now
                    timestampTo = int(datetime.utcnow().timestamp())   # 10-bit format
                    # calculate how many days passed from the last synced timestamp
                    timestampDelta = timestampTo - timestampFrom
                    daysToSync = int(timestampDelta / 86_400)

                    # add params to param dictionary (apiKey param must be the last one)
                    params = OrderedDict([
                        ("fsym", coinCC),
                        ("tsym", base),
                        ("limit", 2000),
                        ("toTs", 0),
                        ("api_key", self.KEY2)
                    ])

                    # more than 2000 days must be synced in separate api calls
                    while daysToSync > 2000:
                        partialTimestampTo = timestampFrom + 2000 * 86_400
                        params["toTs"] = partialTimestampTo

                        # create url string and append it to urls list
                        paramString = "&".join(["{}={}".format(name, value) for (name, value) in params.items()])
                        urls.append(path + "?" + paramString)

                        # the call with limit 2000 actually loads 2001 prices
                        # (including boths first and last timesamp day price)
                        daysToSync -= 2001

                    if daysToSync > 0:
                        params["limit"] = daysToSync
                        params["toTs"] = timestampTo

                        paramString = "&".join(["{}={}".format(name, value) for (name, value) in params.items()])
                        urls.append(path + "?" + paramString)

                        # save the last timestampTo for this pair coin-base to lastSynced.json
                        saveLastSynced(timestampTo * 1000, ("prices", base, coin))

        return urls
