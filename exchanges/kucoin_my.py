from datetime import datetime
from kucoin.client import Client
from kucoin.exceptions import KucoinAPIException
from requests import exceptions

from com.common import LoadingError


class KucoinClient(object):

    def __init__(self, KEY, SECRET, PASSPHRASE):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET
        self.PASSPHRASE = PASSPHRASE

        # create Kucoin object
        self.my_Kucoin = Client(self.KEY, self.SECRET, self.PASSPHRASE)

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        balances = {}
        try:
            results = self.my_Kucoin.get_accounts()
            for account in results:
                # ther might be multiple account for the same coin
                if account["currency"] not in balances:
                    balances[account["currency"]] = float(account["balance"])
                else:
                    balances[account["currency"]] += float(account["balance"])

        except (TypeError, KucoinAPIException, exceptions.ReadTimeout) as e:
            print("Kucoin load balances problem: {}".format(e))
            raise LoadingError
        return balances

    def loadPrices(self, bases, symbols):
        """ loads prices for all combinations of bases and symbols

        args:
            bases - base coins as the 1st currency in the trading pair
            symbols - 2nd currency

        returns dictionaries with [base]:{symbol:price} (only for all combinations between base and my coins)
            i.e.: [USD]: "CZK": 7500.8
        """
        # dictionary with pairs [base]:{symbol:price}
        prices = {}

        for base in bases:
            prices[base] = {}
            for symbol in symbols:
                try:
                    ticker = self.my_Kucoin.get_ticker("{}-{}".format(symbol, base))
                except KucoinAPIException:
                    print("Kucoin {} price NOT loaded".format(symbol + base))
                else:
                    print("Kucoin {} price loaded OK".format(symbol + base))
                    prices[base][symbol] = float(ticker["price"])

        return prices

    def getTransactionHistory(self, timestampFrom):
        """ gets the whole transaction history from the specified timestampFrom
            returns:
                results (dict): dict with all info got from transaction history request (complicated format, debug to see)
                timestampSync (int timestamp in ms - 13 digit)
        """
        transactions = []
        try:
            accounts = self.my_Kucoin.get_accounts()
        except KucoinAPIException:
            raise LoadingError

        timestampTo = int(datetime.utcnow().timestamp() * 1000)
        try:
            for account in accounts:
                results = self.my_Kucoin.get_account_activity(account_id=account["id"], start=str(timestampFrom))
                transactions.extend(item for item in results["items"])
        except (TypeError, KucoinAPIException):
            raise LoadingError

        return transactions, timestampTo

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        transactions, timestampSync = self.getTransactionHistory(timestampFrom)

        newTransactions = {}

        for transaction in transactions:
            # transfers are between kucoin account and do not count as transactions
            if transaction["bizType"] != "Transfer":
                timestamp = transaction["createdAt"]
                # create list for this timestamp's transactions
                if timestamp not in newTransactions:
                    newTransactions[timestamp] = []

                amount = float(transaction["amount"])
                amount = -amount if transaction["direction"] == "out" else amount

                if transaction["bizType"] == "Exchange":
                    operation = "BUY/SELL"
                if transaction["bizType"] == "Deposit":
                    operation = "DEPOSIT"
                if transaction["bizType"] == "Withdrawal":
                    operation = "WITHDRAWAL"

                newTransactions[timestamp].append({
                    "coin": transaction["currency"],
                    "amount": amount,
                    "operation": operation,
                })

        return newTransactions, timestampSync

    def getOpenOrders(self):
        """ gets and return all open orders with additional data (prices data for the used coinPairs)
            returns:
                openOrders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                              price(float), type(str), currentPrice(float), high(float), low(float)}]
        """
        tickers = self.my_Kucoin.get_ticker()
        loadedOpenOrders = self.my_Kucoin.get_orders(status='active')

        openOrders = []

        for order in loadedOpenOrders["items"]:
            # prepare data to correct format
            openOrders.append(
                {
                    "timestamp": order['createdAt'],
                    "coinPair": order["symbol"].replace("-", "_"),
                    "amountBuy": (float(order["size"]) if order["side"].upper() == "BUY"
                                  else float(order["size"]) * float(order["price"])),
                    "amountSell": (-float(order["size"]) if order["side"].upper() == "SELL"
                                   else -float(order["size"]) * float(order["price"])),
                    "price": float(order["price"]),
                    "type": order["side"].upper(),
                }
            )
            # add info extracted from tickers and market summaries
            for ticker in tickers["ticker"]:
                if ticker["symbol"] == order["symbol"]:
                    openOrders[-1]["currentPrice"] = float(ticker["last"])
                    openOrders[-1]["high"] = float(ticker["high"])
                    openOrders[-1]["low"] = float(ticker["low"])
                    break

        return openOrders
