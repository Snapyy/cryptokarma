import requests
import json
import hashlib
import hmac
from datetime import datetime
import dateutil.parser

from com.common import LoadingError


class BittrexClient(object):
    BASE_URL = "https://api.bittrex.com/v3/"

    def __init__(self, KEY, SECRET):
        super().__init__()
        self.KEY = KEY
        self.SECRET = SECRET

    def query(self, method, pathEnd, params):
        """ method to process all bittrex REST api requests
            args:
                method (string): type of url request: "GET" or "POST"
                pathEnd (string): end of the url path of this query
                    example: "deposits/closed"
                params (url params string): parameter key=value pairs separated by &
                    example: "currencySymbol=BTC&startDate=2017-01-02T16:23:45Z"
        """
        if params != '' and method == 'GET':
            url = self.BASE_URL + str(pathEnd) + str('?') + params
        else:
            url = self.BASE_URL + str(pathEnd)

        api_key = self.KEY
        secret = self.SECRET
        api_timestamp = str(int(datetime.now().timestamp() * 1000))

        if method == "POST":
            payload = json.dumps(params)
        else:
            payload = ''

        contentHash = hashlib.sha512(payload.encode()).hexdigest()
        pre_sign = api_timestamp + url + method + contentHash
        signature = hmac.new(secret.encode(), pre_sign.encode(), hashlib.sha512).hexdigest()
        headers = {
            'Api-Key': api_key,
            'Api-Timestamp': api_timestamp,
            'Api-Content-Hash': contentHash,
            'Api-Signature': signature,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        if method == "POST":
            r = requests.post(url=url, data=json.dumps(params), headers=headers)
        else:
            r = requests.get(url=url, params=payload, headers=headers)

        results = r.json()
        return results, r.status_code

    def getBalances(self):
        return self.query('GET', 'balances', '')

    def getTransactionHistory(self, timestampFrom):
        """ get all transactions from bitfinex,
            trades, deposits and withdrawals must be gotten as separate requests
        """
        # prepare time parameters for requests
        timestampTo = int(datetime.now().timestamp() * 1000)
        timestampToIso = datetime.fromtimestamp(int(timestampTo / 1000)).isoformat()
        timestampFromIso = datetime.fromtimestamp(int(timestampFrom / 1000)).isoformat()
        timeParams = "startDate=" + timestampFromIso + "&endDate=" + timestampToIso

        newTrades, status1 = self.query("GET", "orders/closed", timeParams)
        newDeposits, status2 = self.query("GET", "deposits/closed", timeParams)
        newWithdrawals, status3 = self.query("GET", "withdrawals/closed", timeParams)

        # if request response is not OK (200)
        if status1 != 200 and status2 != 200 and status3 != 200:
            newTrades = []
            newDeposits = []
            newWithdrawals = []
            timestampTo = None

        return newTrades, newDeposits, newWithdrawals, timestampTo

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions from the specified timestamp
            args:
                timestampFrom (int) - timestamp from which the transactions are loaded
            return value:
                newTransactions (list) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int) - timestamp of the time the transactions were synced
        """
        newTrades, newDeposits, newWithdrawals, timestampSync = self.getTransactionHistory(timestampFrom)

        newTransactions = {}

        for trade in newTrades:
            # timestamp is in ISO 8601 format, convert to timestamp (13 digit, ms)
            timestampClosed = int(datetime.timestamp(dateutil.parser.isoparse(trade['closedAt'])) * 1000)
            # extract names of coins
            coins = trade["marketSymbol"].split("-")
            coin1 = coins[0]
            coin2 = coins[1]

            # strings to floats
            amountCoin1 = float(trade["quantity"])
            amountCoin2 = float(trade["proceeds"])

            # edit signess for selling orders
            if trade["direction"] == "SELL":
                amountCoin1 = -amountCoin1
                amountCoin2 = -amountCoin2

            # create list for this timestamp's transactions
            if timestampClosed not in newTransactions:
                newTransactions[timestampClosed] = []

            # append the trade of the main coin
            newTransactions[timestampClosed].append({
                "coin": coin1,
                "amount": amountCoin1,
                "operation": "BUY/SELL",
                "coinOppo": coin2,
                "amountOppo": -amountCoin2,
            })
            # append the trade of the coin that was used as a payment
            newTransactions[timestampClosed].append({
                "coin": coin2,
                "amount": -amountCoin2,
                "operation": "BUY/SELL",
                "coinOppo": coin1,
                "amountOppo": amountCoin1,
            })
            # append the fee of the transactions always in the second coin (usually BTC)
            newTransactions[timestampClosed].append({
                "coin": coin2,
                "amount": -float(trade["commission"]),
                "operation": "FEE",
            })

        for move in newDeposits + newWithdrawals:
            # timestamp is in ISO 8601 format, convert to timestamp (13 digit, ms)
            timestampClosed = int(datetime.timestamp(dateutil.parser.isoparse(move['completedAt'])) * 1000)

            # create list for this timestamp's transactions
            if timestampClosed not in newTransactions:
                newTransactions[timestampClosed] = []

            amount = float(move["quantity"])
            # negative amount if it is a withdrawal
            amount = -amount if move in newWithdrawals else amount

            # append the move as a new transaction
            newTransactions[timestampClosed].append({
                "coin": move["currencySymbol"],
                "amount": amount,
                "operation": "WITHDRAWAL" if move in newWithdrawals else "DEPOSIT",
            })

            if "txCost" in move:
                newTransactions[timestampClosed].append({
                    "coin": move["currencySymbol"],
                    "amount": -float(move["txCost"]),
                    "operation": "FEE"
                })

        return newTransactions, timestampSync

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        # get balances
        results, status = self.getBalances()
        # if ok, translate results into pairs: coinName:balance
        if status == 200:
            balances = {}
            for coin in results:
                balances[coin["currencySymbol"]] = float(coin["total"])
            return balances
        else:
            raise LoadingError

    def getOpenOrders(self):
        """ gets and return all open orders with additional data (prices data for the used coinPairs)
            returns:
                openOrders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                              price(float), type(str), currentPrice(float), high(float), low(float)}]
        """
        # tickers contains current price
        tickers, status1 = self.query("GET", "markets/tickers", '')
        # summaries contains high, low
        summaries, status2 = self.query("GET", "markets/summaries", '')
        loadedOpenOrders, status3 = self.query("GET", "orders/open", '')

        openOrders = []

        if status1 == 200 and status2 == 200 and status3 == 200:
            for order in loadedOpenOrders:

                # prepare data to correct format
                openOrders.append(
                    {
                        "timestamp": int(datetime.timestamp(dateutil.parser.isoparse(order['createdAt'])) * 1000),
                        "coinPair": order["marketSymbol"].replace("-", "_"),
                        "amountBuy": (float(order["quantity"]) if order["direction"] == "BUY"
                                      else float(order["quantity"]) * float(order["limit"])),
                        "amountSell": (-float(order["quantity"]) if order["direction"] == "SELL"
                                       else -float(order["quantity"]) * float(order["limit"])),
                        "price": float(order["limit"]),
                        "type": order["direction"],
                    }
                )
                # add info extracted from tickers and market summaries
                for ticker in tickers:
                    if ticker["symbol"] == order["marketSymbol"]:
                        openOrders[-1]["currentPrice"] = float(ticker["lastTradeRate"])
                        break

                for summary in summaries:
                    if summary["symbol"] == order["marketSymbol"]:
                        openOrders[-1]["high"] = float(summary["high"])
                        openOrders[-1]["low"] = float(summary["low"])
                        break

        return openOrders
