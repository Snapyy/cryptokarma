import time
import requests
import dateutil


class LedgerClient(object):
    BASE_URL_BTC = "https://blockchain.info/"
    btcAddresses = []
    BASE_URL_TEZOS = "https://api.tzstats.com/"   # https://tzstats.com/docs/api/index.html#accounts

    def __init__(self, TEZOS_ADDRESS):
        super().__init__()
        self.TEZOS_ADDRESS = TEZOS_ADDRESS

    def loadBalances(self):
        """ loads balances of all coins of this wallet with balance > 0
            returns
                (dict) - coin(uppercase str) : balance(float)
        """
        balances = {}

        # TEZOS
        if self.TEZOS_ADDRESS:
            request = requests.get(self.BASE_URL_TEZOS + "explorer/account/" + self.TEZOS_ADDRESS)
            results = request.json()
            # data loaded, return non 0 balances
            print("Ledger Tezos balance loaded OK")
            balances["XTZ"] = results["total_balance"]

        return balances

    def getNewTransactions(self, timestampFrom):
        """ loads all new transactions (including trades, withdraws and deposits) from the specified timestamp
            args:
                timestampFrom (int timestamp in ms - 13 digit) - timestamp from which the transactions are loaded
            return value:
                newtransactions (dict) - transactions in a format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                timestampSync: (int timestamp in ms - 13 digit) - timestamp of the time the transactions were synced
        """
        newTransactions = {}
        # must be before loading transactions so it doesn't miss any transaction
        timestampSync = int(time.time() * 1000)

        # TEZOS
        if self.TEZOS_ADDRESS:
            # timestampFrom is ignored for tezos - always gets all transactions
            request = requests.get(self.BASE_URL_TEZOS + "explorer/account/" + self.TEZOS_ADDRESS
                                   + "/operations?type=transaction&limit=1000")
            results = request.json()
        else:
            results = {}

        for transaction in results:
            timestamp = int(dateutil.parser.isoparse(transaction["time"]).timestamp() * 1000)
            # work only with new transactions
            if timestamp > timestampFrom:
                # create list for this timestamp's transactions
                if timestamp not in newTransactions:
                    newTransactions[timestamp] = []
                # received transactions
                if transaction["receiver"] == self.TEZOS_ADDRESS:
                    newTransactions[timestamp].append({
                        "coin": "XTZ",
                        "amount": transaction["volume"],
                        # weak spot - no way to determine staking or deposit
                        "operation": "DEPOSIT" if transaction["volume"] > 3 else "STAKING"
                    })
                # sent transactions
                if transaction["sender"] == self.TEZOS_ADDRESS:
                    newTransactions[timestamp].append({
                        "coin": "XTZ",
                        "amount": -(transaction["volume"] + transaction["fee"]),
                        "operation": "WITHDRAWAL"
                    })

        return newTransactions, timestampSync
