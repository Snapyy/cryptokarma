class GraphData(object):
    """ contains all data necessary to plot a graph
        lists of x and y values, color of graph line and label and lable text """
    def __init__(self, x=[], y=[], color=(0, 0, 0), text="", unit=""):
        super().__init__()
        self.x = x
        self.y = y
        self.color = color
        self.text = text
        self.unit = unit
