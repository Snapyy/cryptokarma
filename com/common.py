""" file to stroe global variables and functions shared accross modules """
import os
import json
import re
import smtplib
import yagmail
import socket
from datetime import datetime

from PyQt5.QtWidgets import QMessageBox, QTableWidgetItem
from PyQt5.QtCore import Qt
from functools import wraps
from time import time


# path to this file's directory
PATH = os.path.dirname(os.path.dirname(__file__))

# global dicts, for info about format, see my_portfolio.json
coins = {}
bases = {}
wallets = {}
# portfolio global data like "total invested"
portfolioTotals = {
    "totalInvestedAmount": {},
    "fiatWithdrawnAmount": {},
    "totalInvestedValue": {},
    "fiatWithdrawnValue": {},
    "portfolioValue": {},
    "portfolioChange": {},
}

notifications = {}
# when in auto refresh mode, messageBoxes are turned off, save errors here and send them once a day
notReportedErrors = []

dailyEvents = []
dailyTransactions = []

# global flag that enables/disables poping of messageBoxes (error or info messages)
showMessageBoxes = False

# number of non fiat coins (cryptos)
nrOfNonFiats = 0

OLDEST_TIMESTAMP = 1356998400000    # 1.1.2013

# every time an error message function is run, add 1
# convenient way to track errors in auto refresh mode
nrOfErrorFromAppStart = 0

notificationTargetMail = ""


def timing(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start = time()
        result = f(*args, **kwargs)
        end = time()
        print(f.__name__ + ', Elapsed time: {}'.format(end - start))
        return result
    return wrapper


def loadLastSynced(specifier=None):
    """ Loads the lastSynced dict from json file """
    try:
        with open(PATH + "/user/safefiles/lastSynced.json", "r", encoding="utf-8") as loadedFile:
            # try to decode the file with json format
            loadedData = json.load(loadedFile)
    except FileNotFoundError:
        # file does not exist - try loading the template file (first run)
        try:
            with open(PATH + "/com/templates/lastSyncedTemplate.json", "r", encoding="utf-8") as loadedFile:
                # try to decode the file with json format
                loadedData = json.load(loadedFile)
        except FileNotFoundError:
            # file does not exist - return empty dict
            errorMessage("/com/templates/lastSyncedTemplate.json loading failed", "File does not exist or is corrupted")
            return {}
        except json.JSONDecodeError as e:
            # not a valid JSON format
            errorMessage("/com/templates/lastSyncedTemplate.json loading failed", "JSON file corrupted:\n{}".format(e))
    except json.JSONDecodeError as e:
        # not a valid JSON format
        errorMessage("lastSynced.json loading failed", "JSON file corrupted:\n{}".format(e))

    # return the loaded values
    if specifier is None or specifier not in loadedData:
        return loadedData
    else:
        return loadedData[specifier]


def saveLastSynced(data, specifiers=None):
    """ saves the lastSynced disct to a json file
        args:
            data (dict) - data to save to a file
            specifier (iterable of strings) - dict specifier within the file. (example: "wallet", "price", "BTC"...)
    """
    # if there is any specifier passed, load the last state of lastSynced file
    # and modify the structure of the dict and values defined by specifiers
    # example, specifiers = ("prices", "BTC", "USD")
    # lastSynce["prices"]["BTC"]["USD"] = data
    if specifiers is not None:
        lastSynced = loadLastSynced()
        target = lastSynced
        for specifier in specifiers[:-1]:
            if specifier not in target:
                target[specifier] = {}
            target = target[specifier]
        target[specifiers[-1]] = data
    else:
        lastSynced = data
    # try to open the file for writting
    try:
        with open(PATH + "/user/safefiles/lastSynced.json", "w", encoding="utf-8") as savedFile:
            # dump dictionary to the chosen file
            json.dump(lastSynced, savedFile, indent=4)
    except PermissionError:
        # permission denied (i.e. read only file)
        errorMessage("Saving lastSynced.json failed", "Permission denied \nfile lastSynced.json \n\nhint: \nremove read-only flag", )


def errorMessage(title, text):
    """ Show a messageBox with error message
        args:
            title (str) - messageBox's title
            text (str) - messageBox's text
    """
    global nrOfErrorFromAppStart
    # add 1 error to global counter
    nrOfErrorFromAppStart += 1

    if showMessageBoxes:
        messageBox = QMessageBox(text=text, windowTitle=title)
        messageBox.setIcon(QMessageBox.Warning)
        messageBox.exec_()
    else:
        print("---\n" + "ERROR:\n" + title + "\n---\n" + text + "\n---\n")
        notReportedErrors.append({
            "time": datetime.now(),
            "title": title,
            "text": text,
        })


def infoMessage(title, text):
    """ Show a messageBox with info message
        args:
            title (str) - messageBox's title
            text (str) - messageBox's text
    """
    if showMessageBoxes:
        messageBox = QMessageBox(text=text, windowTitle=title)
        messageBox.setIcon(QMessageBox.Information)
        messageBox.exec_()
    else:
        print("---\n" + "INFO:\n" + title + "\n---\n" + text + "\n---\n")


def sendMailNotification(client, subject, text):
    """ Sends an email if notifications are initialized correctly
        to a target address defined in apiKeys.json
        args:
            client (yagmail client) - initialized with developer mail + password
            subject (str) - mail subject
            text (str) - text of the mail
    """
    global notificationTargetMail

    # target mail must be loaded from apiKeys.json
    if notificationTargetMail:
        try:
            # sending the email
            client.send(to=notificationTargetMail, subject=subject, contents=text)
            print("Notifications email sent successfully (subject: " + subject + ")")
        except (smtplib.SMTPAuthenticationError, yagmail.error.YagInvalidEmailAddress, socket.gaierror) as e:
            print("Notifications email was NOT sent (subject: " + subject + "), reason: " + str(e))
    else:
        print("Can't send notifications, notification target mail is not set (in apiKeys.json)")


def roundTimestampToDay(timestamp):
    if timestamp > 100_000_000_000:
        return int(timestamp / 86_400_000) * 86_400_000
    else:
        return int(timestamp / 86_400) * 86_400


def readApiKeys():
    """ read and return dict with all API keys from json file """
    apiKeys = {}
    try:
        with open(PATH + "/user/apiKeys.json", "r", encoding="utf-8") as loadedFile:
            apiKeys = json.load(loadedFile)
    except FileNotFoundError as e:   # file does not exist - return empty dict
        errorMessage("user/apiKeys.json loading failed", "File not found:\n{}".format(e))
    except json.JSONDecodeError as e:   # not a valid JSON format
        errorMessage("user/apiKeys.json loading failed", "JSON file corrupted:\n{}".format(e))

    return apiKeys


def reasonableStr(x):
    """ edits inputed price, balance or similar to be reasonably long as a string
        args:
            x (int or float) """
    if x > 99_999 or x < -99_999:
        result = "{:,d}".format(int(x))   # 11 222 333  -55 358 879
    elif x == 0:
        result = "0"
    elif -0.000001 < x < 0.000001:
        result = "{:,.9f}".format(x)      # 0.000000123
    elif -0.00001 < x < 0.00001:
        result = "{:,.8f}".format(x)      # 0.00000123
    elif -0.0001 < x < 0.0001:
        result = "{:,.7f}".format(x)      # 0.0000123
    elif -0.001 < x < 0.001:
        result = "{:,.6f}".format(x)      # 0.000123
    elif -1 < x < 1:
        result = "{:,.5f}".format(x)      # 0.12345   0.00355
    else:
        result = "{:,.5g}".format(x)      # 1.2345    1234.5
    # substitute , separators with spaces
    return result.replace(",", " ")


def superfloat(x):
    """ function capable of extracting a float from a string
    compared to normal float function can convert strings with separators or other text like:
    12 512 305.6548 CZK or COVID-19.59 is the worst (as -19.59)"""
    # try to float it normally
    try:
        return float(x)
    # normal float not working
    except ValueError:
        # pattern for finding float numbers in a string
        pattern = r"""                   # doesnt have to be at the start of the string
            [-+]?                       # optional + or - sign
            \s?                         # optional space between +- and the number
            (?:                         # non-capturing group for the ORs
                                        # 1st option - complete float:  25 689.57
                [0-9]+                      # 1+ digits
                (?:[\s]?[0-9]{3})*          # 0+ groups of optional space and 3 digits
                (?:\.[0-9]*)?               # optional group of a decimal point and any number of digits
                |                       # 2nd option - only dot and decimals:   .5694
                (?:\.[0-9]+)                # optional group of a decimal point and any number of digits
            )
                                        # after any of the above number might be exponential format 25.41e-9
            (?:
                [eE]                    # e or E
                [-+]?                   # optional +/- sign
                [0-9]+                  # 1+ numbers
            )?  """                     # the whole exponential group is optional
        rex = re.compile(pattern, re.VERBOSE)
        # find all matces of the pattern in the x string and strip all spaces (there could be thousands separator)
        extractedFloat = rex.findall(x)
        # try to float the first match with classic float again
        try:
            return float(extractedFloat[0].replace(" ", ""))
        # no match was found, return 0,0
        except IndexError:
            return 0.0


class QTableWidgetItemEnhanced(QTableWidgetItem):
    """ enhanced tableWidgetItem has the ability to be sorted as a number (default sorting is alphabetical) """
    def __lt__(self, other):
        # if the items text contaions a digit:
        if "+ ∞" == self.text():
            return False
        if "+ ∞" == other.text():
            return True

        if any(char.isdigit() for char in self.text()):
            # if the item's text is floatable, compare it as numbers, if not compare it as strings = alphabetically
            try:
                return (superfloat(self.data(Qt.EditRole)) < superfloat(other.data(Qt.EditRole)))
            except (ValueError, TypeError):
                return QTableWidgetItem.__lt__(self, other)
        else:
            return QTableWidgetItem.__lt__(self, other)

    def __repr__(self):
        try:
            return "QtableWidgetItemEnhanced, row: {}, column: {}".format(self.row(), self.column())
        except AttributeError:
            super(QTableWidgetItemEnhanced, self).__repr__()


class LoadingError(Exception):
    """ raise this for any error when loading data from remote servers """
