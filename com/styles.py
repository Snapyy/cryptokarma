""" module to store PyQt stylesheets to be used as constants in CryptoKarMa """
from PyQt5.QtGui import QFont, QColor

BASIC = """background-color: rgb(25, 35, 45);
color: rgb(255, 255, 255);"""
HEADER = """background-color: rgb(40, 50, 105);
color: rgb(255, 255, 255);
min-width: 0px;"""
HEADER_SYNCED = """background-color: rgb(150, 200, 255);
color: rgb(0, 0, 0);
min-width: 0px;"""
FIAT = """color: rgb(240, 240, 240);
background-color: rgb(20, 20, 20);"""
TOTAL = """background-color: rgb(192, 193, 255);
color: rgb(0, 0, 0);"""
LOWHIGH_SYNCED = """background-color: rgb(0, 20, 70);
color: rgb(255, 255, 255);"""
PRICE_SYNCED = """background-color: rgb(0, 10, 120);
color: rgb(255, 255, 255);"""
VALUE_SYNCED = """background-color: rgb(64, 0, 97);
color: rgb(255, 255, 255);"""
ALLOCATION_SYNCED = """background-color: rgb(130, 80, 0);
color: rgb(255, 255, 255);"""

HISTO_PRICES_SYNCING = """background-color: rgb(128, 85, 0);
color: rgb(255, 255, 255);"""
HISTO_PRICES_SYNCED_OK = """background-color: rgb(15, 100, 0);
color: rgb(255, 255, 255);"""
HISTO_PRICES_SYNCED_ERROR = """background-color: rgb(100, 0, 0);
color: rgb(255, 255, 255);"""

TRANSACTIONS_SYNCING = """background-color: rgb(210, 160, 70);
color: rgb(0, 0, 0);
min-width: 0px;"""
TRANSACTIONS_SYNCED_OK = """background-color: rgb(15, 100, 0);
color: rgb(255, 255, 255);
min-width: 0px;"""
TRANSACTIONS_SYNCED_ERROR = """background-color: rgb(100, 0, 0);
color: rgb(255, 255, 255);
min-width: 0px;"""
TRANSACTIONS_SYNCED_NOT_POSSIBLE = """background-color: rgb(128, 85, 0);
color: rgb(255, 255, 255);
min-width: 0px;"""

BALANCE_EQUAL = """background-color: rgb(0, 61, 117);
border-color: rgb(176, 228, 231);"""
BALANCE_LOWER = """background-color: rgb(157, 0, 0);
border-color: rgb(201, 120, 184);"""
BALANCE_HIGHER = """background-color: rgb(1, 137, 68);
border-color: rgb(182, 224, 206);"""

CHANGE_NEGATIVE = """background-color: rgb(30, 30, 30);
color: rgb(220, 0, 3);"""
CHANGE_POSITIVE = """background-color: rgb(30, 30, 30);
color: rgb(0, 200, 0);"""
CHANGE_NEUTRAL = """background-color: rgb(30, 30, 30);
color: rgb(200, 150, 100);"""

GAIN_NEGATIVE = """color: rgb(220, 0, 3);"""
GAIN_POSITIVE = """color: rgb(0, 200, 0);"""

PLUS_BUTTON = """background-color: rgb(21, 44, 129);
color: rgb(255, 255, 255);
min-width: 0px;"""
MINUS_BUTTON = """background-color: rgb(80, 0, 0);
color: rgb(255, 255, 255);
min-width: 0px;"""

START_REFRESH = """background-color: rgb(15, 100, 0);
color: rgb(255, 255, 255);"""
STOP_REFRESH = """background-color: rgb(100, 0, 0);
color: rgb(255, 255, 255);"""

CSV_BUTTON_ENABLED = """background-color: rgb(10, 10, 150);
min-width: 0px;
"""

TOTAL_INVESTED_ORIG = """color: rgb(255, 255, 255);
background-color: rgb(16, 29, 70);
border-color: rgb(255, 255, 0); """
TOTAL_INVESTED_CALC = """color: rgb(255, 255, 255);
background-color: rgb(16, 29, 70); """

NOTIF_PRIMARY = """color: rgb(255, 255, 255);
background-color: rgb(60, 50, 180);"""
NOTIF_SECONDARY = """color: rgb(255, 255, 255);
background-color: rgb(80, 30, 180);"""
NOTIF_REMOVE_BUTTON = """background-color: rgb(80, 0, 0);
color: rgb(255, 255, 255);
min-width: 40px;"""
NOTIF_GRAY = """background-color: rgb(30, 30, 30);
color: rgb(255, 255, 255);"""

BRUSH_BALANCE_OK = QColor(0, 100, 0)
BRUSH_BALANCE_NOK = QColor(100, 0, 0)
BRUSH_BALANCE_WARN = QColor(177, 109, 0)

BRUSH_ROI_GREAT = QColor(0, 150, 0)
BRUSH_ROI_GOOD = QColor(60, 90, 0)
BRUSH_ROI_BAD = QColor(150, 40, 0)
BRUSH_ROI_FUCKED = QColor(100, 0, 0)

BRUSH_TRANSACTION_OLD = QColor(40, 10, 120)
BRUSH_TRANSACTION_NEW = QColor(100, 0, 0)

BRUSH_ORDER_AMOUNT_SELL = QColor(80, 0, 0)
BRUSH_ORDER_AMOUNT_BUY = QColor(0, 80, 0)
BRUSH_ORDER_BUY = QColor(60, 0, 0)
BRUSH_ORDER_SELL = QColor(0, 60, 0)

# progressBar colors
PB_BINANCE = "(0,50,50)"
PB_COINBASE_PRO = "(0,0,50)"
PB_TRANSACTIONS = "(0,50,0)"
PB_BALANCES = "(50,0,50)"
PB_VALUES = "(0,0,50)"
PB_PRICES = "(50,50,0)"
PB_ORDERS = "(50,0,0)"

FONT_HEADER = QFont("MS Shell Dlg 2", pointSize=10, weight=75)
FONT_10_SLIM = QFont("MS Shell Dlg 2", pointSize=10, weight=50)
FONT_9_SLIM = QFont("MS Shell Dlg 2", pointSize=9, weight=50)
FONT_8_SLIM = QFont("MS Shell Dlg 2", pointSize=8, weight=50)

# height of cells in myPortfolio
CELL_HEIGHT = 25

# WIDTH of columns of myPortfolio
WALLET_WIDTH = 70
CHANGE_WIDTH = 65
LE_WIDTHS = {
    "Ledger": WALLET_WIDTH + 10,    # kvuli milionum Kč
    "Coinmate": WALLET_WIDTH,
    "Bitfinex": WALLET_WIDTH,
    "Bitfinex": WALLET_WIDTH,
    "Bittrex": WALLET_WIDTH,
    "Binance": WALLET_WIDTH,
    "Coinbase": WALLET_WIDTH,
    "Kucoin": WALLET_WIDTH,
    "Neon": WALLET_WIDTH,
    "Other": WALLET_WIDTH,
    "Fiat": WALLET_WIDTH,
    "Total": 70,
    "Ico": 30,
    "Low": 65,
    "Price": 85,
    "High": 65,
    "Value": 100,
    "Allocation": 75,
    "Ch(24h)": CHANGE_WIDTH,
    "Ch(1d)": CHANGE_WIDTH,
    "Ch(7d)": CHANGE_WIDTH,
    "Ch(1m)": CHANGE_WIDTH,
    "Ch(3m)": CHANGE_WIDTH,
    "Ch(1y)": CHANGE_WIDTH,
}

SYNCED_STYLE = {
    "Low": LOWHIGH_SYNCED,
    "Price": PRICE_SYNCED,
    "High": LOWHIGH_SYNCED,
    "Value": VALUE_SYNCED,
    "Allocation": ALLOCATION_SYNCED,
}
SYNCED_FONT = {
    "Low": FONT_8_SLIM,
    "Price": FONT_9_SLIM,
    "High": FONT_8_SLIM,
    "Value": FONT_9_SLIM,
    "Allocation": FONT_9_SLIM,
}
