from PyQt5.QtWidgets import QProgressBar, QLabel, QSizePolicy
from PyQt5.QtCore import QTimer

import com.styles as styles


class ProgressInformer(object):

    # counts the number of progressBars to set the correct position in parent frame
    counter = 0

    def __init__(self, color, parent):
        super().__init__()
        self.color = color
        self.parent = parent

        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.progressBar = QProgressBar(parent=parent, objectName="progressBar_{}".format(ProgressInformer.counter),
                                        styleSheet="background-color: rgb{};".format(color),
                                        sizePolicy=sizePolicy,
                                        value=0)
        parent.layout().addWidget(self.progressBar, ProgressInformer.counter, 1, 1, 1)

        self.progressLabel = QLabel(parent=parent, objectName="progressLabel_{}".format(ProgressInformer.counter),
                                    styleSheet="background-color: rgb{};".format(color),
                                    sizePolicy=sizePolicy,
                                    font=styles.FONT_10_SLIM)
        parent.layout().addWidget(self.progressLabel, ProgressInformer.counter, 0, 1, 1)

        ProgressInformer.counter += 1

    def setValue(self, value):
        self.progressBar.setValue(value)
        # start the killing process if the procces is completed
        if value == 100:
            self.kill()

    def setText(self, text):
        self.progressLabel.setText(text)

    def kill(self):
        """ set a timer to kill these widgets in X seconds """
        self.killTimer = QTimer(self.parent, singleShot=True)
        self.killTimer.timeout.connect(self._killingTime)
        self.killTimer.start(1 * 500)

    def _killingTime(self):
        """ kill the widgets """
        for widget in [self.progressBar, self.progressLabel]:
            self.parent.layout().removeWidget(widget)
        for widget in [self.progressBar, self.progressLabel, self.killTimer]:
            widget.deleteLater()
            widget = None
        self = None
