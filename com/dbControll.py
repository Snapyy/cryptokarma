import sqlite3
from com.common import errorMessage, roundTimestampToDay


class DbHistory():

    def __init__(self):
        self.connection = sqlite3.connect("user/safefiles/CryptoKarMa.db")
        # row_factory ensures that I can acces data from database columns
        # as index of the column or name of the column
        # FOR EXAMPLE when reading transactions, then
        # transaction[0] is the same as transaction["timestamp"], both exqual to 1605946514000
        # https://stackoverflow.com/a/2526294
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()

    def createTransactionsTable(self):
        with self.connection:
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS transactions (
                timestamp INTEGER,
                date TEXT,
                coin TEXT,
                amount REAL,
                operation TEXT,
                type TEXT,
                wallet TEXT,
                coinOppo TEXT,
                amountOppo REAL,
                btcValue REAL,
                usdValue REAL,
                czkValue REAL,
                eurValue REAL,
                btcPrice REAL,
                usdPrice REAL,
                czkPrice REAL,
                eurPrice REAL,
                UNIQUE(timestamp, coin, amount, coinOppo, amountOppo)
            )
            """)

    def addTransactions(self, transactions):
        """ adds multiple transactions to CryptoKarMa.db/transactions
            args:
                transactions (list of transactions in tuple format) """
        with self.connection:
            self.cursor.executemany("""INSERT OR REPLACE INTO transactions (
                timestamp, date, coin, amount, operation, type, wallet, coinOppo, amountOppo,
                btcValue, usdValue, czkValue, eurValue, btcPrice, usdPrice, czkPrice, eurPrice
            )
            values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
            """, transactions)

    def createPriceHistoryTable(self):
        with self.connection:
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS priceHistory (
                timestamp INTEGER,
                date TEXT,
                base TEXT,
                coin TEXT,
                price REAL,
                UNIQUE(timestamp, base, coin)
            )
            """)

    def addHistoryPrices(self, prices):
        """ adds multiple prices to CryptoKarMa.db/priceHistory
            args:
                prices (list of prices in tuple format) """
        with self.connection:
            self.cursor.executemany("""INSERT OR REPLACE INTO priceHistory (
                timestamp, date, base, coin, price
            )
            values(?,?,?,?,?);
            """, prices)

    def createOpenOrdersTable(self):
        with self.connection:
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS openOrders (
                timestamp INTEGER,
                date TEXT,
                wallet TEXT,
                coinPair TEXT,
                coin TEXT,
                base TEXT,
                type TEXT,
                amountBuy TEXT,
                amountSell TEXT,
                price REAL,
                currentPrice REAL,
                remaining REAL,
                closestPrice REAL,
                closestRemaining REAL,
                UNIQUE(timestamp, wallet, coinPair, amountBuy, amountSell, price)
            )
            """)

    def addOpenOrders(self, openOrders):
        """ adds multiple openOrders to CryptoKarMa.db/openOrders
            args:
                openOrders (list of openOrders in tuple format) """
        with self.connection:
            self.cursor.executemany("""INSERT OR REPLACE INTO openOrders (
                timestamp, date, wallet, coinPair, coin, base, type, amountBuy, amountSell, price,
                currentPrice, remaining, closestPrice, closestRemaining
            )
            values(?,?,?,?,?,?,?,?,?,?,?,?,?,?);
            """, openOrders)

    def readFromDb(self, table, columns, where=None, orderBy=None, rows=None):
        """ read data from sqlite database
            args:
                table (str) - name of the table in db i.e. "priceHistory"
                columns (iterable of strings) - tuple of strings with column names
                    for one column use smthng like ("price",)
                    for all column use "*"
                where (dict) - fills where clause of the sqlite query
                    "operator": "AND" or "OR" or "STR" - then the string is copyed directly from where arg
                    "data" (iterable of strings)
                    example: "operator"="AND", "data" = ("base='BTC'","coin='CZK'")
                       in priceHistory table will result in querring all records with CZK/BTC prices
                orderBy (str) - sting with name of the column and ASC or DESC, i.e: "timestmap DESC"
                rows (int) - nr. of rows to query. All rows are fetched if None
        """
        columns = ",".join(columns)
        orderByStr = " ORDER BY {}".format(orderBy) if orderBy is not None else ""

        # process where argument
        whereStr = ""
        if where is not None:
            # no data means no where string
            if where["data"]:
                # operator = "STR": take the where string directly
                if "operator" in where and where["operator"] == "STR":
                    whereStr = " WHERE " + where["data"]
                # operator = "AND" or "OR": create the where string
                else:
                    whereStr = " WHERE " + " {} ".format(where["operator"]).join(where["data"])

        with self.connection:
            self.cursor.execute("""SELECT {} FROM {}{}{}""".format(columns, table, whereStr, orderByStr))
            if rows is None:
                return self.cursor.fetchall()
            else:
                return self.cursor.fetchmany(rows)

    def loadSpecificPrice(self, timestamp, coin, base):
        """ return 1 specific price of a pair coin/base at a specific timestamp """
        priceToBase = self.readFromDb(
            table="priceHistory",
            columns=("price",),
            where={
                "operator": "AND",
                "data": (
                    "timestamp={}".format(roundTimestampToDay(timestamp / 1000)),
                    "base='{}'".format(base),
                    "coin='{}'".format(coin)
                )
            }
        )
        if priceToBase:
            return priceToBase[0]["price"]
        else:
            return 0

    def deleteSomething(self, table, where=None):
        """ delete specified data from sqlite database
            args:
                table (str) - name of the table in db i.e. "priceHistory"
                where (dict) - fills where clause of the sqlite query
                    "operator": "AND" or "OR" or "STR" - then the string is copyed directly from where arg
                    "data" (iterable of strings)
                    example: "operator"="AND", "data" = ("base='BTC'","coin='CZK'")
                       in priceHistory table will result in querring all records with CZK/BTC prices
        """
        whereStr = ""
        # process where argument
        if where is not None:
            # no data means no where string
            if where["data"]:
                # operator = "STR": take the where string directly
                if "operator" in where and where["operator"] == "STR":
                    whereStr = " WHERE " + where["data"]
                # operator = "AND" or "OR": create the where string
                else:
                    whereStr = " WHERE " + " {} ".format(where["operator"]).join(where["data"])

        with self.connection:
            try:
                self.cursor.execute("""DELETE FROM {} {}""".format(table, whereStr))
            except sqlite3.OperationalError as e:
                errorMessage("Unable to execute SQLite command", "CryptoKarMa.db might be locked locked. " + str(e))

    def deleteSyncedTransactions(self):
        """ deletes all type-synced transactions from CryptoKarMa.db """
        with self.connection:
            try:
                self.cursor.execute("""DELETE FROM transactions WHERE type='synced'""")
            except sqlite3.OperationalError as e:
                errorMessage("Unable to execute SQLite command", "CryptoKarMa.db might be locked locked. " + str(e))

    def deleteCsvTransactions(self):
        """ deletes all type-csv transactions from CryptoKarMa.db """
        with self.connection:
            try:
                self.cursor.execute("""DELETE FROM transactions WHERE type='csv'""")
            except sqlite3.OperationalError as e:
                errorMessage("Unable to execute SQLite command", "CryptoKarMa.db might be locked locked. " + str(e))
