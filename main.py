from PyQt5.QtWidgets import QApplication
import sys
import qdarkstyle

from CryptoKarMa import CryptoKarMa_mainWindow


def main():

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    CryptoKarMa_mainWindow()
    # standart win.show() is used inside CryptoKarMa_mainWindow

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
