import re
import os
import json
import copy
import operator
import requests
import webbrowser
from datetime import datetime
from dateutil.relativedelta import relativedelta

from PyQt5.QtWidgets import QSizePolicy, QPushButton, QLabel, QLineEdit, QSpacerItem, QApplication, QInputDialog
from PyQt5.QtGui import QRegExpValidator, QPixmap
from PyQt5.QtCore import QSize, Qt, QRegExp, QEvent, QObject, QThread, pyqtSignal

from exchanges.fiats import Fiats
from exchanges.cryptoCompare import LoadHistPricesThread

import tabs.notifications as notifications
from com import common
from com.common import (reasonableStr, errorMessage, infoMessage, LoadingError,
                        saveLastSynced, superfloat, roundTimestampToDay,
                        sendMailNotification)
import com.styles as styles
from com.progressInformer import ProgressInformer


class MyPortfolio(QObject):

    SPACE_FOR_NEW_CRYPTOS = 100
    DATA_HEADERS = ["Total", "Ico", "Low", "Price", "High", "Value", "Allocation",
                    "Ch(24h)", "Ch(1d)", "Ch(7d)", "Ch(1m)", "Ch(3m)", "Ch(1y)"]

    def __init__(self, name, humanName, ui, wallets, cryptoCompare, dbHistory, yagMail):
        super().__init__()
        self.name = name
        self.humanName = humanName
        self.ui = ui
        self.walletClients = wallets
        self.cryptoCompare = cryptoCompare
        self.dbHistory = dbHistory
        self.yagMail = yagMail

        self.fiats = Fiats()

        self.threads = []
        # show the app window in its current state
        QApplication.processEvents()

        self._initMyPortfolio()

    def _initMyPortfolio(self):
        """ Loads data from my_portfolio.json and saves them to global dictionaries
        Initializes my_portfolios object variables
        Calls GUI initialization function
        """
        # load data from my_portfolio.json
        # _coinsStartup is loaded at app startup - is used to show values from the last save
        # for info about format, see my_portfolio.json/coins
        common.bases, savedWallets, self._coinsStartup, common.portfolioTotals = self._loadMyPortfolioFile()
        # copy _coinsStartup to global dictionary which will be changed during program run (_coinsStartup stays the same til saved)
        common.coins = copy.deepcopy(self._coinsStartup)
        # initialize common dict of wallet according to initialized wallet clients
        common.wallets = {}
        for walletName, client in self.walletClients.items():
            common.wallets[walletName] = True if hasattr(client, "getOpenOrders") else False

        # add new wallets to coins dictionary
        for wallet in common.wallets:
            for coin in common.coins:
                if wallet not in common.coins[coin]["column"]:
                    common.coins[coin]["column"][wallet] = 0

        # save this state after init as startup state of the portfolio
        self.saveMyPortfolioValues(startup=True, infoMessageOn=False)

        # calculate the number of nonFiat coins
        for coin in common.coins.values():
            if coin["isCrypto"] is True:
                common.nrOfNonFiats += 1

        # dictionary of flags with information whether data from each column are synchronized or not
        # pairs "columnName": True/False
        self._columnSynchronized = {}
        # initialize all flags to synchronized:False
        for column in list(common.wallets.keys()) + MyPortfolio.DATA_HEADERS:
            self._columnSynchronized[column] = False

        # flag that historical changes were loaded
        self._oldChangesLoaded = False
        # flag that descending sorting was done
        self._sortedDescending = False

        # create GUI
        self._initMyPortfolioGUI()

        # show the app window in its current state
        QApplication.processEvents()

        # start syncing daily prices (in other thread)
        self._syncHistoricalPrices()

        # refreshes values and balances
        self.refreshAllClicked()

        # hide low balances as default
        self.showLowBalancesChanged(False)

    def _loadMyPortfolioFile(self):
        """ Loads and return last saved data from myPortfolio.json """
        # try to open the file for reading
        try:
            with open(common.PATH + "/user/safefiles/myPortfolio.json", "r", encoding="utf-8") as loadedFile:
                # try to decode the file with json format
                loadedData = json.load(loadedFile)
        except (FileNotFoundError, TypeError):
            try:
                with open(common.PATH + "/com/templates/myPortfolioTemplate.json", "r", encoding="utf-8") as loadedFile:
                    # try to decode the file with json format
                    loadedData = json.load(loadedFile)
            except (FileNotFoundError, TypeError):
                errorMessage("File loading error", "/com/templates/myPortfolioTemplate.json does not exist or is corrupted")
            except json.JSONDecodeError as e:
                errorMessage("File loading error", "/com/templates/myPortfolioTemplate.json file corrupted:\n{}".format(e))
        except json.JSONDecodeError as e:
            # not a valid JSON format
            errorMessage("File loading error", "/user/safefiles/myPortfolio.json file corrupted:\n{}".format(e))

        # add necessary fields if not created already
        for base, data in loadedData["bases"]["options"].items():
            if "rate" not in data:
                data["rate"] = {base: 1 for base in loadedData["bases"]["options"]}

        if "wallets" not in loadedData:
            loadedData["wallets"] = []

        if "totals" not in loadedData:
            loadedData["totals"] = {}
        for total in [
            "totalInvestedAmount",
            "totalInvestedValue",
            "fiatWithdrawnAmount",
            "fiatWithdrawnValue",
            "portfolioValue",
            "portfolioChange",
            "portfolioValueFiat",
            "portfolioValueWithFiat",
        ]:
            if total not in loadedData["totals"]:
                loadedData["totals"][total] = {base: 0 for base in loadedData["bases"]["options"]}

        loadedData["totals"]["portfolioValueDailyHigh"] = {base: 0 for base in loadedData["bases"]["options"]}
        loadedData["totals"]["portfolioValueDailyLow"] = {base: 9999999999999 for base in loadedData["bases"]["options"]}

        # return the loaded values
        return loadedData["bases"], loadedData["wallets"], loadedData["coins"], loadedData["totals"]

    def _initMyPortfolioGUI(self):
        """ initialize all the GUI object for this tab"""
        # fill base currency comboBox with loaded base coins
        for base in common.bases["options"]:
            self.ui.comboBox_baseCurrencies.addItem(base)

        # parents of editor-created widgets of my_portfolio
        parentR = self.ui.scrollAreaWidget_myPortfolio
        parentL = self.ui.scrollAreaWidget_myPortfolioCurrencies

        # dictionary with reference to widgets in the myPortfolio table
        self._widgets = {}
        self._widgets["Header"] = {}

        self._createColumnHeaders()
        self._createRowHeaders(common.coins, 1)
        self._createDataWidgets(common.coins, 1)

        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        # add a push button SAVE
        self.pushButton_save = QPushButton(parent=parentL, text="Save", font=styles.FONT_HEADER,
                                           minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                           sizePolicy=sizePolicy,
                                           styleSheet=styles.MINUS_BUTTON)
        parentL.layout().addWidget(self.pushButton_save, 0, 0, 1, 2)
        self.pushButton_save.clicked.connect(self.saveMyPortfolioValues)

        # add a push button PLUS between all crypto and fiat buttons (headers)
        self.pushButton_addCrypto = QPushButton(parent=parentL, text="+", font=styles.FONT_HEADER,
                                                minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                                sizePolicy=sizePolicy,
                                                styleSheet=styles.PLUS_BUTTON)
        parentL.layout().addWidget(self.pushButton_addCrypto, 1 + len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS, 0, 1, 1)
        self.pushButton_addCrypto.clicked.connect(self._addCryptoClicked)

        # add a push button MINUS between all crypto and fiat buttons (headers)
        self.pushButton_removeCrypto = QPushButton(parent=parentL, text="-", font=styles.FONT_HEADER,
                                                   minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                                   sizePolicy=sizePolicy,
                                                   styleSheet=styles.MINUS_BUTTON)
        parentL.layout().addWidget(self.pushButton_removeCrypto, 1 + len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS, 1, 1, 1)
        self.pushButton_removeCrypto.clicked.connect(self._removeCryptoClicked)

        # and fixed spacer on the corresponding (to the plus button) space on the right scrollarea
        spacerItem = QSpacerItem(0, styles.CELL_HEIGHT, QSizePolicy.Minimum, QSizePolicy.Fixed)
        parentR.layout().addItem(spacerItem, 1 + len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS, 0, 1, 1)

        self._widgets["Total"] = {}
        # add a push button total at the bottom as a vertical header
        self.pushButton_totalWallet = QPushButton(parent=parentL, text="Total",
                                                  minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                                  sizePolicy=sizePolicy,
                                                  font=styles.FONT_HEADER,
                                                  styleSheet=styles.PLUS_BUTTON)
        parentL.layout().addWidget(
            self.pushButton_totalWallet,
            1 + 2 * len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 1, 0, 1, 2
        )

        # create lineEdits for total wallet value row
        for columnIdx, column in enumerate(common.wallets):
            width = styles.LE_WIDTHS[column] if column in styles.LE_WIDTHS else styles.WALLET_WIDTH
            lineEdit = QLineEdit(parent=parentR, sizePolicy=sizePolicy, alignment=Qt.AlignRight, readOnly=True,
                                 minimumSize=QSize(width, styles.CELL_HEIGHT),
                                 maximumSize=QSize(width + 10, 6969),
                                 objectName="lineEdit_myPortfolio_{}_{}".format("total", column),
                                 font=styles.FONT_8_SLIM, styleSheet=styles.TOTAL)
            parentR.layout().addWidget(lineEdit, 1 + 2 * len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 1, columnIdx, 1, 1)
            # add a this widget in widget reference dictionary
            self._widgets["Total"][column] = lineEdit

        # vertical spacer at the bottom to shrink all table object to minimal height
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)
        # add to the left and right scroll area
        parentR.layout().addItem(spacerItem, 1 + 2 * len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 2, 0, 1, 1)
        parentL.layout().addItem(spacerItem, 1 + 2 * len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 2, 0, 1, 1)
        # and insert also small fixed spacer at the bottom of the left area to compensate for the horizontal scroll bar on the right
        spacerItem = QSpacerItem(0, 20, QSizePolicy.Minimum, QSizePolicy.Fixed)
        parentL.layout().addItem(spacerItem, 1 + 2 * len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 3, 0, 1, 1)
        # finally expanding space after the last column especially for the view when wallets are hidden
        # expanding spacer at the bottom to shrink all table object to minimal height
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Fixed)
        parentR.layout().addItem(spacerItem, 0, len(list(common.wallets.keys()) + MyPortfolio.DATA_HEADERS), 1, 1)

        # recalculates the whole portfolio based on amounts in all wallets
        self.recalculateAllTotals()

        # load base currency that was set during the last save
        self.ui.comboBox_baseCurrencies.setCurrentText(common.bases["current"])

        # connect events to functions
        self.ui.scrollArea_myPortfolio.verticalScrollBar().valueChanged.connect(
            lambda value, w=self.ui.scrollArea_myPortfolio: self.scrollBarsConnection(value, w)
        )
        self.ui.scrollArea_myPortfolioCurrencies.verticalScrollBar().valueChanged.connect(
            lambda value, w=self.ui.scrollArea_myPortfolioCurrencies: self.scrollBarsConnection(value, w)
        )
        self.ui.pushButton_refreshWallets.clicked.connect(lambda _: self.refreshWalletsClicked())
        self.ui.pushButton_refreshValues.clicked.connect(lambda _: self.refreshValuesClicked())
        self.ui.pushButton_refreshAll.clicked.connect(lambda _: self.refreshAllClicked())
        self.ui.comboBox_baseCurrencies.currentTextChanged.connect(lambda text: self.currentBaseChanged(text))
        self.ui.checkBox_lowBalances.clicked.connect(lambda checked: self.showLowBalancesChanged(checked))
        self.ui.checkBox_detailView.clicked.connect(lambda checked: self.showWalletsChanged(checked, ["Ch(3m)", "Ch(1y)", "Low", "High"]))
        self.ui.pushButton_syncHistoricalPrices.clicked.connect(lambda _: self._syncHistoricalPrices())
        self.ui.pushButton_refreshAllPrices.clicked.connect(lambda _: self._refreshAllPrices())
        self.ui.pushButton_openMyPortfolioJson.clicked.connect(lambda _: self._openMyPortfolioJson())
        self.ui.pushButton_openUserFolder.clicked.connect(lambda _: self._openUserFolder())

        # show all coins until the refresh All is done
        self.showLowBalancesChanged(True)

    def _createColumnHeaders(self):
        """ create header widgets for all wallets and other horizontal headers """
        parentR = self.ui.scrollAreaWidget_myPortfolio

        for columnIdx, headerName in enumerate(list(common.wallets.keys()) + MyPortfolio.DATA_HEADERS):
            pushButton_horHeader = QPushButton(parent=parentR, objectName="button_myPortfolio_{}_{}".format(0, headerName),
                                               text=headerName,
                                               font=styles.FONT_HEADER, styleSheet=styles.HEADER,
                                               sizePolicy=QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred))
            parentR.layout().addWidget(pushButton_horHeader, 0, columnIdx, 1, 1)
            # connect event to slot
            if headerName in common.wallets:
                pushButton_horHeader.clicked.connect(lambda _, h=headerName: self.walletClicked(h))
            elif headerName in ["Value", "Allocation", "Ch(24h)", "Ch(1d)", "Ch(7d)", "Ch(1m)", "Ch(3m)", "Ch(1y)"]:
                pushButton_horHeader.clicked.connect(lambda _, h=headerName: self.headerClickedSorting(h))
            # add a this widget in widget reference dictionary
            self._widgets["Header"][headerName] = pushButton_horHeader

        # add tooltips to differenciate 24h and 1d changes
        for headerName in ["Ch(24h)", "Ch(1d)"]:
            self._widgets["Header"][headerName].setText(self._widgets["Header"][headerName].text() + " 🛈")
        self._widgets["Header"]["Ch(24h)"].setToolTip("Change since this time the previous day")
        self._widgets["Header"]["Ch(1d)"].setToolTip("Change since the beginning of this day (00:00 GMT time)")

    def _createRowHeaders(self, newCoins, firstRow):
        """ create vertical header widgets from coin names """
        parentL = self.ui.scrollAreaWidget_myPortfolioCurrencies

        for rowIdx, coin in enumerate(newCoins):
            pushButton_verHeader = QPushButton(parent=parentL, objectName="button_myPortfolio_{}_{}".format(coin, 0),
                                               text=coin,
                                               font=styles.FONT_HEADER,
                                               minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                               sizePolicy=QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum))
            # distinguish betweem crypto and fiat headers
            if common.coins[coin]["isCrypto"] is False:
                # fiat header have different style and leave a space for a plus button
                pushButton_verHeader.setStyleSheet("min-width: 0px;" + styles.FIAT)
                parentL.layout().addWidget(
                    pushButton_verHeader,
                    firstRow + len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 1 + rowIdx, 0, 1, 2
                )
            else:
                # crypto headers are inserted normally in order
                pushButton_verHeader.setStyleSheet(styles.HEADER)
                parentL.layout().addWidget(pushButton_verHeader, firstRow + rowIdx, 0, 1, 2)

            # add a dictionary for every coin in widget reference dictionary
            # and add a this widget in widget reference dictionary
            self._widgets[coin] = {
                "Header": pushButton_verHeader
            }

    def _createDataWidgets(self, newCoins, firstRow):
        """ fill the whole myPortfolio table with proper lineEdits """
        parentR = self.ui.scrollAreaWidget_myPortfolio
        for rowIdx, coin in enumerate(newCoins):
            for columnIdx, column in enumerate(list(common.wallets.keys()) + MyPortfolio.DATA_HEADERS):
                # for Ico column, dont create lineEdits but label with icon as pixmap
                if column == "Ico":
                    widget = QLabel(parent=parentR, sizePolicy=QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed),
                                    minimumSize=QSize(styles.CELL_HEIGHT, styles.CELL_HEIGHT),
                                    maximumSize=QSize(styles.CELL_HEIGHT, styles.CELL_HEIGHT),
                                    scaledContents=True)
                    widget.setPixmap(QPixmap("icons/{}.png".format(coin)))
                # for all other columns
                else:
                    width = styles.LE_WIDTHS[column] if column in styles.LE_WIDTHS else styles.WALLET_WIDTH
                    widget = QLineEdit(parent=parentR, objectName="lineEdit_myPortfolio_{}_{}".format(coin, column),
                                       sizePolicy=QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred),
                                       alignment=Qt.AlignRight,
                                       minimumSize=QSize(width, styles.CELL_HEIGHT),
                                       maximumSize=QSize(width + 10, 6969),
                                       font=styles.FONT_8_SLIM, styleSheet=styles.BASIC)
                    # set validator to any decimal number, + or -  Decimal point might be . or ,
                    widget.setValidator(QRegExpValidator(QRegExp("[+-]?\\d*[\\.]?\\d+")))

                    # for all NON wallet columns, set read only
                    if not columnIdx < len(common.wallets):
                        widget.setReadOnly(True)

                    # special formatting and slot assigning for different columns and their events
                    if column in ["Low", "High"]:
                        widget.setFont(styles.FONT_8_SLIM)
                    # for wallet, change and allocation columns:
                    elif column in common.wallets or column in ["Allocation", "Ch(24h)", "Ch(1d)", "Ch(7d)",
                                                                "Ch(1m)", "Ch(3m)", "Ch(1y)"]:
                        widget.setText(str(common.coins[coin]["column"][column]))
                        widget.textChanged.connect(lambda text, c=coin, col=column: self.amountChanged(text, col, c))
                    elif column == "Total":
                        widget.textChanged.connect(lambda text, c=coin, w=widget: self.totalChanged(text, c, w))
                        widget.setStyleSheet(styles.TOTAL)
                        widget.setFont(styles.FONT_10_SLIM)
                    elif column == "Price":
                        widget.textChanged.connect(lambda text, c=coin: self.priceChanged(text, c))
                    elif column == "Value":
                        widget.textChanged.connect(lambda text, c=coin: self.valueChanged(text, c))

                # add a this widget in widget reference dictionary
                self._widgets[coin][column] = widget
                # distinguish wdget position and style between crypto and fiat lineEdits
                if common.coins[coin]["isCrypto"] is False:
                    # fiat cells leave a space for a plus button
                    parentR.layout().addWidget(
                        widget,
                        firstRow + len(common.coins) + MyPortfolio.SPACE_FOR_NEW_CRYPTOS + 1 + rowIdx, columnIdx, 1, 1
                    )
                    widget.setStyleSheet(styles.FIAT)
                else:
                    # crypto headers are inserted normally in order
                    parentR.layout().addWidget(widget, firstRow + rowIdx, columnIdx, 1, 1)

    def saveMyPortfolioValues(self, startup=False, infoMessageOn=True):
        """ saves values from my portfolio table to a file """
        # try to open the file for writting
        try:
            with open(common.PATH + "/user/safefiles/myPortfolio.json", "w", encoding="utf-8") as savedFile:
                # create a dictionary to be dumped to json file
                savedData = {}
                savedData["bases"] = common.bases
                savedData["wallets"] = common.wallets
                savedData["coins"] = common.coins
                savedData["totals"] = common.portfolioTotals
                # dump this dictionary to the chosen file
                json.dump(savedData, savedFile, indent=4)
            # update startup coin dict according to these new saved values
            self._coinsStartup = copy.deepcopy(common.coins)
            # skip this step if this is a startup call of this function
            if startup is False:
                # resynchronize already synchronized wallets again
                for wallet in common.wallets:
                    if self._columnSynchronized[wallet] is True:
                        self._columnSynchronized[wallet] = False
                        self.walletClicked(wallet)

            # show messageBox with succesfully saved
            if infoMessageOn:
                infoMessage("Successfully saved", "My portfolio data saved")
        except PermissionError:
            # permission denied (i.e. read only file)
            errorMessage("Permission error",
                         "Trying to save myPortfolio.json resulted in Permission error\n\nhint: \nremove read-only flag")

    def scrollBarsConnection(self, value, widget):
        """ connects vertical scrollBars from ScrollAreas so they both scroll together """
        # get widget2 - the one which will be moved
        if widget is self.ui.scrollArea_myPortfolioCurrencies:
            widget2 = self.ui.scrollArea_myPortfolio
        else:
            widget2 = self.ui.scrollArea_myPortfolioCurrencies
        # block signals ang move the scrollBar
        widget2.blockSignals(True)
        widget2.verticalScrollBar().setValue(value)
        widget2.blockSignals(False)

######################################################################################################
#                _     ___    _    ____       _    ____ ___   ____    _  _____  _
#               | |   / _ \  / \  |  _ \     / \  |  _ \_ _| |  _ \  / \|_   _|/ \
#               | |  | | | |/ _ \ | | | |   / _ \ | |_) | |  | | | |/ _ \ | | / _ \
#               | |__| |_| / ___ \| |_| |  / ___ \|  __/| |  | |_| / ___ \| |/ ___ \
#               |_____\___/_/   \_\____/  /_/   \_\_|  |___| |____/_/   \_\_/_/   \_\          ivrit
######################################################################################################

    def loadBaseCurrencyRates(self, bases):
        """ refreshes rates between base coins and saves them in global dictionary """
        # loads all available prices for the current base currency
        for base in bases["options"]:
            # loads all available prices for the current base currency
            loadedData = self.cryptoCompare.getCurrentPrices(base, common.coins, ["PRICE"])
            # if some data were loaded
            if len(loadedData["PRICE"]) != 0:
                # go through all loaded coins
                for coin, price in loadedData["PRICE"].items():
                    # if this coin is also a base coin
                    if coin in bases["options"]:
                        # save its rate to the global dictionary
                        bases["options"][base]["rate"][coin] = price

    def loadCrCmpCurrentData(self, base):
        """ fills all columns from conversion tablecurrent data from APIs
        if no data are loaded from API, it fills saved prices form a file """
        # information name in CryptoCompare api and CryptoKarMa header name
        CONVERSION_TABLE = {
            "PRICE": "Price",
            "LOW24HOUR": "Low",
            "HIGH24HOUR": "High",
            "CHANGEPCT24HOUR": "Ch(24h)",
        }

        # loads all available prices for the current base currency
        loadedData = self.cryptoCompare.getCurrentPrices(base, common.coins, CONVERSION_TABLE)
        # if some data were loaded
        if len(loadedData["PRICE"]) != 0:
            # go through all the data from conversion tables
            for CrComp, CrKarMa in CONVERSION_TABLE.items():
                # for every coin
                for coin, extractedValue in loadedData[CrComp].items():
                    currentCell = self._widgets[coin][CrKarMa]
                    # for change and price column set different format
                    if CrKarMa == "Ch(24h)":
                        currentCell.setText("{:,.2f} %".format(extractedValue))
                    elif CrKarMa == "Price":
                        currentCell.setText(reasonableStr(extractedValue) + " " + common.bases["options"][base]["symbol"])
                    else:
                        currentCell.setText(reasonableStr(extractedValue))
            # set style to all relevant columns
            self.syncedStyling(base, ["Price", "Low", "High", "Value", "Allocation", "Ch(24h)"])
            # fill data for constant cells for this bas
            self.fillSameCoin(base)
        # if no data were loaded
        else:
            # load at least prices from the saved file (needed for other columns to work properly)
            for coin in common.coins:
                currentCell = self._widgets[coin]["Price"]
                currentCell.setText(reasonableStr(common.coins[coin]["column"]["Price"])
                                    + " " + common.bases["options"][base]["symbol"])

    def syncedStyling(self, base, columns):
        """ stylesheet and format changes after data synchronization """
        # for every inputed column
        for column in columns:
            # set header to synced
            self._widgets["Header"][column].setStyleSheet(styles.HEADER_SYNCED)
            # go through every currency
            for coin in common.coins:
                # for columns with defined stylesheets
                if column in styles.SYNCED_STYLE:
                    # PRICE, LOW, HIGH, VALUE, ALLOCATIOn
                    if common.coins[coin]["isCrypto"] is True:
                        self._widgets[coin][column].setStyleSheet(styles.SYNCED_STYLE[column])
                        self._widgets[coin][column].setFont(styles.SYNCED_FONT[column])
                # changes columns
                else:
                    text = self._widgets[coin][column].text()
                    # different style for positive and negative change
                    if superfloat(text) == 0:
                        self._widgets[coin][column].setStyleSheet(styles.CHANGE_NEUTRAL)
                    elif superfloat(text) < 0:
                        self._widgets[coin][column].setStyleSheet(styles.CHANGE_NEGATIVE)
                    else:
                        self._widgets[coin][column].setStyleSheet(styles.CHANGE_POSITIVE)

    def fillSameCoin(self, coin):
        """ fill cells that correspond to a pair of the same coin (CZK-CZK values)"""
        self._widgets[coin]["Price"].setText(reasonableStr(1) + " " + common.bases["options"][coin]["symbol"])
        self._widgets[coin]["Low"].setText(reasonableStr(1))
        self._widgets[coin]["High"].setText(reasonableStr(1))
        self._widgets[coin]["Ch(24h)"].setText("0.00 %")
        self._widgets[coin]["Ch(1d)"].setText("0.00 %")
        self._widgets[coin]["Ch(7d)"].setText("0.00 %")
        self._widgets[coin]["Ch(1m)"].setText("0.00 %")
        self._widgets[coin]["Ch(3m)"].setText("0.00 %")
        self._widgets[coin]["Ch(1y)"].setText("0.00 %")

########################################################################################################################################
#               _   _ _     _                                _
#              | | | (_)___| |_ ___  _ __ _   _   _ __  _ __(_) ___ ___  ___
#              | |_| | / __| __/ _ \| '__| | | | | '_ \| '__| |/ __/ _ \/ __|
#              |  _  | \__ \ || (_) | |  | |_| | | |_) | |  | | (_|  __/\__ \
#              |_| |_|_|___/\__\___/|_|   \__, | | .__/|_|  |_|\___\___||___/
#                                         |___/  |_|
########################################################################################################################################

    def _refreshAllPrices(self):
        """ erases all timestamps from lastSynced.json/prices
            and performs the price syncing """
        saveLastSynced({}, ("prices", ))
        self._syncHistoricalPrices()

    def _priceLoadingProgressChanged(self, progress):
        """ progress changed in price loading thread"""
        # update progress informer
        self.progressInfo_prices.setText("Loading historical daily prices (CryptoCompare)...")
        self.progressInfo_prices.setValue(progress)
        QApplication.processEvents()

    def _histPricesLoadingDone(self, newPrices, error):
        """ thread with loading historical prices finished
            and emited them to this function
            args:
                newPrices(dict) - {base: {coin: {timestamp(int 10-bit): price(float)}}}, timestamp is rounded to 1 day
                error(bool) - False if no error occured during loading all historical prices, otherwise True """
        self.progressInfo_prices.setText("Loading historical daily prices (CryptoCompare)...DONE")
        self.progressInfo_prices.setValue(100)
        # save all new prices to database
        newPricesToDb = []
        for base, baseContent in newPrices.items():
            for coin, coinContent in baseContent.items():
                for timestamp, price in coinContent.items():
                    date = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d')
                    newPricesToDb.append((timestamp, date, base, coin, price))
        # if there is at least one price loaded
        if newPricesToDb:
            self.dbHistory.addHistoryPrices(newPricesToDb)

        # set color of the sync prices button to indicate result of the sync
        if error:
            self.ui.pushButton_syncHistoricalPrices.setStyleSheet(styles.HISTO_PRICES_SYNCED_ERROR)
        else:
            self.ui.pushButton_syncHistoricalPrices.setStyleSheet(styles.HISTO_PRICES_SYNCED_OK)

        # get the name of the currently selected base currency
        currentBase = common.bases["current"]
        # recalculate longer changes
        self._recalculateChanges(currentBase)

    def _syncHistoricalPrices(self):
        """Loads all daily prices that are not yet in the database
            (based on timestamps from lastSynced.json/"prices")
            and saves them to CryptoKarMa.db
        """
        # initialize new progressInformer
        self.progressInfo_prices = ProgressInformer(styles.PB_PRICES, self.ui.frame_progressInformers)
        # change style of the button to indicate syncing in progress
        self.ui.pushButton_syncHistoricalPrices.setStyleSheet(styles.HISTO_PRICES_SYNCING)
        # start a thread to load all historical data
        # self. must be here so the thread live while mainWindow lives
        self.histPricesLoadingThread = LoadHistPricesThread(common.bases["options"], common.coins)
        self.histPricesLoadingThread.progressChanged.connect(lambda progress: self._priceLoadingProgressChanged(progress))
        self.histPricesLoadingThread.loadingDone.connect(
            lambda historicalPrices, error: self._histPricesLoadingDone(historicalPrices, error))
        self.histPricesLoadingThread.start()

    def _recalculateChanges(self, base):
        """ calculates changes of the current price to historical prices (1d, 7d, 1m...)
            args:
                base(str) - is done only for the current base"""

        # initialize historical changes to have the same structure as historicalPrices

        changeTimestamps = {
            "Ch(1d)": roundTimestampToDay(int(datetime.timestamp(datetime.utcnow())) - 86_400),
            "Ch(7d)": roundTimestampToDay(int(datetime.timestamp(datetime.utcnow())) - 604_000),
            "Ch(1m)": roundTimestampToDay(int(datetime.timestamp(datetime.utcnow() - relativedelta(months=1)))),
            "Ch(3m)": roundTimestampToDay(int(datetime.timestamp(datetime.utcnow() - relativedelta(months=3)))),
            "Ch(1y)": roundTimestampToDay(int(datetime.timestamp(datetime.utcnow() - relativedelta(years=1)))),
        }

        for coin in common.coins:
            for change, timestamp in changeTimestamps.items():
                # get all base prices from prices DB
                historicalPrice = self.dbHistory.readFromDb(
                    table="priceHistory",
                    columns=("price",),
                    where={
                        "operator": "AND",
                        "data": ("base='{}'".format(base), "timestamp={}".format(timestamp), "coin='{}'".format(coin)),
                    },
                )

                # price might not be in the database
                if historicalPrice:
                    historicalPrice = historicalPrice[0]["Price"]

                    currentPrice = common.coins[coin]["column"]["Price"]

                    try:    # current price might be 0 (especially in BTC prices)
                        # change = (current price - historical price) / historical price * 100 (%)
                        changeValue = (currentPrice - historicalPrice) / historicalPrice * 100
                        # load the change into proper cell
                        self._widgets[coin][change].setText("{:,.2f} %".format(changeValue))
                    except ZeroDivisionError:
                        self._widgets[coin][change].setText("xxx")
                else:
                    self._widgets[coin][change].setText("xxx")

        # set style to all relevant columns
        self.syncedStyling(base, changeTimestamps)
        # fill data for constant cells for this bas
        self.fillSameCoin(base)
        # recalculate daily gain
        self.recalculateDailyGain(common.bases["current"])

######################################################################################################
#                _______     __ ____ _   _ _____ ____
#               | ____\ \   / / ____| \ | |_   _/ ___|
#               |  _|  \ \ / /|  _| |  \| | | | \___ \
#               | |___  \ V / | |___| |\  | | |  ___) |
#               |_____|  \_/  |_____|_| \_| |_| |____/
######################################################################################################

    def tabGotFocus(self):
        """ tab myPortfolio just got focus"""
        # refresh portfolio totals like total value, invested, gains...
        self.recalculatePortfolioTotals(common.bases["current"])

    def eventFilter(self, source, event):
        """ function to catch pyqt events i.e. when hovering mouse over a lineEdit
            args:
                source - pyqt widget that triggered the event
                event - event object """
        # react on event mouse hovered over one of the lineEdit widgets
        if event.type() == QEvent.HoverEnter and "lineEdit_myPortfolio_" in source.objectName():
            self.mouseOverNewBallanceEnter(source)
            return True     # event was proccessed, dont proccess it again
        # mouse left the lineEdit
        if event.type() == QEvent.HoverLeave and "lineEdit_myPortfolio_" in source.objectName():
            self.mouseOverNewBallanceLeave(source)
            return True
        # if the event was not captured by above, return it unchanged for further processing outside event filter
        return super(MyPortfolio, self).eventFilter(source, event)

    def mouseOverNewBallanceEnter(self, lineEdit):
        """ mouse hover over a line edith with wallet balance which changed since the last saving of the file
            args:
                lineEdit - pyqt widget that triggered the event"""
        # remember the current stylesheet of the lineEdit to restore it in LeaveEvent
        self.styleSheetMemory = lineEdit.styleSheet()
        # get name of the object that is hovered
        name = lineEdit.objectName()
        # extract currency and column from the widget name
        pattern = re.compile(r"lineEdit_myPortfolio_(.+)_(.+)")
        coin, column = pattern.findall(name)[0]

        # remember the current value because it will be overwritten by setText function and its event
        currentMemory = common.coins[coin]["column"][column]
        # substract saved value from API loaded value and round up to 5 decimal places
        change = "{:,.4g}".format(superfloat(currentMemory) - superfloat(self._coinsStartup[coin]["column"][column])).replace(",", " ")
        # add "+" sign for positive numbers
        if superfloat(change) > 0:
            change = "+ " + change
            lineEdit.setStyleSheet(styles.CHANGE_POSITIVE)
        elif superfloat(change) < 0:
            lineEdit.setStyleSheet(styles.CHANGE_NEGATIVE)

        # show the change between synced and API value
        lineEdit.setText(str(change))
        # WARNING - this triggers amountChanged function that will save the change to common.coins
        # so the value must be restored back
        common.coins[coin]["column"][column] = currentMemory

    def mouseOverNewBallanceLeave(self, lineEdit):
        """ mouse left hovering area of a line edith with column balance which changed since the last saving of the file """
        # get name of the object that is hovered
        name = lineEdit.objectName()
        # extract currency and column from the widget name
        pattern = re.compile(r"lineEdit_myPortfolio_(.+)_(.+)")
        coin, column = pattern.findall(name)[0]
        # set the text to the current value (not the change value that is there since EnterEvent)
        lineEdit.setText(reasonableStr(common.coins[coin]["column"][column]))
        # restore the stylesheet that was active before mouse hover (saved in EnterEvent)
        lineEdit.setStyleSheet(self.styleSheetMemory)

    def currentBaseChanged(self, base):
        """ base currency was changed in comboBox """
        # save the current choice
        common.bases["current"] = base
        # reload prices for this new base currency
        self.loadCrCmpCurrentData(base)

        # recalculate longer changes
        self._recalculateChanges(base)

        # recalculate global portfolio stats
        self.recalculatePortfolioTotals(base)

        self.recalculatePortfolioDailyLowHigh(base)

        self.ui.comboBox_roiBases.setCurrentText(base)

#########################################################################################################

    def amountChanged(self, text, column, coin):
        """ function triggered by cahnging text in lineEdit with amount of coin in a wallet """
        # update the amount in a proper element of myCurrencyCurrent dictionary
        common.coins[coin]["column"][column] = superfloat(text) if superfloat(text) != 0 else 0

        # if the cell is not synchronized
        if self._widgets[coin][column].styleSheet() == styles.BASIC:
            # recalculate the total field
            self.recalculateTotal(coin)

        if column in common.wallets:
            self.recalculateVetricalTotal(column)

    def recalculateTotal(self, coin):
        """ calculates total cell
            args:
                coin: str with currency abreviation. ("BTC")
        """
        # calculate total amount
        totalAmount = 0.0
        for wallet in common.wallets:
            # ignore Fiat wallet for value calculation
            if wallet != "Fiat":
                # add up all subamounts to total amount
                totalAmount += common.coins[coin]["column"][wallet]
        # find the total field and set its value
        self._widgets[coin]["Total"].setText(reasonableStr(totalAmount))

    def recalculateAllTotals(self):
        """ recalculates all total cells in my portfolio """
        for coin in common.coins:
            self.recalculateTotal(coin)

    def totalChanged(self, text, coin, lineEdit):
        """ function triggered by change in any total lineEdit """
        # update the total in a proper element of myCurrencyCurrent dictionary
        common.coins[coin]["column"]["Total"] = superfloat(text)

        # update VALUE cell
        self.recalculateValue(coin)

    def priceChanged(self, text, coin):
        """ function triggered by change in any price lineEdit """
        # update the price in a proper element of myCurrencyCurrent dictionary
        common.coins[coin]["column"]["Price"] = superfloat(text)

        # update VALUE cell
        self.recalculateValue(coin)

    def recalculateVetricalTotal(self, wallet):
        """ racalculates 1 total wallet value cell from total and price """
        total = 0
        # calculate the total value for the wallet
        for coin in common.coins:
            total += (common.coins[coin]["column"][wallet] * common.coins[coin]["column"]["Price"])
        # get the name of the currently selected base currency
        currentBase = common.bases["current"]
        # find the cell to change
        totalCell = self._widgets["Total"][wallet]
        # set the value string      decimal definition for example "{:.3f}"                    value          symbol for example "$"
        totalCell.setText(common.bases["options"][currentBase]["valueDecimals"].format(total).replace(",", " ")
                          + " " + common.bases["options"][currentBase]["symbol"])

    def recalculateAllVerticalTotals(self):
        """ recalculates all total wallet value cells in my portfolio """
        for wallet in common.wallets:
            self.recalculateVetricalTotal(wallet)

    def recalculateValue(self, coin):
        """ racalculates 1 value cell from total and price """
        # find the value cell that will be updated
        valueCell = self._widgets[coin]["Value"]
        # calculate value as price * total
        value = common.coins[coin]["column"]["Price"] * common.coins[coin]["column"]["Total"]
        # get the name of the currently selected base currency
        currentBase = common.bases["current"]
        # set the value string      decimal definition for example "{:.3f}"                    value          symbol for example "$"
        valueCell.setText(common.bases["options"][currentBase]["valueDecimals"].format(value).replace(",", " ")
                          + " " + common.bases["options"][currentBase]["symbol"])

    def valueChanged(self, text, coin, ignoreAllocations=False):
        """ function triggered by change in any value lineEdit """
        # update the value in a proper element of myCurrencyCurrent dictionary
        common.coins[coin]["column"]["Value"] = superfloat(text)

        # recalculate allocation every time value is changed
        self.recalculateAllocations()

    def recalculatePortfolioTotals(self, currentBase):
        """ refreshes portfolio totals section of myPortfolio tab (portfolio total, gains, invested...)
            args:
                currentBase (str) - currently selected base """
        self.recalculatePortfolioTotal(currentBase)
        self.recalculateDailyGain(currentBase)
        self.recalculateTotalInvested(currentBase)
        self.recalculateTotalGain(currentBase)

    def recalculatePortfolioTotal(self, currentBase):
        """ calculate portfolio value from values of all cryptos
            args:
                currentBase (str) - currently selected base """
        totalValue = 0
        totalFiatValue = 0
        # go through all cryptos
        for coin in common.coins:
            # count cryptos
            if common.coins[coin]["isCrypto"] is True:
                # add each crypto"s value to the total value
                totalValue += common.coins[coin]["column"]["Value"]
            # count fiats
            else:
                totalFiatValue += common.coins[coin]["column"]["Value"]

        # set the totalValue to the PORTFOLIO VALUE label with proper coin symbol
        self.ui.label_portfoliValueAbs.setText(common.bases["options"][currentBase]["valueDecimals"]    # number format
                                               .format(totalValue).replace(",", " ")                    # value with separators
                                               + " " + common.bases["options"][currentBase]["symbol"])  # symbol for example "$"
        # same for fiats
        self.ui.label_portfoliValueFiat.setText("+ " + common.bases["options"][currentBase]["valueDecimals"]    # number format
                                                .format(totalFiatValue).replace(",", " ")                # value with separators
                                                + " " + common.bases["options"][currentBase]["symbol"]  # symbol for example "$"
                                                + " (fiat)")
        # and for combination of cryptos and fiats
        self.ui.label_portfoliValueWithFiat.setText(common.bases["options"][currentBase]["valueDecimals"]    # number format
                                                    .format(totalValue + totalFiatValue).replace(",", " ")  # value with separators
                                                    + " " + common.bases["options"][currentBase]["symbol"])  # symbol for example "$"

        # save portfolio value to common dict
        common.portfolioTotals["portfolioValue"][currentBase] = totalValue
        common.portfolioTotals["portfolioValueFiat"][currentBase] = totalFiatValue
        common.portfolioTotals["portfolioValueWithFiat"][currentBase] = totalValue + totalFiatValue

    def recalculateDailyGain(self, currentBase):
        """ recalculates DailyGain from changes (1d)
            args:
                currentBase (str) - currently selected base """
        # get the name of the currently selected base currency
        symbol = common.bases["options"][currentBase]["symbol"]
        sumOfValues = 0
        sumOfOldValues = 0
        for coin in common.coins:
            if common.coins[coin]["isCrypto"] is True:
                try:    # current value might be 0 (especially in BTC prices)
                    # calculate sum of all crypto values
                    sumOfValues += superfloat(common.coins[coin]["column"]["Value"])
                    # calculate sum of all crypto daily changes in absolute nr. not percents
                    sumOfOldValues += (100 * superfloat(common.coins[coin]["column"]["Value"])
                                       / (100 + superfloat(common.coins[coin]["column"]["Ch(1d)"])))
                except ZeroDivisionError:
                    pass

        dailyChangeAbs = sumOfValues - sumOfOldValues
        try:
            dailyChangePerc = dailyChangeAbs / sumOfOldValues * 100
        except ZeroDivisionError:
            dailyChangePerc = 0

        # set style and number format based on the gain being positive or negative
        if dailyChangeAbs > 0:
            self.ui.label_dailyGainAbs.setText("+ " + reasonableStr(dailyChangeAbs) + " " + symbol)
            self.ui.label_dailyGainPerc.setText("+ {:,.2f} %".format(dailyChangePerc))
            self.ui.label_dailyGainAbs.setStyleSheet(styles.GAIN_POSITIVE)
            self.ui.label_dailyGainPerc.setStyleSheet(styles.GAIN_POSITIVE)
        else:
            self.ui.label_dailyGainAbs.setText(reasonableStr(dailyChangeAbs) + " " + symbol)
            self.ui.label_dailyGainPerc.setText("{:,.2f} %".format(dailyChangePerc))
            self.ui.label_dailyGainAbs.setStyleSheet(styles.GAIN_NEGATIVE)
            self.ui.label_dailyGainPerc.setStyleSheet(styles.GAIN_NEGATIVE)

        # save portfolio daily change to common dict
        common.portfolioTotals["portfolioChange"][currentBase] = dailyChangePerc

    def recalculateAllocations(self):
        """ recalculates all allocation cells from the current value """
        totalCryptoValue = 0
        # go through all active coins
        for coin in common.coins:
            # and sum them all up (only cryptos)!!!!
            if common.coins[coin]["isCrypto"] is True:
                totalCryptoValue += common.coins[coin]["column"]["Value"]
        # go through all active coins
        for coin in common.coins:
            if common.coins[coin]["isCrypto"] is True:
                value = common.coins[coin]["column"]["Value"]
                # calculate allocation as value/total value * 100 in percents
                if totalCryptoValue > 0:
                    self._widgets[coin]["Allocation"].setText("{:.2f}".format(value / totalCryptoValue * 100)
                                                              .replace(",", " ") + " %")

    def recalculateTotalInvested(self, currentBase):
        """ fills labels total invested and fiat withdrawn with updated values
            args:
                currentBase (str) - currently selected base """
        totalInvestedValue = {}
        fiatWithdrawnValue = {}

        # load totalInvested, Fiat withdrawn from DB
        for coinName, coin in common.coins.items():
            if coin["isCrypto"] is False:
                # load all fiat withdrawals for this fiat coin
                fiatWithdrawals = self.dbHistory.readFromDb(
                    table="transactions", columns=("amount",),
                    where={"operator": "AND", "data": ("coin='{}'".format(coinName), "operation='WITHDRAWAL'", "wallet='Fiat'"), },
                )
                # save sum of all withdraws to common dict
                sumOfWithdrawals = 0
                for withdrawal in fiatWithdrawals:
                    sumOfWithdrawals += withdrawal["amount"]
                common.portfolioTotals["totalInvestedAmount"][coinName] = -sumOfWithdrawals

                # load all fiat deposits for this fiat coin
                fiatDeposits = self.dbHistory.readFromDb(
                    table="transactions", columns=("amount",),
                    where={"operator": "AND", "data": ("coin='{}'".format(coinName), "operation='DEPOSIT'", "wallet='Fiat'"), },
                )
                # save sum of all deposits to common dict
                sumOfDeposits = 0
                for deposit in fiatDeposits:
                    sumOfDeposits += deposit["amount"]
                common.portfolioTotals["fiatWithdrawnAmount"][coinName] = sumOfDeposits

        # convert amounts of fiat withdrawals and deposits to values in all bases
        for base in common.bases["options"]:
            totalInvestedValue = 0
            fiatWithdrawnValue = 0

            for coin, amount in common.portfolioTotals["totalInvestedAmount"].items():
                # get value in current base
                value = amount * common.coins[coin]["column"]["Price"] / common.bases["options"][currentBase]["rate"][base]
                totalInvestedValue += value

            for coin, amount in common.portfolioTotals["fiatWithdrawnAmount"].items():
                # get value in current base
                value = amount * common.coins[coin]["column"]["Price"] / common.bases["options"][currentBase]["rate"][base]
                fiatWithdrawnValue += value

            common.portfolioTotals["totalInvestedValue"][base] = totalInvestedValue
            common.portfolioTotals["fiatWithdrawnValue"][base] = fiatWithdrawnValue

        # show total invested and fiat withdrawn in the current base
        self.ui.label_totalInvested.setText("- " + reasonableStr(common.portfolioTotals["totalInvestedValue"][currentBase])
                                            + " " + common.bases["options"][currentBase]["symbol"])
        self.ui.label_fiatWithdrawn.setText("+ " + reasonableStr(common.portfolioTotals["fiatWithdrawnValue"][currentBase])
                                            + " " + common.bases["options"][currentBase]["symbol"])

    def recalculateTotalGain(self, currentBase):
        """ recalculates label_totalGainAbs and label_totalGainPerc
            args:
                currentBase (str) - currently selected base """
        # get the name of the currently selected base currency
        symbol = common.bases["options"][currentBase]["symbol"]
        # recalculate total gain absolute and in percents
        if superfloat(common.portfolioTotals["totalInvestedValue"][currentBase]) != 0:      # zeroDivisionError
            totalValue = common.portfolioTotals["portfolioValue"][currentBase]
            totalValueFiat = common.portfolioTotals["portfolioValueFiat"][currentBase]
            totalInvested = common.portfolioTotals["totalInvestedValue"][currentBase]
            totalWithdrawn = common.portfolioTotals["fiatWithdrawnValue"][currentBase]

            # calculations absolute gain
            totalGain = totalValue + totalValueFiat - totalInvested + totalWithdrawn
            # similar to history tab ROI calculations
            totalGainPerc = ((totalValue + totalValueFiat + totalWithdrawn) / totalInvested - 1) * 100

            # set style and number format based on the gain being positive or negative
            if totalGain > 0:
                self.ui.label_totalGainAbs.setText("+ " + reasonableStr(totalGain) + " " + symbol)
                self.ui.label_totalGainPerc.setText("+ {:,.2f} %".format(totalGainPerc))
                self.ui.label_totalGainAbs.setStyleSheet(styles.GAIN_POSITIVE)
                self.ui.label_totalGainPerc.setStyleSheet(styles.GAIN_POSITIVE)
            else:
                self.ui.label_totalGainAbs.setText(reasonableStr(totalGain) + " " + symbol)
                self.ui.label_totalGainPerc.setText("{:,.2f} %".format(totalGainPerc))
                self.ui.label_totalGainAbs.setStyleSheet(styles.GAIN_NEGATIVE)
                self.ui.label_totalGainPerc.setStyleSheet(styles.GAIN_NEGATIVE)

    def recalculatePortfolioDailyLowHigh(self, currentBase):
        """ update daily low and daily high of the portfolio value
            cant be updated with updating portfolioValue - it is updating after every coin, portfolio low would be incorrect
            args:
                currentBase (str) - currently selected base """
        totalValue = common.portfolioTotals["portfolioValue"][currentBase]

        if totalValue < common.portfolioTotals["portfolioValueDailyLow"][currentBase]:
            common.portfolioTotals["portfolioValueDailyLow"][currentBase] = totalValue

        if totalValue > common.portfolioTotals["portfolioValueDailyHigh"][currentBase]:
            common.portfolioTotals["portfolioValueDailyHigh"][currentBase] = totalValue

        self.ui.label_portfoliValueDailyLow.setText(
            common.bases["options"][currentBase]["valueDecimals"].format(
                common.portfolioTotals["portfolioValueDailyLow"][currentBase]).replace(",", " ")
            + " " + common.bases["options"][currentBase]["symbol"])
        self.ui.label_portfoliValueDailyHigh.setText(
            common.bases["options"][currentBase]["valueDecimals"].format(
                common.portfolioTotals["portfolioValueDailyHigh"][currentBase]).replace(",", " ")
            + " " + common.bases["options"][currentBase]["symbol"])

    def resetPortfolioDailyHighLow(self):
        """ resets all daily high low portfolio values """
        for base in common.bases["options"]:
            common.portfolioTotals["portfolioValueDailyLow"][base] = 9999999999999
            common.portfolioTotals["portfolioValueDailyHigh"][base] = 0

#########################################################################################################
#             _           _   _
#            | |__  _   _| |_| |_ ___  _ __  ___
#            | '_ \| | | | __| __/ _ \| '_ \/ __|
#            | |_) | |_| | |_| || (_) | | | \__ \
#            |_.__/ \__,_|\__|\__\___/|_| |_|___/
#                                                       ivrit
#########################################################################################################

    def walletClicked(self, walletName):
        """
        if the clicked walletName is not synchronized, tries to synchronize balances with remote servers accesed with API
        If it was synchronized, returns walues from start of the application (saved in textfile)
        Parameters:
            walletName - (string) name of any used wallet
                example: "Coinmate"
        """
        # coins that are not in common.coins and are in this wallet's balance
        missingCoins = []
        # wallet is not synchronized from API
        if self._columnSynchronized[walletName] is False:
            # load Balances from the wallet according to parameter walletName
            try:
                balances = self.walletClients[walletName].loadBalances()
            except (KeyError, json.JSONDecodeError, LoadingError) as e:
                errorMessage("{}".format(type(e).__name__),
                             "*** Portfolio balances loading from {} failed\n: {}".format(walletName, e))
            except requests.exceptions.ConnectionError:
                print("NO INTERNET CONNECTION (loading balances from {})".format(walletName))
            else:
                print("Portfolio balances loading from *** {} ***\tOK and done".format(walletName))
                # change color of wallet name
                currentCell = self._widgets["Header"][walletName]
                # set style to wallet synced (pushbutton must have min-width = 0 to adapt to its contents)
                currentCell.setStyleSheet(styles.HEADER_SYNCED)

                # set all non 0 balances to 0 and red color (in case they are not loaded with balances
                # it means that they were non 0 and now are 0 so they must appear red)
                if walletName not in ["Ledger", "Other", "Trezor", "Fiat"]:  # don"t erase the ledger and other values
                    for coin in common.coins:
                        currentCell = self._widgets[coin][walletName]
                        # all cells in synchronized wallet cant be edited
                        currentCell.setReadOnly(True)
                        # if the amount is not 0 in GUI
                        if superfloat(currentCell.text()) != 0:
                            # set current cell to 0, red color and install mouse hover event (reasons above)
                            currentCell.setStyleSheet(styles.BALANCE_LOWER)
                            currentCell.installEventFilter(self)
                        currentCell.setText("0")

                # process all loaded balances from API
                try:
                    for coin in balances:
                        # eliminate supersmalll balances because of exponential string formating (1.58e-8)
                        if balances[coin] < 0.00001:
                            balances[coin] = 0.0
                        # for case when there is a coin loaded from exchange but not in the list (i.e. has very low balance)
                        try:
                            # find and clear the next coin"s cell
                            currentCell = self._widgets[coin][walletName]
                            # round up the result to 5 decimal points
                            currentCell.setText(reasonableStr(balances[coin]))
                            currentCell.setFont(styles.FONT_9_SLIM)
                            # API value is bigger than saved value - green color and install event filter for mouse hovering
                            if superfloat(currentCell.text()) > superfloat(self._coinsStartup[coin]["column"][walletName]):
                                currentCell.setStyleSheet(styles.BALANCE_HIGHER)
                                currentCell.installEventFilter(self)
                            # API value is lower than saved value - red color and install event filter for mouse hovering
                            elif superfloat(currentCell.text()) < superfloat(self._coinsStartup[coin]["column"][walletName]):
                                currentCell.setStyleSheet(styles.BALANCE_LOWER)
                                currentCell.installEventFilter(self)
                            else:  # values are equal - base color and UNINSTALL event filter
                                currentCell.setStyleSheet(styles.BALANCE_EQUAL)
                                currentCell.removeEventFilter(self)
                            # synchronized values cannot be edited
                            currentCell.setReadOnly(True)

                            # save all the new values in dictionary
                            common.coins[coin]["column"][walletName] = superfloat(currentCell.text())
                        except (KeyError, TypeError):
                            missingCoins.append(coin)   # synced coin that is not self._widgets[] - its ok, skip this coin
                except TypeError:   # data not loaded correctly - dont do anything
                    pass
                else:   # wallet succesfully synchronized
                    self._columnSynchronized[walletName] = True

                # print missing coins if there are any
                if missingCoins:
                    print("Wallet {} contains these coins that you did not add to your portfolio: ".format(walletName)
                          + ", ".join(missingCoins))

                # all wallets synchronized = total header gets synchronized look
                for idx, walletName in enumerate(common.wallets):
                    if self._columnSynchronized[walletName] is True:
                        # is this the last iteration?
                        if idx == len(common.wallets) - 1:
                            self._widgets["Header"]["Total"].setStyleSheet(styles.HEADER_SYNCED)
                    else:
                        break   # any non synchronized wallet breaks the check

        # wallet was already synchronized, return walues from saved file
        else:
            # return style of all objects to "unsynchronized" look
            self._widgets["Header"][walletName].setStyleSheet(styles.HEADER)

            for coin in self._coinsStartup:
                currentCell = self._widgets[coin][walletName]
                # skip cell, that were not synchronized (process only edited cells)
                if currentCell.styleSheet() not in [styles.BASIC, styles.FIAT]:
                    currentCell.setText(str(self._coinsStartup[coin]["column"][walletName]))
                    currentCell.setStyleSheet(styles.BASIC)
                    currentCell.setFont(styles.FONT_8_SLIM)
                    currentCell.setReadOnly(False)
                    currentCell.removeEventFilter(self)
            # set flag back to "unsynchronized"
            self._columnSynchronized[walletName] = False

        # recalculate all total fields
        self.recalculateAllTotals()

    def refreshWalletsClicked(self):
        """push button refresh wallets was clicked - try to sync all known wallets"""
        # set all headers styles to default
        for wallet in common.wallets:
            self._widgets["Header"][wallet].setStyleSheet(styles.HEADER)
        QApplication.processEvents()

        # initialize new progressInformer
        self.progressInfo_balances = ProgressInformer(styles.PB_BALANCES, self.ui.frame_progressInformers)

        # go through all known wallets
        for walletIdx, walletName in enumerate(common.wallets):
            # set flag as not synchronized
            self._columnSynchronized[walletName] = False

            self.progressInfo_balances.setText("Loading wallet balances... {}".format(walletName))

            # load balances
            self.walletClicked(walletName)

            # update progress informer
            self.progressInfo_balances.setValue((walletIdx + 1) / len(common.wallets) * 100)
            # show changes in GUI after every wallet
            QApplication.processEvents()

        # update progress informer
        self.progressInfo_balances.setText("Loading wallet balances... DONE")
        self.progressInfo_balances.setValue(100)

        # update label last refreshed with the current datetime
        dateAndTime = datetime.utcnow().strftime("%d.%m.%Y  %H:%M:%S")
        self.ui.label_LastRefreshedWallets.setText("last refreshed:   " + dateAndTime)

    def refreshValuesClicked(self):
        """ push button refresh values was clicked
        loads prices from API which triggers changing of values in base currency
        load change values (24h, 7d, 1m)
        calculates alocation
        all of these operations are done for all base coins and stored in dictionaries as superfloat values without suffix"""
        # initialize new progressInformer
        self.progressInfo_values = ProgressInformer(styles.PB_VALUES, self.ui.frame_progressInformers)
        self.progressInfo_values.setText("Loading portfolio values... ")
        QApplication.processEvents()
        # load all current data
        try:
            self.cryptoCompare.loadCurrentPrices(common.bases, common.coins)
            # refresh rates between base coins (BTC-USD)
            self.loadBaseCurrencyRates(common.bases)

            # get the name of the currently selected base currency
            currentBase = common.bases["current"]

            # save current data (portfolio totals, prices) for all bases to common dict
            self._saveCurrentlyRefreshedData()

            # set the last chosen base as active again
            self.ui.comboBox_baseCurrencies.setCurrentText(currentBase)
            # load prices for this base to GUI cells
            self.loadCrCmpCurrentData(currentBase)
            # now the base currency combobox might be enabled (other currency prices are loaded)
            self.ui.comboBox_baseCurrencies.setEnabled(True)

            # update label last refreshed with the current datetime
            dateAndTime = datetime.utcnow().strftime("%d.%m.%Y  %H:%M:%S")
            self.ui.label_LastRefreshedValues.setText("last refreshed:   " + dateAndTime)

            # recalculate longer changes
            self._recalculateChanges(currentBase)

            # update all vertical totals
            self.recalculateAllVerticalTotals()
            # recalculate total value
            self.recalculatePortfolioTotals(currentBase)

        except requests.exceptions.ConnectionError:
            errorMessage("ConnectionError", "My Portfolio values could not be refreshed".format())

        self.progressInfo_values.setText("Loading portfolio values... DONE")
        self.progressInfo_values.setValue(100)

    def _saveCurrentlyRefreshedData(self):
        """ saves current portfolio totals (value, gain, low, high) and coin prices for all bases
            to common dict to be used by other modules (notifications)"""
        # go through all bases
        for base in common.bases["options"]:
            # change the current base so all the calculations are done
            self.ui.comboBox_baseCurrencies.setCurrentText(base)

    def refreshAllClicked(self):
        """ refresh all button was clicked or autorefresh triggered"""
        self.refreshWalletsClicked()
        self.refreshValuesClicked()

        self._handleNotifications()

    def showWalletsChanged(self, state, columns):
        """ event function triggered by changing checkbox show xxx
        args:
            state: state of the checkbox - True/False """
        if state is True:
            for coin in list(common.coins.keys()) + ["Header"] + ["Total"]:
                for column in columns:
                    try:
                        if coin != "Header":
                            if self._widgets[coin]["Header"].isVisible():
                                self._widgets[coin][column].show()
                        else:
                            self._widgets[coin][column].show()
                    except KeyError:
                        pass    # the row might not contain widget for this column (i.e. total row not inlucing price column)
        else:
            for coin in list(common.coins.keys()) + ["Header"] + ["Total"]:
                for column in columns:
                    try:
                        self._widgets[coin][column].hide()
                    except KeyError:
                        pass    # the row might not contain widget for this column (i.e. total row not inlucing price column)

    def showLowBalancesChanged(self, state):
        """ event function triggered by changing checkbox show low balances
        args:
            state: state of the checkbox - True/False """
        # get the name of the currently selected base currency
        for coinName, coin in common.coins.items():
            # if crypto coin is under balance threshold
            if state is False and coin["isCrypto"] is True and coin["column"]["Allocation"] < 0.1:
                for widgetName, widget in self._widgets[coinName].items():
                    widget.hide()
            else:
                for widgetName, widget in self._widgets[coinName].items():
                    widget.show()
        # call all other checkbox checked functions to hide/show proper columns
        self.showWalletsChanged(self.ui.checkBox_detailView.isChecked(), ["Ch(3m)", "Ch(1y)", "Low", "High"])

    def headerClickedSorting(self, header):
        """ header with sorting support was clicked """
        # aquire values to sort as a dictionary with "coinName": value
        valuesToSort = {}
        for coin in common.coins:
            if common.coins[coin]["isCrypto"] is True:
                valuesToSort[coin] = superfloat(self._widgets[coin][header].text())
        # if the dictionary was not sorted as descending previously
        if self._sortedDescending is True:
            # sort it from lowest to highest (ascending)
            sortedDict = dict(sorted(valuesToSort.items(), key=operator.itemgetter(1)))
            self._sortedDescending = False
        else:
            # sort it from highest to lowest (descending)
            sortedDict = dict(sorted(valuesToSort.items(), key=operator.itemgetter(1), reverse=True))
            self._sortedDescending = True

        # get parent GUI objects
        parentR = self.ui.scrollAreaWidget_myPortfolio
        parentL = self.ui.scrollAreaWidget_myPortfolioCurrencies

        # prepade temporal dicts to sort global dictionaries _myCurrencies and _myCurrenciesCurrent
        myCurrenciesSorted = {}
        myCurrenciesCurrentSorted = {}

        # sort the widget in GUI according to the new order
        for idxCoin, coin in enumerate(sortedDict, 1):
            # sort currency headers in the left scroll area
            parentL.layout().addWidget(self._widgets[coin]["Header"], idxCoin, 0, 1, 2)
            for idxCol, column in enumerate(list(common.wallets.keys()) + MyPortfolio.DATA_HEADERS):
                # sort all lineEdits (and icons) in the right scroll area
                parentR.layout().addWidget(self._widgets[coin][column], idxCoin, idxCol, 1, 1)
            # sort global dictionaries _myCurrencies and _myCurrenciesCurrent
            myCurrenciesSorted[coin] = copy.deepcopy(self._coinsStartup[coin])
            myCurrenciesCurrentSorted[coin] = copy.deepcopy(common.coins[coin])

        # add all noncrypto coins at the end of the sorted dict
        for coin in common.coins:
            if common.coins[coin]["isCrypto"] is False:
                myCurrenciesSorted[coin] = copy.deepcopy(self._coinsStartup[coin])
                myCurrenciesCurrentSorted[coin] = copy.deepcopy(common.coins[coin])

        # update global dictionaries with the current order
        self._coinsStartup = myCurrenciesSorted
        common.coins = myCurrenciesCurrentSorted

    def _addCryptoClicked(self):
        """ plus button clicked - add new coin """
        newCoin, ok = QInputDialog.getText(None, "New coin", "Insert name of a new coin:")
        newCoin = newCoin.upper()

        if ok:
            common.nrOfNonFiats += 1
            # add new coin to global dict
            common.coins[newCoin] = {
                "isCrypto": True,
                "column": {

                }
            }
            # set all wallet balances and other values to 0
            for wallet in common.wallets:
                common.coins[newCoin]["column"][wallet] = 0
            for header in ["Total", "Low", "Price", "High", "Value", "Allocation",
                           "Ch(24h)", "Ch(1d)", "Ch(7d)", "Ch(1m)", "Ch(3m)", "Ch(1y)"]:
                common.coins[newCoin]["column"][header] = 0

            # refresh startup state
            self._coinsStartup = copy.deepcopy(common.coins)
            # add widgets for this coin
            self._createRowHeaders({newCoin: common.coins[newCoin]}, len(common.coins))
            self._createDataWidgets({newCoin: common.coins[newCoin]}, len(common.coins))
            # recalculate its total value
            self.recalculateTotal(newCoin)
            # sync prices for this new coin
            self._syncHistoricalPrices()

    def _removeCryptoClicked(self):
        """ minus button clicked, remove one coin """
        coin, ok = QInputDialog.getText(None, "Remove coin", "Insert name of the deleted coin:")
        coin = coin.upper()

        # only cryptos can be eliminated, not fiats
        if ok and coin in common.coins and common.coins[coin]["isCrypto"] is True:
            common.nrOfNonFiats -= 1
            # remove coin .coins
            common.coins.pop(coin)
            self._coinsStartup = copy.deepcopy(common.coins)

            for widget in self._widgets[coin].values():
                widget.parent().layout().removeWidget(widget)
                widget.deleteLater()
            self._widgets[coin] = None

    def _openMyPortfolioJson(self):
        """ push button clicked - open the json file in default editor """
        try:
            os.startfile(common.PATH + "/user/safefiles/myPortfolio.json")
        except Exception as e:
            print("/user/safefiles/myPortfolio.json loading failed:\n{}".format(str(e)))

    def _openUserFolder(self):
        """ push button clicked - open the json file in default editor """
        try:
            webbrowser.open(common.PATH + "/user")
        except Exception as e:
            print("/user folder loading failed:\n{}".format(str(e)))

####################################################################################################################################
#          _         _        ____       __               _        ___                 _   _  __ _           _   _
#         / \  _   _| |_ ___ |  _ \ ___ / _|_ __ ___  ___| |__    ( _ )    _ __   ___ | |_(_)/ _(_) ___ __ _| |_(_) ___  _ __  ___
#        / _ \| | | | __/ _ \| |_) / _ \ |_| '__/ _ \/ __| '_ \   / _ \/\ | '_ \ / _ \| __| | |_| |/ __/ _` | __| |/ _ \| '_ \/ __|
#       / ___ \ |_| | || (_) |  _ <  __/  _| | |  __/\__ \ | | | | (_>  < | | | | (_) | |_| |  _| | (_| (_| | |_| | (_) | | | \__ \
#      /_/   \_\__,_|\__\___/|_| \_\___|_| |_|  \___||___/_| |_|  \___/\/ |_| |_|\___/ \__|_|_| |_|\___\__,_|\__|_|\___/|_| |_|___/
#
####################################################################################################################################

    def _handleNotifications(self):
        """ Check updated portfolio values and trigger notifications if conditions met """
        # PORTFOLIO VALUE NOTIFICATION
        if "portfolioValue" in common.notifications:
            for notification in common.notifications["portfolioValue"]:
                # notification must be fully defined, ignore filled notifications
                if (notification["base"] in common.portfolioTotals["portfolioValue"]
                        and not ("filled" in notification and notification["filled"] is True)):
                    # PORTFOLIO VALUE > NOTIFICATION VALUE
                    if "More than" in notification["type"]:
                        # portfolio value is higher than the value of the notification
                        if common.portfolioTotals["portfolioValue"][notification["base"]] > notification["value"]:
                            sendMailNotification(self.yagMail,
                                                 "Portfolio value > {} {}".format(notification["value"], notification["base"]),
                                                 """CryptoKarMa reporting that value of your portfolio just reached
                                                 over {} {}""".format(notification["value"], notification["base"]))
                            # notification is filled, should not be triggered again
                            notification["filled"] = True
                            notification["filledTimestamp"] = int(datetime.now().timestamp())
                    # PORTFOLIO VALUE < NOTIFICATION VALUE
                    elif "Less than" in notification["type"]:
                        # portfolio value is higher than the value of the notification
                        if common.portfolioTotals["portfolioValue"][notification["base"]] < notification["value"]:
                            sendMailNotification(self.yagMail,
                                                 "Portfolio value < {} {}".format(notification["value"], notification["base"]),
                                                 """CryptoKarMa reporting that value of your portfolio is lower than
                                                 {} {}""".format(notification["value"], notification["base"]))
                            # notification is filled, should not be triggered again
                            notification["filled"] = True
                            notification["filledTimestamp"] = int(datetime.now().timestamp())

            notifications.saveNotifications()

        # PORTFOLIO CHANGE NOTIFICATION
        if "portfolioChange" in common.notifications:
            for notification in common.notifications["portfolioChange"]:
                # notification must be fully defined, ignore filled notifications
                if (notification["base"] in common.portfolioTotals["portfolioChange"]
                        and not ("filled" in notification and notification["filled"] is True)):
                    # abs(PORTFOLIO CHANGE) > NOTIFICATION VALUE
                    if "More than" in notification["type"]:
                        # portfolio change is higher than the value of the notification positive or negative
                        if abs(common.portfolioTotals["portfolioChange"][notification["base"]]) > notification["value"]:
                            sendMailNotification(self.yagMail,
                                                 "Portfolio change (in {}) > {} %".format(notification["base"], notification["value"]),
                                                 """CryptoKarMa reporting that change of value of your portfolio is more than
                                                 {} % in {}""".format(notification["value"], notification["base"]))
                            # notification is filled, should not be triggered again
                            notification["filled"] = True
                            notification["filledTimestamp"] = int(datetime.now().timestamp())

                # erase all filled flags if they were achieved yesterday
                if "filled" in notification and "filledTimestamp" in notification and notification["filled"] is True:
                    if datetime.fromtimestamp(notification["filledTimestamp"]).day != datetime.now().day:
                        notification["filled"] = False
                        notification["filledTimestamp"] = None

            notifications.saveNotifications()

        # BTC PRICE NOTIFICATION
        if "btcPrice" in common.notifications:
            for notification in common.notifications["btcPrice"]:
                # ignore filled notifications
                if (not ("filled" in notification and notification["filled"] is True)):
                    # BTC PRICE > NOTIFICATION VALUE
                    if "More than" in notification["type"]:
                        # BTC PRICE is higher than the value of the notification
                        if common.bases["options"][notification["base"]]["rate"]["BTC"] > notification["value"]:
                            sendMailNotification(self.yagMail,
                                                 "BTC price > {} in {}".format(notification["value"], notification["base"]),
                                                 """CryptoKarMa reporting that BTC price just reached
                                                 over {} {}""".format(notification["value"], notification["base"]))
                            # notification is filled, should not be triggered again
                            notification["filled"] = True
                            notification["filledTimestamp"] = int(datetime.now().timestamp())
                    # BTC PRICE < NOTIFICATION VALUE
                    elif "Less than" in notification["type"]:
                        # BTC PRICE is higher than the value of the notification
                        if common.bases["options"][notification["base"]]["rate"]["BTC"] < notification["value"]:
                            sendMailNotification(self.yagMail,
                                                 "BTC price < {} {}".format(notification["value"], notification["base"]),
                                                 """CryptoKarMa reporting that BTC price is lower than
                                                 {} {}""".format(notification["value"], notification["base"]))
                            # notification is filled, should not be triggered again
                            notification["filled"] = True
                            notification["filledTimestamp"] = int(datetime.now().timestamp())


class LoadWalletBalancesThread(QThread):
    """ """
    errorCatched = pyqtSignal(Exception)
    progressChanged = pyqtSignal(dict, str)
    loadingDone = pyqtSignal(dict)

    def __init__(self, walletClient, walletName, parent=None):
        QThread.__init__(self, parent)
        self.walletClient = walletClient
        self.walletName = walletName

    def run(self):
        # load Balances from the wallet according to parameter walletName
        try:
            balances = self.walletClient.loadBalances()
        except Exception as e:
            self.errorCatched.emit(e, self.walletName)
        else:
            self.loadingDone.emit(balances, self.walletName)
