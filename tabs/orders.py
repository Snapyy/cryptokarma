import requests
import os
from datetime import datetime

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (QSpacerItem, QSizePolicy, QPushButton, QApplication)
from PyQt5.QtGui import QBrush

from com import common
from com.common import (reasonableStr, superfloat,
                        QTableWidgetItemEnhanced, errorMessage)

import com.styles as styles
from com.progressInformer import ProgressInformer


class Orders(QObject):

    def __init__(self, name, humanName, ui, wallets, dbHistory, yagMail):
        super().__init__()
        self.name = name
        self.humanName = humanName
        self.ui = ui
        self.walletClients = wallets
        self.dbHistory = dbHistory
        self.yagMail = yagMail

        self._initOrders()

    def _initOrders(self):
        """ """
        self._widgets = {}

        self._initOrdersGui()

        # refresh orders tab tables
        self._refreshOpenOrdersTable()

    def _initOrdersGui(self):

        # inser coins and exchange wallets to open orders comboBoxes
        for coin in common.coins:
            self.ui.comboBox_ordersCoinFilter.addItem(coin)
        for base in common.bases["options"]:
            self.ui.comboBox_ordersBaseFilter.addItem(base)
        for wallet, isExchange in common.wallets.items():
            if isExchange:
                self.ui.comboBox_ordersWalletFilter.addItem(wallet)

        # assign events to slots
        self.ui.comboBox_ordersCoinFilter.currentTextChanged.connect(self._refreshOpenOrdersTable)
        self.ui.comboBox_ordersBaseFilter.currentTextChanged.connect(self._refreshOpenOrdersTable)
        self.ui.comboBox_ordersWalletFilter.currentTextChanged.connect(self._refreshOpenOrdersTable)
        self.ui.pushButton_ordersRefresh.clicked.connect(self.refreshAllClicked)
        self.ui.pushButton_openHistorDb2.clicked.connect(self._openHistorDb)

        # show buttons of wallets in tab header
        self._initWalletButtons()

        # set width to tableWidget columns
        OPENORDERS_HEADERS = {
            "Created": 140,
            "Wallet": 90,
            "CoinPair": 80,
            "Type": 50,
            "AmountSell": 180,
            "AmountBuy": 180,
            "Price": 80,
            "CurrentPrice": 90,
            "Remaining": 80,
            "ClosestPrice": 90,
            "ClosestRemaining": 110,
        }
        for headerIdx, width in enumerate(OPENORDERS_HEADERS.values()):
            self.ui.tableWidget_openOrders.setColumnWidth(headerIdx, width)

    def _initWalletButtons(self):
        """ initializes wallets buttons by inserting pushButtons with all wallet names """
        self._widgets["wallets"] = {}

        # add all wallet pushButtons to their scrollArea
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        for walletIdx, (walletName, isExchange) in enumerate(common.wallets.items()):
            if isExchange:
                pushButton_wallet = QPushButton(parent=self.ui.frame_ordersHeader,
                                                text=walletName, font=styles.FONT_HEADER,
                                                sizePolicy=sizePolicy,
                                                styleSheet=styles.PLUS_BUTTON)

                self.ui.frame_ordersHeader.layout().addWidget(pushButton_wallet, 0, walletIdx + 2, 1, 1)
                self._widgets["wallets"][walletName] = pushButton_wallet

                # connect clicked event to slot function
                pushButton_wallet.clicked.connect(lambda _, w=walletName: self._loadOpenOrdersOneWallet(w, self.walletClients[w]))

        # add spacer at the end of both scrollAreas
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.ui.frame_ordersHeader.layout().addItem(spacerItem, 0, walletIdx + 3, 1, 1)

######################################################################################################
#                _______     __ ____ _   _ _____ ____
#               | ____\ \   / / ____| \ | |_   _/ ___|
#               |  _|  \ \ / /|  _| |  \| | | | \___ \
#               | |___  \ V / | |___| |\  | | |  ___) |
#               |_____|  \_/  |_____|_| \_| |_| |____/
######################################################################################################

    def refreshAllClicked(self):
        """ loads data about open orders from all exchanges API
            loads data about prices regarding pairs with open orders
            calculates additional data about orders (remaining %)
            saves data to database """

        # initialize new progressInformer
        self.progressInfo_orders = ProgressInformer(styles.PB_ORDERS, self.ui.frame_progressInformers)

        nrOfExchanges = len([True for isExchange in common.wallets.values() if isExchange])
        progress = 0
        # go through all wallets and synchronize their open orders
        for walletIdx, (walletName, isExchange) in enumerate(common.wallets.items()):
            if isExchange:
                # cant use wallet index, there might be some non exchange wallets
                walletClient = self.walletClients[walletName]
                # update progress informer
                self.progressInfo_orders.setText(
                    "Loading open orders from exchange API and saving to CryptoKarMa.db... {}".format(walletName)
                )

                # synchronize transactions from 1 wallet's API
                self._loadOpenOrdersOneWallet(walletName, walletClient)

                # update progress informer
                progress += 1
                self.progressInfo_orders.setValue(progress / nrOfExchanges * 100)
                QApplication.processEvents()

        # update progress informer
        self.progressInfo_orders.setText("Loading open orders from wallets API and saving to CryptoKarMa.db ...DONE")
        self.progressInfo_orders.setValue(100)

    def _openHistorDb(self):
        """ push button clicked - open the db file in default editor """
        try:
            os.startfile(common.PATH + "/user/safefiles/CryptoKarMa.db")
        except Exception as e:
            print("/user/safefiles/CryptoKarMa.db loading failed:\n{}".format(str(e)))

    def _loadOpenOrdersOneWallet(self, walletName, walletClient):
        """ synchronize open orders from 1 wallet API
            args:
                walletName (str) - for example "Coinmate"
                walletClient (wallet client object): for example CoinmateClient object """
        openOrders = {}
        # set stylesheet - syncing in progress
        self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCING)
        QApplication.processEvents()

        try:
            openOrders = walletClient.getOpenOrders()
        except Exception as e:
            errorMessage("{}".format(type(e).__name__), "Loading open orders from {} failed: {}".format(walletName, e))
            # set stylesheet - Error during API openOrder syncing
            self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCED_ERROR)
        except requests.exceptions.ConnectionError:
            print("NO INTERNET CONNECTION (loading open orders from {})".format(walletName))
        else:
            print("Open orders loading from *** {} ***\tOK and done".format(walletName))
            # set stylesheet - Transaction syncing OK
            self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCED_OK)
            # enhance orders = calculate all information that are not directly synced
            openOrdersFullInfo = self._enhanceOpenOrders(openOrders, walletName)
            # delete all orders of this wallet from db
            self.dbHistory.deleteSomething("openOrders", where={"operator": "AND", "data": ("wallet='{}'".format(walletName),)})
            # add all these orders to database
            self.dbHistory.addOpenOrders(openOrdersFullInfo)

        # refresh orders tab tables
        self._refreshOpenOrdersTable()
        QApplication.processEvents()

    def _enhanceOpenOrders(self, orders, walletName):
        """ adds some calculated data to inserted orders,
            args:
                orders (list of dicts): [{timestamp(int ms13dig), coinPair(str), amountBuy(float), amountSell(float),
                                          price(float), type(str), currentPrice(float), high(float), low(float)}]
                walletName (str) - name of the wallet on which the orders are
            returns:
                enhancedOrders (list of tuples):
                    (timestamp(int), date(datetime sting), wallet(str), coinPair(str), type(str),
                     amountBuy(str), amountSell(str), price(float), currentPrice(float),
                     remaining(float), closestPrice(float), closestRemaining(float))
        """
        enhancedOrders = []

        for order in orders:
            timestamp = int(order["timestamp"])
            date = datetime.utcfromtimestamp(timestamp / 1000).strftime('%Y-%m-%d %H:%M:%S')

            # find this order in database to edit its closest price
            thisOrder = self.dbHistory.readFromDb(
                table="openOrders",
                columns=("closestPrice",),
                where={
                    "operator": "AND",
                    "data": (
                        "timestamp='{}'".format(timestamp),
                        "wallet='{}'".format(walletName),
                        "coinPair='{}'".format(order["coinPair"]),
                        "price='{}'".format(order["price"]),
                    ),
                },
            )
            closestPricePrevious = thisOrder[0]["closestPrice"] if len(thisOrder) > 0 else None

            # round amounts to maximum of 10 decimal digits (division problem 99999999999)
            order["amountBuy"] = round(order["amountBuy"], 10)
            order["amountSell"] = round(order["amountSell"], 10)

            coin = order["coinPair"].split("_")[0]
            base = order["coinPair"].split("_")[1]

            if order["type"] == "BUY":
                # determine the closest price based on current high, lows and previous closest price
                if closestPricePrevious:
                    if order["low"] < closestPricePrevious:
                        closestPrice = order["low"]
                    else:
                        closestPrice = closestPricePrevious
                else:
                    closestPrice = order["low"]
                # calculate how much does the current and closest price have to change to close the order in %
                remaining = (1 - (order["price"] / order["currentPrice"])) * 100
                remainingClosest = (1 - (order["price"] / closestPrice)) * 100
                # add coinName to both amounts
                amountBuy = str(order["amountBuy"]) + " " + coin
                amountSell = str(order["amountSell"]) + " " + base
            else:
                if closestPricePrevious:
                    if order["high"] > closestPricePrevious:
                        closestPrice = order["high"]
                    else:
                        closestPrice = closestPricePrevious
                else:
                    closestPrice = order["high"]
                remaining = (order["price"] / order["currentPrice"] - 1) * 100
                remainingClosest = (order["price"] / closestPrice - 1) * 100

                amountBuy = str(order["amountBuy"]) + " " + base
                amountSell = str(order["amountSell"]) + " " + coin

            # add it all to the list
            enhancedOrders.append((
                timestamp, date, walletName, order["coinPair"], coin, base, order["type"], amountBuy,
                amountSell, order["price"], order["currentPrice"], remaining, closestPrice, remainingClosest
            ))

        return enhancedOrders

    def _refreshOpenOrdersTable(self):
        """ loads all open orders from database and shows them in table widget """
        # set filters from comboBoxes as sqlite where clauses
        coinFilter = self.ui.comboBox_ordersCoinFilter.currentText()
        baseFilter = self.ui.comboBox_ordersBaseFilter.currentText()
        walletFilter = self.ui.comboBox_ordersWalletFilter.currentText()
        coinFilter = "1" if coinFilter == "All" else "coin='{}'".format(coinFilter)
        baseFilter = "1" if baseFilter == "All" else "base='{}'".format(baseFilter)
        walletFilter = "1" if walletFilter == "All" else "wallet='{}'".format(walletFilter)

        # get openOrders from database with filtering
        openOrders = self.dbHistory.readFromDb(
            table="openOrders",
            columns=("date", "wallet", "coinPair", "type", "amountBuy", "amountSell", "price",
                     "currentPrice", "remaining", "closestPrice", "closestRemaining"),
            orderBy="remaining ASC",
            where={
                "operator": "AND",
                "data": (
                    coinFilter,
                    baseFilter,
                    walletFilter,
                ),
            },
        )

        tableWidget = self.ui.tableWidget_openOrders
        # sorting must be disabled when changin rowCount otherwise table will be incorrectly populated
        tableWidget.setSortingEnabled(False)
        tableWidget.setRowCount(0)
        for orderIdx, order in enumerate(openOrders):
            tableWidget.setRowCount(tableWidget.rowCount() + 1)
            for columnIdx, columnText in enumerate(order):
                item = QTableWidgetItemEnhanced()
                # stylesheet for the whole row
                if order["type"] == "BUY":
                    item.setBackground(QBrush(styles.BRUSH_ORDER_BUY))
                else:
                    item.setBackground(QBrush(styles.BRUSH_ORDER_SELL))
                # round up some columns
                if columnIdx in [6, 7, 9]:
                    columnText = reasonableStr(superfloat(columnText))
                # format amounts
                if columnIdx == 4:  # amount buy
                    item.setBackground(QBrush(styles.BRUSH_ORDER_AMOUNT_BUY))
                    columnText = "+" + columnText
                if columnIdx == 5:  # amount sell
                    item.setBackground(QBrush(styles.BRUSH_ORDER_AMOUNT_SELL))
                # format remainings in %
                if columnIdx in [8, 10]:  # remaining %
                    columnText = reasonableStr(superfloat(columnText)) + " %"
                    if order["type"] == "BUY":
                        columnText = "-" + columnText

                item.setText(str(columnText))
                tableWidget.setItem(orderIdx, columnIdx, item)

        tableWidget.setSortingEnabled(True)
