import json
import os
import copy

from PyQt5.QtWidgets import QSizePolicy, QPushButton, QSpacerItem, QSpinBox, QComboBox
from PyQt5.QtCore import QSize, QObject, Qt

from com import common
from com.common import errorMessage
import com.styles as styles


class Notifications(QObject):

    tables = {}
    widgets = {}

    def __init__(self, name, humanName, ui):
        super().__init__()
        self.name = name
        self.humanName = humanName
        self.ui = ui

        # dict with pairs tableName: table's parent Widget
        Notifications.tables = {
            "portfolioValue": self.ui.scrollAreaWidgetContents_portfolioValueNotifications,
            "portfolioChange": self.ui.scrollAreaWidgetContents_portfolioChangeNotifications,
            "btcPrice": self.ui.scrollAreaWidgetContents_btcPriceNotifications,
        }

        self._initNotifications()

    def _initNotifications(self):
        """ Initialize the tab """

        self.ui.pushButton_saveNotifications.clicked.connect(saveNotifications)
        self.ui.pushButton_notificationsJson.clicked.connect(self._openNotificationsJson)

        # same initialization for all tables
        for tableName, parent in Notifications.tables.items():
            # container for all widget references
            Notifications.widgets[tableName] = []
            # new member in common dictionary
            common.notifications[tableName] = []
            # init GUI
            self._initNotificationsTableGui(tableName, parent)

        # load notifications saved in a file
        self._loadNotifications()

    def _initNotificationsTableGui(self, tableName, parent):
        """ Initialize 1 notifications table
            args:
                tableName (str) - valid name of a notification table
                parent (QWidget) - parent container widget of the table (i.e. scrollArea_widget) """
        # add plus button at the bottom of the table
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        pushButton_addRow = QPushButton(parent=parent, text="+", font=styles.FONT_HEADER,
                                        minimumSize=(QSize(0, styles.CELL_HEIGHT)),
                                        sizePolicy=sizePolicy, styleSheet=styles.PLUS_BUTTON)
        parent.layout().addWidget(pushButton_addRow, 5000, 0, 1, 4)
        pushButton_addRow.clicked.connect(lambda _, tn=tableName, p=parent: self._addNotificationRow(tn, p))

        # add vertical spacer below the add button
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Expanding)
        parent.layout().addItem(spacerItem, 5001, 0, 1, 1)

    def _addNotificationRow(self, tableName, parent):
        """ Adds one row to represent one condition that, when filled triggers notification
            args:
                tableName (str) - valid name of a notification table
                parent (QWidget) - parent container widget of the table (i.e. scrollArea_widget) """
        # get row index
        rowIdx = len(Notifications.widgets[tableName])

        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        # create comboBox with notification type
        comboBox_type = QComboBox(parent=parent, styleSheet=styles.NOTIF_PRIMARY,
                                  sizePolicy=sizePolicy, font=styles.FONT_HEADER,
                                  minimumSize=QSize(0, 20), maximumSize=QSize(150, 16777215),
                                  sizeAdjustPolicy=QComboBox.AdjustToContents)
        parent.layout().addWidget(comboBox_type, 2 + rowIdx, 0, 1, 1)
        comboBox_type.addItem("")
        comboBox_type.addItem("More than")
        if tableName != "portfolioChange":
            comboBox_type.addItem("Less than")

        # create spinBox with value of the notification
        spinBox_value = QSpinBox(parent=parent, styleSheet=styles.NOTIF_SECONDARY,
                                 sizePolicy=sizePolicy, font=styles.FONT_HEADER,
                                 minimumSize=QSize(150, 20), maximumSize=QSize(200, 16777215),
                                 minimum=0, maximum=100_000_000_000, value=0,
                                 alignment=Qt.AlignRight)
        parent.layout().addWidget(spinBox_value, 2 + rowIdx, 1, 1, 1)
        # suffix is different for various table
        if tableName == "portfolioChange":
            spinBox_value.setSuffix(" %")

        # create comboBox with bases
        comboBox_base = QComboBox(parent=parent, styleSheet=styles.NOTIF_PRIMARY,
                                  sizePolicy=sizePolicy, font=styles.FONT_HEADER,
                                  minimumSize=QSize(0, 20), maximumSize=QSize(90, 16777215),
                                  sizeAdjustPolicy=QComboBox.AdjustToContents)
        parent.layout().addWidget(comboBox_base, 2 + rowIdx, 2, 1, 1)
        comboBox_base.addItem("")
        for base in common.bases["options"]:
            comboBox_base.addItem(base)

        # create push button to remove notifications
        pushButton_remove = QPushButton(parent=parent, text=" - ", font=styles.FONT_HEADER,
                                        sizePolicy=sizePolicy,
                                        minimumSize=QSize(40, 20), maximumSize=QSize(40, 16777215),
                                        styleSheet=styles.NOTIF_REMOVE_BUTTON)
        parent.layout().addWidget(pushButton_remove, 2 + rowIdx, 3, 1, 1)

        # save all new widgets to widgets dictionary
        Notifications.widgets[tableName].append({
            "comboBox_type": comboBox_type,
            "spinBox_value": spinBox_value,
            "comboBox_base": comboBox_base,
            "pushButton_remove": pushButton_remove,
        })

        # add event handling for new widgets
        pushButton_remove.clicked.connect(lambda _, tn=tableName, p=parent, rIdx=rowIdx: self._removeNotification(tn, p, rIdx))

######################################################################################################
#                _______     __ ____ _   _ _____ ____
#               | ____\ \   / / ____| \ | |_   _/ ___|
#               |  _|  \ \ / /|  _| |  \| | | | \___ \
#               | |___  \ V / | |___| |\  | | |  ___) |
#               |_____|  \_/  |_____|_| \_| |_| |____/
######################################################################################################

    def tabGotFocus(self):
        """ tab Notifications just got focus"""
        # gray out all filled notifications
        self._grayFilledNotifications()

    def _removeNotification(self, tableName, parent, rowIdx):
        """ A remove notification button was pushed, remove the row from notifications
            args:
                tableName (str) - valid name of a notification table
                parent (QWidget) - parent container widget of the table (i.e. scrollArea_widget)
                rowIdx (int) - index of the notification within the parent table """
        # delete all widgets from that row
        for widget in Notifications.widgets[tableName][rowIdx].values():
            parent.layout().removeWidget(widget)
            widget.deleteLater()
        # erase the reference from the widgets dictionary
        Notifications.widgets[tableName].pop(rowIdx)
        # pop it also from common dict
        try:    # it is saved to common after clicking save button so the index might not be there
            common.notifications[tableName].pop(rowIdx)
        except IndexError:
            pass

        # reconnect row events with updated row indexes in the whole table
        for rowIdx2 in range(rowIdx, len(Notifications.widgets[tableName])):
            pushButton_remove = Notifications.widgets[tableName][rowIdx2]["pushButton_remove"]
            try:
                pushButton_remove.clicked.disconnect()
            except TypeError:
                pass
            pushButton_remove.clicked.connect(lambda _, tn=tableName, p=parent, rIdx=rowIdx2: self._removeNotification(tn, p, rIdx))

            # reposition all widgets below this deleted row to have correct indexes
            for columnIdx, widget in {
                0: Notifications.widgets[tableName][rowIdx2]["comboBox_type"],
                1: Notifications.widgets[tableName][rowIdx2]["spinBox_value"],
                2: Notifications.widgets[tableName][rowIdx2]["comboBox_base"],
                3: Notifications.widgets[tableName][rowIdx2]["pushButton_remove"],
            }.items():
                parent.layout().addWidget(widget, 2 + rowIdx2, columnIdx, 1, 1)

    def _loadNotificationsFile(self):
        """ Loads saved notifications from json file """
        try:
            with open(common.PATH + "/user/safefiles/notifications.json", "r", encoding="utf-8") as loadedFile:
                # try to decode the file with json format
                return json.load(loadedFile)
        except FileNotFoundError:
            print("/user/safefiles/notifications.json loading failed", "File does not exist (is created after first save)")
            return None
        except json.JSONDecodeError as e:
            # not a valid JSON format
            errorMessage("notifications.json loading failed", "JSON file corrupted:\n{}".format(e))
            return None

    def _loadNotifications(self):
        """ loads notifications data from json file and inserts them to GUI """
        loadedNotifications = self._loadNotificationsFile()

        if loadedNotifications:
            for tableName, tableNotifications in loadedNotifications.items():
                # set loaded data to common dict
                common.notifications[tableName] = tableNotifications
                # get parent GUI container (scrollArea)
                parent = Notifications.tables[tableName]
                # Add each notification to GUI table
                for rowIdx, notification in enumerate(tableNotifications):
                    # add one row and insert data
                    self._addNotificationRow(tableName, parent)
                    Notifications.widgets[tableName][rowIdx]["comboBox_type"].setCurrentText(notification["type"])
                    Notifications.widgets[tableName][rowIdx]["spinBox_value"].setValue(notification["value"])
                    Notifications.widgets[tableName][rowIdx]["comboBox_base"].setCurrentText(notification["base"])

            self._grayFilledNotifications()

        # if any table does not have any loaded notifications at this point,
        # add 1 row so the format initializes properly
        for tableName, parent in Notifications.tables.items():
            if len(Notifications.widgets[tableName]) == 0:
                self._addNotificationRow(tableName, parent)

    def _openNotificationsJson(self):
        """ push button clicked - open the json file in default editor """
        try:
            os.startfile(common.PATH + "/user/safefiles/notifications.json")
        except Exception as e:
            print("/user/safefiles/notifications.json loading failed:\n{}".format(str(e)))

    def _grayFilledNotifications(self):
        """ sets gray color to all filled notifications based on filled flag in common.notifications """
        for tableName, notifications_ in common.notifications.items():
            for rowIdx, notification in enumerate(notifications_):
                if notification["filled"]:
                    for widget in Notifications.widgets[tableName][rowIdx].values():
                        # dont change button's stylesheet - it would change its width
                        if "min-width" not in widget.styleSheet():
                            widget.setStyleSheet(styles.NOTIF_GRAY)


def saveNotifications():
    """ Saves configured notifications to a json file and to common.notifications global dict """
    # save contents of all tables
    tempNotifications = copy.deepcopy(common.notifications)
    for tableName in Notifications.tables:
        # always create new list
        common.notifications[tableName] = []
        for rowIdx, rowWidgets in enumerate(Notifications.widgets[tableName]):
            type_ = rowWidgets["comboBox_type"].currentText()
            value = rowWidgets["spinBox_value"].value()
            base = rowWidgets["comboBox_base"].currentText()
            try:
                filled = tempNotifications[tableName][rowIdx]["filled"]
                filledDate = tempNotifications[tableName][rowIdx]["filledTimestamp"]
            except (KeyError, IndexError):
                filled = False
                filledDate = None
            common.notifications[tableName].append({
                "type": type_,
                "value": value,
                "base": base,
                "filled": filled,
                "filledTimestamp": filledDate
            })

    # try to open the file for writting
    try:
        with open(common.PATH + "/user/safefiles/notifications.json", "w", encoding="utf-8") as savedFile:
            # dump dictionary to the chosen file
            json.dump(common.notifications, savedFile, indent=4)
    except PermissionError:
        # permission denied (i.e. read only file)
        errorMessage("Saving notifications.json failed",
                     "Permission denied \nfile notifications.json \n\nhint: \nremove read-only flag", )
