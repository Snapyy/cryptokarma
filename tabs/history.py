import json
import csv
import dateutil
import os
import copy
import requests

import com.common as common
from com.common import (saveLastSynced, loadLastSynced, errorMessage, infoMessage,
                        roundTimestampToDay, LoadingError, reasonableStr, superfloat,
                        QTableWidgetItemEnhanced, sendMailNotification)
from com.graph import GraphData

from datetime import datetime
from dateutil.relativedelta import relativedelta
from PyQt5.QtWidgets import (QFileDialog, QCheckBox,
                             QSpacerItem, QSizePolicy, QPushButton, QApplication)
from PyQt5.QtGui import QBrush

from com.progressInformer import ProgressInformer
import com.styles as styles

import pyqtgraph
# import pyqtgraph.examples
# pyqtgraph.examples.run()


class History():

    def __init__(self, name, humanName, mainWindow, ui, wallets, cryptoCompare, dbHistory, yagMail):
        super().__init__()
        # https://stackoverflow.com/questions/42247583/pyqt-why-does-new-window-close-immediately-after-opening-it
        self.name = name
        self.humanName = humanName
        self.mainWindow = mainWindow  # needed to keep new windows alive until mainWindow is closed
        self.ui = ui
        self.walletClients = wallets
        self.cryptoCompare = cryptoCompare
        self.dbHistory = dbHistory
        self.yagMail = yagMail

        # dictionary with all synced transaction from all wallets since start of time
        # format:
        # {timestamp1: [
        #     {wallet: xyz, coin: BTC, amount: -0,1},
        #     {wallet: xyz, coin: BTC, amount: -0,1},]
        # }
        self.totalTransactionHistory = {}

        # dict with timestamps of the last synchronization of various APIs
        # load lastSynced dict from a file
        self._lastSyncedTransactions = loadLastSynced("transactions")

        # set of symbols of used coins
        self._usedCoins = set()

        # history of daily values of all used coins
        self._valueHistory = {}

        self._widgets = {}

        # list to store all graph windows
        self.mainWindow.graphWindows = []

        self.initUi()

    def initUi(self):
        """ initialize GUI """
        TRANSACTION_HEADERS = {
            "Time": 115,
            "Coin": 50,
            "Change": 80,
            "Wallet": 70,
            "Operation": 85,
        }
        BALANCE_HEADERS = {
            "Coin": 55,
            "Balance": 75,
            "Transactions": 75,
        }
        ROI_HEADERS = {
            "Coin": 50,
            "Type": 75,
            "ROI": 75,
            "ROI (%)": 75,
        }

        for headerIdx, width in enumerate(TRANSACTION_HEADERS.values()):
            self.ui.tableWidget_transactions.setColumnWidth(headerIdx, width)

        for headerIdx, width in enumerate(BALANCE_HEADERS.values()):
            self.ui.tableWidget_balanceCheck.setColumnWidth(headerIdx, width)

        for headerIdx, width in enumerate(ROI_HEADERS.values()):
            self.ui.tableWidget_roi.setColumnWidth(headerIdx, width)

        # save currentBase, add bases to comboBox (the first will trigger baseChanged function)
        tempCurrentBase = common.bases["current"]
        for base in common.bases["options"]:
            self.ui.comboBox_roiBases.addItem(base)
        # set the "current base" - this is the initial value after history tab init that
        # will be shown in my portfolio due to connection of ROI base and MyPortfolio base comboBoxes
        self._baseChanged(tempCurrentBase)

        self._initGraphSettingsGui()
        self._initWalletsColumn()

        self.ui.pushButton_syncTransactions.clicked.connect(self._syncAllApiTransactions)
        self.ui.pushButton_plotGraph.clicked.connect(self._plotGraphClicked)
        self.ui.pushButton_deleteAllTransactions.clicked.connect(self._deleteAllTransactions)
        self.ui.pushButton_deleteSyncedTransactions.clicked.connect(self._deleteSyncedTransactions)
        self.ui.pushButton_deleteCsvTransactions.clicked.connect(self._deleteCsvTransactions)
        self.ui.pushButton_loadNewestCsvs.clicked.connect(self._loadNewestCsvs)
        self.ui.pushButton_refreshAllHistory.clicked.connect(self.refreshAllHistory)
        self.ui.pushButton_loadLedgerCsv.clicked.connect(lambda: self._loadCsvWallet("Ledger"))
        self.ui.pushButton_loadNeonCsv.clicked.connect(lambda: self._loadCsvWallet("Neon"))
        self.ui.pushButton_loadCryptoComCsv.clicked.connect(lambda: self._loadCsvWallet("CryptoCom"))
        self.ui.pushButton_loadManualCsv.clicked.connect(lambda: self._loadCsvWallet("Manual"))
        self.ui.pushButton_loadTrezorCsv.clicked.connect(self._loadTrezorCsv)
        self.ui.checkBox_coinsAllCrypto.clicked.connect(lambda checked: self._graphCoinsAllCryptoClicked(checked))
        self.ui.checkBox_walletsAll.clicked.connect(lambda checked: self._graphWalletsAllClicked(checked))
        self.ui.checkBox_basesAll.clicked.connect(lambda checked: self._graphBasesAllClicked(checked))
        self.ui.comboBox_roiBases.currentTextChanged.connect(lambda text: self._baseChanged(text))
        self.ui.radioButton_graphAll.clicked.connect(lambda checked, widget=self.ui.radioButton_graphAll:
                                                     self._graphTimeframeClicked(checked, widget))
        self.ui.radioButton_graphYear.clicked.connect(lambda checked, widget=self.ui.radioButton_graphYear:
                                                      self._graphTimeframeClicked(checked, widget))
        self.ui.radioButton_graphLast.clicked.connect(lambda checked, widget=self.ui.radioButton_graphLast:
                                                      self._graphTimeframeClicked(checked, widget))
        self.ui.pushButton_openHistorDb.clicked.connect(self._openHistorDb)

        if "Binance" in self.walletClients:
            self.ui.pushButton_refreshBinanceSymbols.clicked.connect(self.walletClients["Binance"].refreshBinanceSymbolsAssets)
        if "CB Pro" in self.walletClients:
            self.ui.pushButton_refreshCBproSymbols.clicked.connect(self.walletClients["CB Pro"].refreshCoinBaseProAccountsProducts)

    def _initGraphSettingsGui(self):
        """ initializes graphical interface of graph settings frame """
        self._widgets["graphs"] = {
            "coins": {},
            "wallets": {},
            "bases": {},
            "types": {},
            "comparisons": {},
        }

        self._refreshGraphCoins()

        self._refreshGraphWallets()

        # add all base checkBoxes to their scrollArea
        for baseName in common.bases["options"]:
            checkBox_base = QCheckBox(parent=self.ui.scrollAreaWidget_graphBases,
                                      text=baseName)

            self.ui.scrollAreaWidget_graphBases.layout().addWidget(checkBox_base)
            self._widgets["graphs"]["bases"][baseName] = checkBox_base
            checkBox_base.clicked.connect(lambda checked, base=baseName: self._checkBoxBaseClicked(checked, base))

        # add spacer at the end of both scrollAreas
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.ui.scrollAreaWidget_graphCoins.layout().addItem(spacerItem, 1000, 0)
        self.ui.scrollAreaWidget_graphWallets.layout().addItem(spacerItem)
        self.ui.scrollAreaWidget_graphBases.layout().addItem(spacerItem)

        # default settings - all wallets and bases checked
        self.ui.checkBox_walletsAll.setChecked(True)
        self.ui.checkBox_coinsAllCrypto.setChecked(True)
        self._graphWalletsAllClicked(True)
        self._graphCoinsAllCryptoClicked(True)
        self._widgets["graphs"]["bases"]["CZK"].setChecked(True)

        # add graphTypes to widgets dictionary
        for checkBox in [self.ui.checkBox_graphTypeAmount, self.ui.checkBox_graphTypePrice, self.ui.checkBox_graphTypeValue]:
            self._widgets["graphs"]["types"][checkBox.text()] = checkBox

        # add comparisons to widgets dictionary
        for checkBox in [self.ui.checkBox_btcCzk, self.ui.checkBox_btcUsd]:
            self._widgets["graphs"]["comparisons"][checkBox.text()] = checkBox

        # fill comboBox year in frame Timeframe
        firstYear = datetime.fromtimestamp(common.OLDEST_TIMESTAMP / 1000).year
        lastYear = datetime.now().year
        for year in range(lastYear, firstYear - 1, -1):
            self.ui.comboBox_graphYear.addItem(str(year))

        self.ui.comboBox_graphYear.currentTextChanged.connect(self._graphSettingsYearChanged)
        self.ui.comboBox_GraphLast.currentTextChanged.connect(self._graphSettingsLastChanged)

    def _initWalletsColumn(self):
        """ initializes wallets column by inserting pushButtons with all wallet names """
        self._widgets["wallets"] = {}

        # add all wallet pushButtons to their scrollArea
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        for walletName in common.wallets:
            pushButton_wallet = QPushButton(parent=self.ui.scrollAreaWidget_transactionWallets,
                                            text=walletName, font=styles.FONT_HEADER,
                                            sizePolicy=sizePolicy,
                                            styleSheet=styles.PLUS_BUTTON)

            self.ui.scrollAreaWidget_transactionWallets.layout().addWidget(pushButton_wallet)
            self._widgets["wallets"][walletName] = pushButton_wallet

            # connect clicked event to slot function
            pushButton_wallet.clicked.connect(lambda _, w=walletName: self._syncApiTransactionsOneWallet(w, self.walletClients[w]))

        # add spacer at the end of both scrollAreas
        spacerItem = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.ui.scrollAreaWidget_transactionWallets.layout().addItem(spacerItem)

    def tabGotFocus(self):
        """ History is chosen as an active tab in tabWidget_main """
        # refresh history tab tables
        self._refreshGraphCoins()
        self._refreshGraphWallets()
        self._refreshHistoryTabTables()

    def _refreshHistoryTabTables(self):
        """ refresh values in all History tab tables """
        self._refreshLastTransactions()
        self._refreshBalances()
        self._refreshRoi()

    def _refreshGraphCoins(self):
        """ add all coin checkBoxes to their scrollArea in graphs
            can be called repeatedly and only new coins are added in next calls  """
        for coinIdx, coinName in enumerate(common.coins):
            # creates everything only once
            if coinName not in self._widgets["graphs"]["coins"]:
                checkBox_coin = QCheckBox(parent=self.ui.scrollAreaWidget_graphCoins,
                                          text=coinName)

                self._widgets["graphs"]["coins"][coinName] = checkBox_coin
                checkBox_coin.clicked.connect(lambda checked, coin=coinName: self._checkBoxCoinClicked(checked, coin))
            else:
                checkBox_coin = self._widgets["graphs"]["coins"][coinName]

            self.ui.scrollAreaWidget_graphCoins.layout().addWidget(checkBox_coin, coinIdx + 2, 0, 1, 1)

    def _refreshGraphWallets(self):
        """ get list of wallets by going throuh transactions database
            add all wallet checkBoxes to their scrollArea in graphs
            can be called repeatedly and only new wallets are added in next calls """
        # get list of wallets from CryptoKarMa.db/transactions
        transactionWallets = self.dbHistory.readFromDb(
            table="transactions",
            columns=("wallet",),
        )
        walletList = [transaction[0] for transaction in transactionWallets]
        # eliminate duplicates and sort alphabetically
        walletList = sorted(list(set(walletList)))

        # add all wallet checkBoxes to their scrollArea
        for walletIdx, walletName in enumerate(walletList):
            # creates everything only once
            if walletName not in self._widgets["graphs"]["wallets"]:
                checkBox_wallet = QCheckBox(parent=self.ui.scrollAreaWidget_graphWallets,
                                            text=walletName)

                self._widgets["graphs"]["wallets"][walletName] = checkBox_wallet
                checkBox_wallet.clicked.connect(lambda checked, wallet=walletName: self._checkBoxWalletClicked(checked, wallet))
            else:
                checkBox_wallet = self._widgets["graphs"]["wallets"][walletName]

            self.ui.scrollAreaWidget_graphWallets.layout().addWidget(checkBox_wallet, walletIdx + 2, 0, 1, 1)

    def refreshAllHistory(self):
        """ Loads all new transactions from all synchronizable wallets and saves them to CryptoKarMa.db
            Loads newest csv files from /user/csv folder and saves loaded transactions to CryptoKarMa.db
            And as a result of previous actions:
                Shows last 100 transactions in the table
                Performs balance checks and shows its results (for proper results refresh and save myPortfolio balances)
        """
        self._syncAllApiTransactions()
        self._loadNewestCsvs()

    def _openHistorDb(self):
        """ push button clicked - open the db file in default editor """
        try:
            os.startfile(common.PATH + "/user/safefiles/CryptoKarMa.db")
        except Exception as e:
            print("/user/safefiles/CryptoKarMa.db loading failed:\n{}".format(str(e)))

########################################################################################################################################
#   _____                               _   _
#  |_   _| __ __ _ _ __  ___  __ _  ___| |_(_) ___  _ __  ___
#    | || '__/ _` | '_ \/ __|/ _` |/ __| __| |/ _ \| '_ \/ __|
#    | || | | (_| | | | \__ \ (_| | (__| |_| | (_) | | | \__ \
#    |_||_|  \__,_|_| |_|___/\__,_|\___|\__|_|\___/|_| |_|___/
########################################################################################################################################

    def _refreshLastTransactions(self):
        """ Loads last 100 transactions from database and shows them in transactions tableWidget """

        last100Transactions = self.dbHistory.readFromDb(
            table="transactions",
            columns=("date", "coin", "amount", "wallet", "operation", "timestamp"),
            orderBy="timestamp DESC",
            rows=100
        )

        try:
            lastSyncedTimestamp = self._lastSyncedTransactions["previousSync"]
        except KeyError:
            lastSyncedTimestamp = 0

        tableWidget = self.ui.tableWidget_transactions
        # sorting must be disabled when changin rowCount otherwise table will be incorrectly populated
        tableWidget.setSortingEnabled(False)
        tableWidget.setRowCount(0)
        for transactionIdx, transaction in enumerate(last100Transactions):
            tableWidget.setRowCount(tableWidget.rowCount() + 1)
            for columnIdx, column in enumerate(transaction):
                if columnIdx == 2:
                    # round up amount to reasonable nr. of decimal places
                    column = reasonableStr(float(column))
                if columnIdx != 5:      # dont print the timestamp column
                    item = QTableWidgetItemEnhanced()
                    item.setText(str(column))
                    # set stylesheet base on new/old transactions
                    if transaction["timestamp"] > lastSyncedTimestamp:
                        item.setBackground(QBrush(styles.BRUSH_TRANSACTION_NEW))
                    else:
                        item.setBackground(QBrush(styles.BRUSH_TRANSACTION_OLD))
                    tableWidget.setItem(transactionIdx, columnIdx, item)

        tableWidget.setSortingEnabled(True)

    def _enhanceTransactions(self, transactions, type_, walletName):
        """ takes list of transactions in basic format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]} ,
        gets more information about the transactions and returns transactions in enhanced tuple format
            args:
                transactions (list of transactions) - format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
                type (str) - "synced" or "manual"
                walletName (str) - name of the wallet on which this transaction happened
            returns - list of enhanced transactions in tuple format:
            (timestamp(int), date(datetime sting), coin(str), amount(float), operation(text), type(text), walletName(str),
            coinOppo(str), amountOppo(float), usdValue(float), btcValue(float), czkValue(float), eurValue(float),
            btcPrice(float), usdPrice(float), czkPrice(float), eurPrice(float))
        """
        enhancedTransactions = []

        for timestamp, transactionsDaycoin in transactions.items():
            timestamp = int(timestamp)
            date = datetime.utcfromtimestamp(timestamp / 1000).strftime('%Y-%m-%d %H:%M:%S')

            # find identical transactions within one timestamp and merge them to 1 transaction
            # this must be done or otherwise UNIQUE settings of tables would only take one of these transactions
            if len(transactionsDaycoin) > 1:
                indexesToPop = []
                # go through all transactions from this timestamp and compare them to all others
                for idx1, transaction1 in enumerate(transactionsDaycoin):
                    # skip this transaction if it is already set to be popped
                    if idx1 not in indexesToPop:
                        amountToAdd = 0
                        amountOppoToAdd = 0
                        for idx2, transaction2 in enumerate(transactionsDaycoin[idx1 + 1:]):
                            if transaction1 == transaction2:
                                amountToAdd += transaction2["amount"]
                                if "amountOppo" in transaction2 and transaction2["amountOppo"] is not None:
                                    amountOppoToAdd += transaction2["amountOppo"]
                                indexesToPop.append(idx2 + idx1 + 1)
                        # add the amount from all transactions2 to transaction1
                        transaction1["amount"] += amountToAdd
                        if "amountOppo" in transaction1 and transaction1["amountOppo"] is not None:
                            transaction1["amountOppo"] += amountOppoToAdd
                # pop all transactions2 that were identical to a transaction1
                for idx in sorted(indexesToPop, reverse=True):
                    transactionsDaycoin.pop(idx)

            # enhance every transaction
            for transaction in transactionsDaycoin:
                coin = transaction["coin"]
                amount = transaction["amount"]
                # there are some transactions with 0 fee - empty 0 amount transaction that makes a mess
                if amount != 0:
                    # take wallet name if included with transaction otherwise use the walletName passed to this function
                    walletNameTransaction = transaction["wallet"] if "wallet" in transaction else walletName

                    # set loaded or default values for opposite coin and amount
                    coinOppo = transaction["coinOppo"] if "coinOppo" in transaction and transaction["coinOppo"] is not None else ""
                    amountOppo = transaction["amountOppo"] if "amountOppo" in transaction and transaction["amountOppo"] is not None else 0

                    # prepare dicts for base values and base prices
                    prices = {}
                    values = {}
                    # if coin is one of the bases, value in that base = amount, price = 1
                    if coin in common.bases["options"]:
                        # value of the transaction in that base = amount
                        values[coin] = amount
                        prices[coin] = 1
                    # if opposite coin is one of the bases, fill price and value of coin/opposite coin
                    if coinOppo in common.bases["options"]:
                        values[coinOppo] = -amountOppo
                        prices[coinOppo] = -amountOppo / amount

                    # determine mainCoin for this transaction from which the rest of price and values will be calculated
                    # priority order - [usd, btc, czk, eur], coin - if any of the bases is part of the transaction,
                    # it will be used for calculation because base prices are usually more precise than coin prices
                    mainCoin = coin
                    mainValue = amount
                    for base in ["USD", "BTC", "CZK", "EUR"]:
                        if base in values:
                            mainCoin = base
                            mainValue = values[base]
                            break
                    # prefer calculations with coins with long history because they tend to be more precise
                    # specifically transaction XTZ/ETH - XTZ price in db that day is 2.72 real price was 0.5
                    # calculating the values with ETH price will make the calculation more precise
                    for coinWithLongHistory in ["ETH", "LTC"]:
                        if coinWithLongHistory == coinOppo:
                            mainCoin = coinOppo
                            mainValue = amountOppo
                            break

                    # fill remaining values and prices for all bases (calculating from mainCoin value and price)
                    for base in common.bases["options"]:
                        # calculate value if not already determined from previous steps
                        if base not in values:
                            # calculate value: mainCoinValue * price(mainCoin, base)
                            values[base] = mainValue * self.dbHistory.loadSpecificPrice(timestamp, mainCoin, base)

                        if base not in prices:
                            # divide calculated value of this transaction in base with original amount to get price of coin/base
                            prices[base] = values[base] / amount

                    enhancedTransactions.append((
                        timestamp, date, coin, transaction["amount"], transaction["operation"], type_, walletNameTransaction,
                        coinOppo, amountOppo, values["BTC"], values["USD"], values["CZK"], values["EUR"],
                        prices["BTC"], prices["USD"], prices["CZK"], prices["EUR"]
                    ))

        # erase ignored transactions
        ignoredTransactions = self._loadIgnoredTransactions()
        for ignTransaction in ignoredTransactions:
            try:
                ignTimestamp = int(ignTransaction["Time"])
                amount = superfloat(ignTransaction["Amount"].replace(",", "."))
            except ValueError:
                print("IgnoredTransactions file contains rows with non integer Timestamps or non float amounts - fix it moron")
            if ignTimestamp in transactions:
                # go through all enhancedTransactions and erase ignored ones
                for trIdx, transaction in enumerate(enhancedTransactions):
                    if (transaction[0] == ignTimestamp
                            and transaction[2] == ignTransaction["Coin"]
                            and abs(transaction[3]) < abs(amount) * 1.00001
                            and abs(transaction[3]) > abs(amount) * 0.99999
                            and transaction[4] == ignTransaction["Operation"]):
                        # erase the transaction equal to the ignored one from transactions
                        enhancedTransactions.pop(trIdx)

        return enhancedTransactions

    def _loadIgnoredTransactions(self):
        """ """
        try:
            with open(common.PATH + "/user/csv/ignoredTransactions.csv", "r") as csvFile:
                reader = csv.DictReader(csvFile, delimiter=";")
                return [line for line in reader]
        except Exception as e:
            print(e)
            return {}

    def _refreshIncompleteTransactions(self):
        """ if any of the transactions in database is missing some data
        (usually price because it was not loaded at time of syncing)
        try to refresh it now """
        incompleteTransactions = self.dbHistory.readFromDb(
            table="transactions",
            columns=("timestamp", "coin", "amount", "operation", "wallet"),
            where={
                "operator": "AND",
                "data": ("btcPrice=-1",)
            },
        )

        newTransactions = {}

        for transaction in incompleteTransactions:
            # create list for this timestamp's transactions
            if transaction["timestamp"] not in newTransactions:
                newTransactions[transaction["timestamp"]] = []

            newTransactions[transaction["timestamp"]].append({
                "coin": transaction["coin"],
                "amount": transaction["amount"],
                "operation": transaction["operation"],
                "wallet": transaction["wallet"],
            })

        # enhance transacitons in a normal way
        # wallet = None indicates that wallet information is part of transaction dict
        newTransactionsFullInfo = self._enhanceTransactions(newTransactions, "synced", None)
        # update all these "new" transatcions to database
        self.dbHistory.addTransactions(newTransactionsFullInfo)

# ######### API synced transactions ####################################################################################################

    def _syncAllApiTransactions(self):
        """ load all new transactions from all available wallet APIs
            add new transactions to database
        """
        # mark all already loaded transactions as old
        try:
            self._lastSyncedTransactions["previousSync"] = roundTimestampToDay(max(list(self._lastSyncedTransactions.values())))
        except ValueError:  # max of empty list is valueError
            self._lastSyncedTransactions["previousSync"] = 0
        # if there are some transactions with incomplete data in db, try to aquire these data
        self._refreshIncompleteTransactions()

        # initialize new progressInformer
        self.progressInfo_transactions = ProgressInformer(styles.PB_TRANSACTIONS, self.ui.frame_progressInformers)

        newTransactions = {}
        # go through all wallets and synchronize their transactions
        for walletIdx, (walletName, walletClient) in enumerate(self.walletClients.items()):
            # update progress informer
            self.progressInfo_transactions.setText(
                "Loading transactions from wallets API and saving to CryptoKarMa.db... {}".format(walletName)
            )

            # synchronize transactions from 1 wallet's API
            oneWalletTransactions = self._syncApiTransactionsOneWallet(walletName, walletClient)
            if oneWalletTransactions:
                newTransactions[walletName] = oneWalletTransactions

            # update progress informer
            self.progressInfo_transactions.setValue((walletIdx + 1) / len(self.walletClients) * 100)
            QApplication.processEvents()

        # update progress informer
        self.progressInfo_transactions.setText("Loading transactions from wallets API and saving to CryptoKarMa.db ...DONE")
        self.progressInfo_transactions.setValue(100)

        self._newTransactionsNotification(newTransactions)

    def _syncApiTransactionsOneWallet(self, walletName, walletClient):
        """ synchronize transactions from 1 wallet API
            args:
                walletName (str) - for example "Coinmate"
                walletClient (wallet client object): for example CoinmateClient object """
        newTransactions = {}
        newTransactionsFullInfo = []
        # set stylesheet - syncing in progress
        self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCING)
        QApplication.processEvents()

        # get last synced time of this wallet (if not saved, set it as timestamp for 1.1.2013)
        if walletName in self._lastSyncedTransactions:
            timestampFrom = self._lastSyncedTransactions[walletName]
        else:
            timestampFrom = 1356998400000
        # get new transaction from wallet api in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        try:
            newTransactions, timestampSync = walletClient.getNewTransactions(timestampFrom)
        except (AttributeError, json.JSONDecodeError, LoadingError, requests.exceptions.ConnectionError) as e:
            if isinstance(e, AttributeError):
                print("New transactions loading from {} is not supported".format(walletName))
                # set stylesheet - API syncing not possible for this wallet
                self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCED_NOT_POSSIBLE)
            else:
                errorMessage("{}".format(type(e).__name__), "Loading new transactions from {} failed\n: {}".format(walletName, e))
                # set stylesheet - Error during API transactions syncing
                self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCED_ERROR)
        else:
            print("New transactions loading from *** {} ***\tOK and done".format(walletName))
            # set stylesheet - Transaction syncing OK
            self._widgets["wallets"][walletName].setStyleSheet(styles.TRANSACTIONS_SYNCED_OK)
            # enhance transactions = calculate all information that are not directly synced
            newTransactionsFullInfo = self._enhanceTransactions(newTransactions, "synced", walletName)
            # add all these new transatcions to database

            self.dbHistory.addTransactions(newTransactionsFullInfo)
            # update the synctime timestamp to a dictionary
            if timestampSync is not None:
                self._lastSyncedTransactions[walletName] = timestampSync

                # save updated timestamps from these syncing to a file
                saveLastSynced(self._lastSyncedTransactions, ("transactions",))

        # refresh history tab tables
        self._refreshHistoryTabTables()
        QApplication.processEvents()

        return newTransactionsFullInfo

    def _newTransactionsNotification(self, newTransactions):
        """ Sends a notification with all the newly synced transactions
            args:
                newTransactions (dict) format: {walletName: enhancedTransactions}"""

        if newTransactions:
            # create the string with all new transaction info
            transactionsStr = ""
            for walletName, transactions in newTransactions.items():
                for transaction in transactions:
                    transactionsStr += "{},\t{},\t{},\t{},\t{},\tValue in CZK: {}\n".format(
                        str(transaction[1]),
                        str(walletName),
                        str(transaction[2]),
                        str(transaction[4]),
                        str(transaction[3]),
                        str(transaction[10]),
                    )

            sendMailNotification(
                self.yagMail,
                "New transactions",
                "CryptoKarMa reporting that these new transactions were synced:\n" + transactionsStr
            )

            # save the new transaction strings for daily report sendout
            common.dailyTransactions.append(transactionsStr)

# ######### csv files #########################################################################################################

    def _loadNewestCsvs(self):
        """ Loads newest files for every wallet with csv file synchronization.
            The newest file is determined by timestamp or equivalent in every file name.
            If no file is found for any wallet, the wallet is skipped.
        """
        # get content of the csv folder in CryptoKarMa
        csvFolderContent = os.listdir(common.PATH + "/user/csv/")
        # take only csv files
        csvFiles = [csvFile for csvFile in csvFolderContent if csvFile.endswith("csv")]

        # LEDGER #########################################################################################################
        if "Ledger" in common.wallets:
            ledgerCsvs = [ledgerFile for ledgerFile in csvFiles if ledgerFile.startswith("ledgerlive-operations-")]
            if ledgerCsvs:
                # get the newest file from the file name
                ledgerFileDates = {int("".join(date.split("-")[2].split(".")[:3])): name for (date, name) in zip(ledgerCsvs, ledgerCsvs)}
                ledgerNewestFileName = ledgerFileDates[max(ledgerFileDates.keys())]
            else:
                # indicator, that there is no ledger file in the folder
                ledgerNewestFileName = False

            self._loadCsvWallet("Ledger", common.PATH + "/user/csv/" + ledgerNewestFileName, infoMessagesOn=False)

        # NEON #########################################################################################################
        if "Neon" in common.wallets:
            neonCsvs = [neonFile for neonFile in csvFiles if neonFile.startswith("neon-wallet-activity-")]
            if neonCsvs:
                # get the newest file from the file name
                neonFileDates = {int(date.split("-")[3].split(".")[0]): name for (date, name) in zip(neonCsvs, neonCsvs)}
                neonNewestFileName = neonFileDates[max(neonFileDates.keys())]
            else:
                # indicator, that there is no neon file in the folder
                neonNewestFileName = False

            self._loadCsvWallet("Neon", common.PATH + "/user/csv/" + neonNewestFileName, infoMessagesOn=False)

        # CRYPTO.COM #########################################################################################################
        if "Other" in common.wallets:
            cryptocomCsvs = [cryptocomFile for cryptocomFile in csvFiles if cryptocomFile.startswith("crypto_transactions_record_")]
            if cryptocomCsvs:
                # get the newest file from the file name
                cryptocomFileDates = {int(date.split("_")[3]): name for (date, name) in zip(cryptocomCsvs, cryptocomCsvs)}
                cryptocomNewestFileName = cryptocomFileDates[max(cryptocomFileDates.keys())]
                self._loadCsvWallet("CryptoCom", common.PATH + "/user/csv/" + cryptocomNewestFileName, infoMessagesOn=False)

        # TREZOR #########################################################################################################
        if "Trezor" in common.wallets:
            self._loadTrezorCsv(infoMessagesOn=False)

        # MANUAL #########################################################################################################
        manualCsvs = [manualFile for manualFile in csvFiles if manualFile.startswith("manualTransactions")]
        if manualCsvs:
            self._loadCsvWallet("Manual", common.PATH + "/user/csv/" + manualCsvs[0], infoMessagesOn=False)
        else:
            self._loadCsvWallet("Manual", False, infoMessagesOn=False)

        # refresh history tab tables
        self._refreshHistoryTabTables()

    def _loadCsvWallet(self, walletName, pathName=None, infoMessagesOn=True):
        """ Load transactions from Ledger cvs file
            store these transactions in CryptoKarMa.db if not present already
            args:
                walletName (str) - name of the wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
                infoMessagesOn (bool) - should OK infoMessage be shown or not
        """
        # load csv transactions as a list of ordered dicts
        transactionsCsv = self._loadCsvLines(walletName, pathName)

        if transactionsCsv:
            # call the propper processing function
            if walletName == "Ledger":
                transactions = self._loadLedgerCsv(transactionsCsv, walletName, pathName)
            if walletName == "Neon":
                transactions = self._loadNeonCsv(transactionsCsv, walletName, pathName)
            if walletName == "CryptoCom":
                transactions = self._loadCryptoComCsv(transactionsCsv, walletName, pathName)
            if walletName == "Manual":
                transactions = self._loadManualCsv(transactionsCsv, walletName, pathName)

            if transactions:
                # get all info about transaction to be saved into db
                transactionsFullInfo = self._enhanceTransactions(transactions, "csv", walletName)
                # add all these transatcions to database (duplicates not possible in this database)
                self.dbHistory.addTransactions(transactionsFullInfo)

                if infoMessagesOn:
                    infoMessage("csv read OK", "{} transactions loaded and saved to database".format(walletName))
            else:
                errorMessage("No transactions", "No transactions loaded")
        else:
            errorMessage("empty file", "This csv file is empty or not existing")

    def _loadLedgerCsv(self, transactionsCsv, walletName, pathName=None):
        """ Process transactions from cvs file
            args:
                transactionsCsv (list) - list containing lines from the csv file in orderedDict format
                walletName (str) - name of this wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
            returns:
                transactions (dict) - transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
        """
        # check, that it is a proper type of file
        try:
            transactionsCsv[0]["Operation Date"]
            transactionsCsv[0]["Operation Type"]
            transactionsCsv[0]["Operation Amount"]
            transactionsCsv[0]["Currency Ticker"]
        except KeyError as e:
            errorMessage("Invalid file", "This is not a valid {} csv file, not containing: {}".format(walletName, e))
            transactionsCsv = {}

        # transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        transactions = {}
        for transactionCsv in transactionsCsv:
            timestamp = int(datetime.timestamp(dateutil.parser.isoparse(transactionCsv["Operation Date"])) * 1000)

            # create list for this timestamp's transactions
            if timestamp not in transactions:
                transactions[timestamp] = []
            # + or - transaction amount
            if transactionCsv["Operation Type"] == "OUT":
                transactionCsv["Operation Amount"] = -float(transactionCsv["Operation Amount"])
                operation = "WITHDRAWAL"
            elif transactionCsv["Operation Type"] == "IN":
                if float(transactionCsv["Operation Amount"]) < 3 and transactionCsv["Currency Ticker"] == "XTZ":
                    operation = "STAKING"
                else:
                    operation = "DEPOSIT"

            transactions[timestamp].append({
                "coin": transactionCsv["Currency Ticker"],
                "amount": float(transactionCsv["Operation Amount"]),
                "operation": operation,
            })

        return transactions

    def _loadNeonCsv(self, transactionsCsv, walletName, pathName=None):
        """ Process transactions from cvs file
            args:
                transactionsCsv (list) - list containing lines from the csv file in orderedDict format
                walletName (str) - name of this wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
            returns:
                transactions (dict) - transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
        """
        # check, that it is a proper type of file
        try:
            transactionsCsv[0]["time"]
            transactionsCsv[0]["type"]
            transactionsCsv[0]["amount"]
            transactionsCsv[0]["symbol"]
        except KeyError as e:
            errorMessage("Invalid file", "This is not a valid {} csv file, not containing: {}".format(walletName, e))
            transactionsCsv = {}

        # transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        transactions = {}
        for transactionCsv in transactionsCsv:
            timestamp = int(datetime.strptime(transactionCsv["time"], "%m/%d/%Y | %H:%M:%S").timestamp() * 1000)

            # create list for this timestamp's transactions
            if timestamp not in transactions:
                transactions[timestamp] = []

            # + or - transaction amount
            transactionCsv["amount"] = float(transactionCsv["amount"])

            if transactionCsv["type"] == "CLAIM":
                operation = "REWARD"
            elif transactionCsv["type"] == "RECEIVE":
                operation = "DEPOSIT"
            elif transactionCsv["type"] == "SEND":  # not sure about this SEND
                operation = "WITHDRAWAL"

            if transactionCsv["amount"] != 0:
                transactions[timestamp].append({
                    "coin": transactionCsv["symbol"],
                    "amount": transactionCsv["amount"],
                    "operation": operation,
                })

        return transactions

    def _loadCryptoComCsv(self, transactionsCsv, walletName, pathName=None):
        """ Process transactions from cvs file
            args:
                transactionsCsv (list) - list containing lines from the csv file in orderedDict format
                walletName (str) - name of this wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
            returns:
                transactions (dict) - transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
        """
        # check, that it is a proper type of file
        try:
            transactionsCsv[0]["Timestamp (UTC)"]
            transactionsCsv[0]["Currency"]
            transactionsCsv[0]["Amount"]
            transactionsCsv[0]["Transaction Kind"]
            transactionsCsv[0]["Transaction Description"]
            transactionsCsv[0]["Native Currency"]
            transactionsCsv[0]["Native Amount"]
        except KeyError as e:
            errorMessage("Invalid file", "This is not a valid {} csv file, not containing: {}".format(walletName, e))
            transactionsCsv = {}

        # transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        transactions = {}
        for transactionCsv in transactionsCsv:
            timestamp = int(datetime.timestamp(dateutil.parser.isoparse(transactionCsv["Timestamp (UTC)"])) * 1000)

            # create list for this timestamp's transactions
            if timestamp not in transactions:
                transactions[timestamp] = []

            if ("Buy" in transactionCsv["Transaction Description"]
                    or "->" in transactionCsv["Transaction Description"]):
                operation = "BUY/SELL"
            elif "Stake" in transactionCsv["Transaction Description"]:
                operation = "STAKING"
            elif "Card Cashback" in transactionCsv["Transaction Description"]:
                operation = "DEPOSIT"
            elif "Card Rebate" in transactionCsv["Transaction Description"]:
                operation = "DEPOSIT"
            elif "Referral Bonus" in transactionCsv["Transaction Description"]:
                operation = "REWARD"
            else:
                print("ERROR: Crypto.com - Unknown transaction type {}".format(transactionCsv["Transaction Description"]))
                operation = ""

            # dont add transaction for "{Coin} Stake" description. It is not a transaction for me
            if not transactionCsv["Transaction Description"].endswith("Stake"):
                transactions[timestamp].append({
                    "coin": transactionCsv["Currency"],
                    "amount": float(transactionCsv["Amount"]),
                    "operation": operation,
                })
            if transactionCsv["To Currency"]:
                transactions[timestamp].append({
                    "coin": transactionCsv["To Currency"],
                    "amount": float(transactionCsv["To Amount"]),
                    "operation": operation,
                })

            # the payment for the original transaction
            if "Buy" in transactionCsv["Transaction Description"]:
                transactions[timestamp].append({
                    "coin": transactionCsv["Native Currency"],
                    "amount": -float(transactionCsv["Native Amount"]),
                    "operation": operation,
                })
                # plus add deposit transaction for the amount
                transactions[timestamp].append({
                    "coin": transactionCsv["Native Currency"],
                    "amount": float(transactionCsv["Native Amount"]),
                    "operation": "DEPOSIT",
                })

        return transactions

    def _loadTrezorCsv(self, infoMessagesOn=True):
        """ load data from all trezor files located in trezor folder"""
        trezorFolderContent = os.listdir(common.PATH + "/user/csv/trezor")

        # read all csv transactions
        transactionsCsv = {}
        for accountFile in trezorFolderContent:
            # extract coin from fileName
            coin = accountFile.split("_")[1].split(".")[0].upper()
            transactionsCsv[coin] = []
            pathName = common.PATH + "/user/csv/trezor/" + accountFile
            # load csv transactions as a list of ordered dicts
            with open(pathName, "r") as csvFile:
                reader = csv.DictReader(csvFile)
                transactionsCsv[coin] += [line for line in reader]

        # transactions will be in CryptoKarMa format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        newTransactions = {}

        for coin, transactionsCoin in transactionsCsv.items():
            if transactionsCoin:
                # check, that it is a proper type of file
                try:
                    transactionsCoin[0]["Time"]
                    transactionsCoin[0]["TX type"]
                    transactionsCoin[0]["Value"]
                    transactionsCoin[0]["TX total"]
                    transactionsCoin[0]["Balance"]
                except KeyError as e:
                    errorMessage("Invalid file", "This is not a valid Trezor csv file, not containing: {}".format(e))
                    transactionsCoin = {}

                # extract CryptoKarMa transactions from csv transactions
                for transaction in transactionsCoin:
                    dateAndTime = transaction["ď»żDate"] + " " + transaction["Time"]
                    timestamp = int(datetime.timestamp(dateutil.parser.isoparse(dateAndTime)) * 1000)

                    # create list for this timestamp's transactions
                    if timestamp not in newTransactions:
                        newTransactions[timestamp] = []
                    # + or - transaction amount
                    if transaction["TX type"] == "OUT":
                        operation = "WITHDRAWAL"
                    elif transaction["TX type"] == "IN":
                        operation = "DEPOSIT"

                    newTransactions[timestamp].append({
                        "coin": coin,
                        "amount": float(transaction["TX total"]),
                        "operation": operation,
                    })
            else:
                errorMessage("empty file", "One or more of the Ledger cvs files is empty or not existing")

        if newTransactions:
            # get all info about transaction to be saved into db
            transactionsFullInfo = self._enhanceTransactions(newTransactions, "csv", "Trezor")
            # add all these transatcions to database (duplicates not possible in this database)
            self.dbHistory.addTransactions(transactionsFullInfo)

            if infoMessagesOn:
                infoMessage("csv read OK", "Trezor transactions loaded and saved to database")
        else:
            errorMessage("No transactions", "No transactions loaded")

    def _loadManualCsv(self, transactionsCsv, walletName, pathName=None):
        """ Process transactions from cvs file
            args:
                transactionsCsv (list) - list containing lines from the csv file in orderedDict format
                walletName (str) - name of this wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
            returns:
                transactions (dict) - transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
                    (coinOppo and amountOppo are not obligatory, they are ignored if not present or set to None)
        """
        # check, that it is a proper type of file
        try:
            transactionsCsv[0]["Time"]
            transactionsCsv[0]["Coin"]
            transactionsCsv[0]["Amount"]
            transactionsCsv[0]["Operation"]
            transactionsCsv[0]["Wallet"]
        except KeyError as e:
            errorMessage("Invalid file", "This is not a valid {} csv file, not containing: {}".format(walletName, e))
            transactionsCsv = {}

        # transactions in format {timestamp: [{coin, amount, operation, **coinOppo**, **amountOppo**}]}
        transactions = {}
        for transactionCsv in transactionsCsv:
            timestamp = int(datetime.strptime(transactionCsv["Time"], "%d.%m.%Y %H:%M").timestamp() * 1000)

            # create list for this timestamp's transactions
            if timestamp not in transactions:
                transactions[timestamp] = []

            # if string contaions decimal dash instead of decimal point, replace it
            if isinstance(transactionCsv["Amount"], str) and "," in transactionCsv["Amount"]:
                transactionCsv["Amount"] = transactionCsv["Amount"].replace(",", ".")
            if isinstance(transactionCsv["AmountOppo"], str) and "," in transactionCsv["AmountOppo"]:
                transactionCsv["AmountOppo"] = transactionCsv["AmountOppo"].replace(",", ".")

            transactions[timestamp].append({
                "coin": transactionCsv["Coin"],
                "amount": float(transactionCsv["Amount"]),
                "operation": transactionCsv["Operation"],
                "coinOppo": (transactionCsv["CoinOppo"] if "CoinOppo" in transactionCsv and transactionCsv["CoinOppo"] != "" else None),
                "amountOppo": (float(transactionCsv["AmountOppo"])
                               if "AmountOppo" in transactionCsv and transactionCsv["AmountOppo"] != "" else None),
                # this wallet inside transaction will overrule the "Manual" wallet when going to database
                "wallet": transactionCsv["Wallet"],
            })

        return transactions

    def _loadCsvLines(self, walletName, pathName=None):
        """ loads csv file with transactions and reports error if anything goes to hell
            args:
                walletName (str) - name of the synchronized wallet
                pathName (str) - optional path to the csv file, if not passed try to get it with QFileDialog
            returns:
                (list) - list containing lines from the csv file in orderedDict format """
        if pathName is None:
            # get the file to be loaded from dialog window
            pathName = (QFileDialog.getOpenFileName(
                caption="Choose a csv file wth {} transactions".format(walletName), filter="csv file (*.csv)",
                directory=common.PATH + "/user/csv/"
            ))[0]  # (pathname from dialog is returned as a list)

        if pathName:      # when dialog is closed, there is no pathName
            try:
                if walletName == "Manual":
                    with open(pathName, "r") as csvFile:
                        reader = csv.DictReader(csvFile, delimiter=";")
                        return [line for line in reader]
                else:
                    with open(pathName, "r") as csvFile:
                        reader = csv.DictReader(csvFile)
                        return [line for line in reader]
            except OSError as e:   # typeError might occure when dialog is closed
                errorMessage("OS error", "Loading csv file with {} transactions resulted in:\n{}".format(walletName, e))
                return []

# ######### delete transactions #########################################################################################################

    def _deleteAllTransactions(self):
        """ deletes all transactions from CryptoKarMa.db """
        self._deleteSyncedTransactions()
        self._deleteCsvTransactions()

    def _deleteSyncedTransactions(self):
        """ deletes all type-synced transactions from CryptoKarMa.db
            deletes all synced timestamps from lastSynced.json regarding transactions"""
        self.dbHistory.deleteSyncedTransactions()
        saveLastSynced({}, ("transactions", ))
        self._lastSyncedTransactions = {}

        # refresh history tab tables
        self._refreshHistoryTabTables()

    def _deleteCsvTransactions(self):
        """ deletes all type-csv transactions from CryptoKarMa.db """
        self.dbHistory.deleteCsvTransactions()

        # refresh history tab tables
        self._refreshHistoryTabTables()

# ### BALANCE CHECK #####################################################################################

    def _refreshBalances(self):
        """ Refreshes balance check table
            compares balance from myPortfolio with balance calculated from transactions
            and sets proper color based on this comparison """
        tableWidget = self.ui.tableWidget_balanceCheck
        # sorting must be disabled when changin rowCount otherwise table will be incorrectly populated
        tableWidget.setSortingEnabled(False)
        tableWidget.setRowCount(0)
        for coinIdx, (coinName, coinInfo) in enumerate(common.coins.items()):
            tableWidget.setRowCount(tableWidget.rowCount() + 1)

            # load balances as synced in my_portfolio tab
            item = QTableWidgetItemEnhanced(coinName)
            tableWidget.setItem(coinIdx, 0, item)
            portfolioCoinBalance = coinInfo["column"]["Total"]
            item = QTableWidgetItemEnhanced(reasonableStr(portfolioCoinBalance))
            tableWidget.setItem(coinIdx, 1, item)

            # calculate the balance of the coin based on all transactions
            coinTransactions = self.dbHistory.readFromDb(
                table="transactions",
                columns=("amount",),
                where={
                    "operator": "AND",
                    "data": ("coin='{}'".format(coinName), "wallet!='Fiat'"),
                },
            )
            transactionsCoinBalance = sum([coinTransaction["amount"] for coinTransaction in coinTransactions])

            # round the result to 5 significant digits and ignore small amounts
            if transactionsCoinBalance > 0.00001 or transactionsCoinBalance < -0.00001:
                shownTransactionBalance = reasonableStr(transactionsCoinBalance)
            else:
                shownTransactionBalance = "0"
            item = QTableWidgetItemEnhanced(shownTransactionBalance)
            tableWidget.setItem(coinIdx, 2, item)

            # check that portfolio balance and transaction balance are equal (or very similar)
            # this checks that all transactions are loaded
            # tolerate some small error
            checkRangeMinWarn = transactionsCoinBalance * 0.9995
            checkRangeMaxWarn = transactionsCoinBalance * 1.0005
            checkRangeMin = transactionsCoinBalance * 0.995
            checkRangeMax = transactionsCoinBalance * 1.005

            # calculate amount under which the coin is almost zero and the check is OK
            if common.coins[coinName]["column"]["Price"] != 0:  # handle ZeroDivisionError
                price = common.coins[coinName]["column"]["Price"]
            else:
                price = 0.000001

            if common.bases["current"] == "CZK":
                smallAmount = 50 / price
            elif common.bases["current"] == "USD":
                smallAmount = 2 / price
            elif common.bases["current"] == "EUR":
                smallAmount = 2 / price
            elif common.bases["current"] == "BTC":
                smallAmount = 0.00015 / price

            # perform the check
            # abs is for a case that the balance is negative (would have to turn the signs otherwise)
            if (abs(checkRangeMinWarn) < abs(portfolioCoinBalance) < abs(checkRangeMaxWarn)
                    or (-smallAmount < portfolioCoinBalance < smallAmount and -smallAmount < transactionsCoinBalance < smallAmount)):
                # balance equal - all transactions loaded - GREEN color of the row
                for item in [tableWidget.item(coinIdx, column) for column in range(tableWidget.columnCount())]:
                    item.setBackground(QBrush(styles.BRUSH_BALANCE_OK))
            elif checkRangeMin < portfolioCoinBalance < checkRangeMax:
                # balances not equal but close - ORANGE WARNING color of the row
                for item in [tableWidget.item(coinIdx, column) for column in range(tableWidget.columnCount())]:
                    item.setBackground(QBrush(styles.BRUSH_BALANCE_WARN))
            else:
                # balances not equal at all - RED ERROR color of the row
                for item in [tableWidget.item(coinIdx, column) for column in range(tableWidget.columnCount())]:
                    item.setBackground(QBrush(styles.BRUSH_BALANCE_NOK))

        tableWidget.setSortingEnabled(True)

# ### ROI calculation #####################################################################################

    def _baseChanged(self, base):
        """ base changed in comboBox_roiBase - change base comboBox in myPortfolio """
        self.ui.comboBox_baseCurrencies.setCurrentText(base)
        self._refreshRoi()

    def _refreshRoi(self):
        """ fill ROI table with current data (current base, current coins, current transactions...) """
        # get currently chosen base
        base = self.ui.comboBox_roiBases.currentText()

        tableWidget = self.ui.tableWidget_roi
        # sorting must be disabled when changin rowCount otherwise table will be incorrectly populated
        tableWidget.setSortingEnabled(False)
        tableWidget.setRowCount(0)
        for coinName, coinInfo in common.coins.items():
            # ROI makes sense only for cryptos, not FIATS
            if coinInfo["isCrypto"]:
                # add table row
                tableWidget.setRowCount(tableWidget.rowCount() + 1)
                rowIdx = tableWidget.rowCount() - 1
                # insert coin name in the first column cell of this row
                item = QTableWidgetItemEnhanced(coinName)
                tableWidget.setItem(rowIdx, 0, item)

                # get all related transactions from db (all buy/sell transactions)
                roiTransactions = self.dbHistory.readFromDb(
                    table="transactions",
                    columns=("amount", "coinOppo", "btcValue", "usdValue", "czkValue", "eurValue"),
                    where={
                        "operator": "AND",
                        "data": ("coin='{}'".format(coinName), "operation='BUY/SELL'"),
                    },
                )

                # get a list of all opposite coins from all of this coin's transactions
                coinsOppo = set([transaction["coinOppo"] for transaction in roiTransactions])
                # determine type of ROI calculation based on opposite coins
                type_ = "calculated"
                if base in coinsOppo:
                    type_ = "mixed" if len(coinsOppo) > 1 else "precise"
                # insert ROI calculation type as a second cell of this row
                item = QTableWidgetItemEnhanced(type_)
                tableWidget.setItem(rowIdx, 1, item)

                # calculate absolute roi = - buys + sells + current value
                # buys value have + sign in database, sells have -
                buysValue = sum([roiTransactions["{}Value".format(base)] for roiTransactions in roiTransactions
                                 if roiTransactions["{}Value".format(base)] > 0])
                sellsValue = -sum([roiTransactions["{}Value".format(base)] for roiTransactions in roiTransactions
                                   if roiTransactions["{}Value".format(base)] < 0])
                # for this to work, comboBox bases must be in sync with myPortfolio comboBox bases
                currentValue = common.coins[coinName]["column"]["Value"]
                absoluteRoi = -buysValue + sellsValue + currentValue
                absoluteRoiStr = "+" + reasonableStr(absoluteRoi) if absoluteRoi > 0 else reasonableStr(absoluteRoi)
                # insert absolute ROI as a second cell of this row
                item = QTableWidgetItemEnhanced(absoluteRoiStr)
                tableWidget.setItem(rowIdx, 2, item)

                # calculate ROI in %: (sells + currentValue) / buys
                try:
                    roi = "{:,.1f} %".format(((sellsValue + currentValue) / buysValue - 1) * 100).replace(",", "")
                except ZeroDivisionError:
                    roi = "+ ∞"
                # insert ROI in % as a second cell of this row
                item = QTableWidgetItemEnhanced(roi)
                tableWidget.setItem(rowIdx, 3, item)

                for item in [tableWidget.item(rowIdx, column) for column in range(tableWidget.columnCount())]:
                    if superfloat(roi) > 100 or roi == "+ ∞":
                        item.setBackground(QBrush(styles.BRUSH_ROI_GREAT))
                    elif superfloat(roi) > 0:
                        item.setBackground(QBrush(styles.BRUSH_ROI_GOOD))
                    elif superfloat(roi) > -50:
                        item.setBackground(QBrush(styles.BRUSH_ROI_BAD))
                    else:
                        item.setBackground(QBrush(styles.BRUSH_ROI_FUCKED))

        tableWidget.setSortingEnabled(True)

########################################################################################################################################
#    ____                 _
#   / ___|_ __ __ _ _ __ | |__  ___
#  | |  _| '__/ _` | '_ \| '_ \/ __|
#  | |_| | | | (_| | |_) | | | \__ \
#   \____|_|  \__,_| .__/|_| |_|___/
#                  |_|
########################################################################################################################################

    def _graphCoinsAllCryptoClicked(self, checked):
        """ all crypto coins checkBox changed state in graphs frame according to the clicked all crypto checkbox
            all other coins' checkBoxes will be deselected """
        for coin, checkBox_coin in self._widgets["graphs"]["coins"].items():
            if common.coins[coin]["isCrypto"] is True:
                checkBox_coin.setChecked(checked)
            else:
                checkBox_coin.setChecked(False)
        self._enableDisableGraphTypes()

    def _graphWalletsAllClicked(self, checked):
        """ all wallets checkBox changed state in graphs frame """
        for checkBox_wallet in self._widgets["graphs"]["wallets"].values():
            checkBox_wallet.setChecked(checked)

    def _graphBasesAllClicked(self, checked):
        """ all bases checkBox changed state in graphs frame """
        for checkBox_base in self._widgets["graphs"]["bases"].values():
            checkBox_base.setChecked(checked)
        self._enableDisableGraphTypes()

    def _graphTimeframeClicked(self, checked, radioButton):
        """ ensures proper radioButton switching in frame Timeframe """
        self.ui.radioButton_graphAll.setChecked(False)
        self.ui.radioButton_graphYear.setChecked(False)
        self.ui.radioButton_graphLast.setChecked(False)
        radioButton.setChecked(True)

    def _graphSettingsYearChanged(self, text):
        """ When year changes in timeframe frame in graph settings, select corresponding radio button"""
        self._graphTimeframeClicked(True, self.ui.radioButton_graphYear)

    def _graphSettingsLastChanged(self, text):
        """ When last changes in timeframe frame in graph settings, select corresponding radio button"""
        self._graphTimeframeClicked(True, self.ui.radioButton_graphLast)

    def _checkBoxCoinClicked(self, checked, coin):
        """ a coin checkBox in graph settings was clicked """
        if checked is False:
            self.ui.checkBox_coinsAllCrypto.setChecked(False)
        if checked is True and common.coins[coin]["isCrypto"] is False:
            self.ui.checkBox_coinsAllCrypto.setChecked(False)
        self._enableDisableGraphTypes()

    def _checkBoxWalletClicked(self, checked, wallet):
        """ a wallet checkBox in graph settings was clicked """
        if checked is False:
            self.ui.checkBox_walletsAll.setChecked(False)

    def _checkBoxBaseClicked(self, checked, base):
        """ a base checkBox in graph settings was clicked """
        if checked is False:
            self.ui.checkBox_basesAll.setChecked(False)
        self._enableDisableGraphTypes()

    def _enableDisableGraphTypes(self):
        # all base checkboxes are unchecked = disable value and price type graph
        if any([checkBox_base.isChecked() for checkBox_base in self._widgets["graphs"]["bases"].values()]):
            # all crypto selected = disable amount and price
            if self.ui.checkBox_coinsAllCrypto.isChecked():
                self.ui.checkBox_graphTypeAmount.setEnabled(False)
                self.ui.checkBox_graphTypePrice.setEnabled(False)
                self.ui.checkBox_graphTypeValue.setEnabled(True)
                self.ui.checkBox_graphTypeAmount.setChecked(False)
                self.ui.checkBox_graphTypePrice.setChecked(False)
            else:
                self.ui.checkBox_graphTypeAmount.setEnabled(True)
                self.ui.checkBox_graphTypePrice.setEnabled(True)
                self.ui.checkBox_graphTypeValue.setEnabled(True)
        else:
            if self.ui.checkBox_coinsAllCrypto.isChecked():
                self.ui.checkBox_graphTypeAmount.setEnabled(False)
                self.ui.checkBox_graphTypePrice.setEnabled(False)
                self.ui.checkBox_graphTypeValue.setEnabled(False)
                self.ui.checkBox_graphTypeAmount.setChecked(False)
                self.ui.checkBox_graphTypePrice.setChecked(False)
                self.ui.checkBox_graphTypeValue.setChecked(False)
            else:
                self.ui.checkBox_graphTypeAmount.setEnabled(True)
                self.ui.checkBox_graphTypePrice.setEnabled(False)
                self.ui.checkBox_graphTypeValue.setEnabled(False)
                self.ui.checkBox_graphTypePrice.setChecked(False)
                self.ui.checkBox_graphTypeValue.setChecked(False)

###################################################################################################################################

    def _getSelectedCoins(self):
        """ returns list of selected coin names from graph settings checkboxes
            also return a string of selected coins that can be used in sqlite where clause """
        # get all graph settings from checkboxes

        # ALL CRYPTO selected
        if self.ui.checkBox_coinsAllCrypto.isChecked():
            selectedCoins = "allCrypto"
            # all transactions will be fetched, fiats will be used to calculate investment
            selectedCoinsStr = ""
        # ALL CRYPTO NOT selected
        else:
            selectedCoins = []
            selectedCoinsStr = []
            for coinName, checkBox_coin in self._widgets["graphs"]["coins"].items():
                if checkBox_coin.isChecked():
                    selectedCoins.append(coinName)
                    selectedCoinsStr.append("coin='{}'".format(coinName))

            # no coins selected
            if not selectedCoins:
                raise GraphCreationError("No coin selected")

        return selectedCoins, selectedCoinsStr

    def _getSelectedWallets(self):
        """ returns list of selected wallet names from graph settings checkboxes
            also return a string of selected wallets that can be used in sqlite where clause """
        selectedWallets = []
        selectedWalletsStr = ""
        # get all graph settings from checkboxes

        # ALL WALLETS selected
        if self.ui.checkBox_walletsAll.isChecked():
            selectedWallets = "allWallets"
        # ALL WALLETS NOT selected
        else:
            for walletName, checkBox_wallet in self._widgets["graphs"]["wallets"].items():
                if checkBox_wallet.isChecked():
                    selectedWallets.append(walletName)
            selectedWalletsStr = "(" + " OR ".join(["wallet='{}'".format(walletName) for walletName in selectedWallets]) + ")"

            # no wallets selected
            if not selectedWallets:
                raise GraphCreationError("No wallet selected")

        return selectedWallets, selectedWalletsStr

    def _getGraphTransactionsFromDb(self, whereStr):
        """ calls readFromDb function with right parameters to get trransactions in format for graph """
        return self.dbHistory.readFromDb(
            table="transactions",
            columns=("timestamp", "coin", "amount", "operation"),
            orderBy="timestamp ASC",
            where={
                "operator": "STR",
                "data": whereStr,
            }
        )

    def _getGraphTransactions(self):
        """ gets and returns all transactions according to the graph settings
            from which the graph will be plotted
            returns:
                transactions(dict) - {coinName (or "allCrypto"): transactions(sqlRow)}"""
        # process selected coins and wallets and report errors if configuration is not plausible
        selectedCoins, selectedCoinsStr = self._getSelectedCoins()
        selectedWallets, selectedWalletsStr = self._getSelectedWallets()

        # get all transactions based on selected inputs
        transactions = {}
        if selectedCoins == "allCrypto":
            whereStr = selectedWalletsStr if selectedWalletsStr else ""
            transactions["allCrypto"] = self._getGraphTransactionsFromDb(whereStr)
            # take only transactions that are made with coins that are in myPortfolio (common.coins)
            transactions["allCrypto"] = [transaction for transaction in transactions["allCrypto"] if transaction["coin"] in common.coins]
        elif len(selectedCoins) <= 3:
            for coin, coinWhereStr in zip(selectedCoins, selectedCoinsStr):
                whereStr = coinWhereStr + " AND " + selectedWalletsStr if selectedWalletsStr else coinWhereStr
                transactions[coin] = self._getGraphTransactionsFromDb(whereStr)
        else:
            raise GraphCreationError("Too many coins selected")

        return transactions

    def _calculateNrOfGraphLines(self, transactions, selectedTypes, selectedBases):
        """ calculates number of lines that souhld be plotted in future graph
            base on inputs from checkboxes and check if the value is within range
            args:
                transactions(dict) {coinName (or "allCrypto"): transactions}
                selectedTypes(list of strings) - example ["price","amount"]
                selectedBases(list of strings) - example ["BTC","CZK"] """
        # calculate the number of graph lines to be plotted
        nrOfGraphLines = 0
        if "amount" in selectedTypes:
            nrOfGraphLines += len(transactions)     # len(transactions) is number of selected coins
        for type_ in ["value", "price"]:
            if type_ in selectedTypes:
                nrOfGraphLines += len(transactions) * len(selectedBases)
        if "allCrypto" in transactions:     # addictional line for investment value
            nrOfGraphLines += len(selectedBases)

        if nrOfGraphLines < 1:
            raise GraphCreationError("No type or base selected")

        if nrOfGraphLines > 10:
            raise GraphCreationError("Too many graph lines selected \nLower the number of selected coins, bases or types")

    def _calculateTimeframe(self, transactions):
        """ based on Timeframe frame inputs in GUI calculate first and last timestamps
            to set the X axis of the future graph
            ruturns:
                rounded timestamps to day, 10-digit format """
        # "ALL" will find the oldest timestamp from all transactions and set it as first
        if self.ui.radioButton_graphAll.isChecked():
            try:
                firstTimestamp = min(
                    [transaction[0]["timestamp"] for transaction in transactions.values() if len(transaction) > 0])
            except ValueError:  # min on empty list - no transactions loaded
                raise GraphCreationError("No transactions loaded")
            lastTimestamp = int(datetime.utcnow().timestamp() * 1000)
        # "Year" will return first and last timestamp of that year
        elif self.ui.radioButton_graphYear.isChecked():
            year = int(self.ui.comboBox_graphYear.currentText())
            firstTimestamp = int(datetime(year, 1, 1).timestamp()) * 1000
            lastTimestamp = int(datetime(year + 1, 1, 1).timestamp()) * 1000
        # "Last" will return timestamp now as lastTimestamp and timestamp X ago as firstTimestamp
        elif self.ui.radioButton_graphLast.isChecked():
            lastTimestamp = int(datetime.utcnow().timestamp() * 1000)
            text = self.ui.comboBox_GraphLast.currentText()
            if "year" in text:
                firstTimestamp = int((datetime.utcnow() - relativedelta(years=int(text[0]))).timestamp()) * 1000
            elif "month" in text:
                firstTimestamp = int((datetime.utcnow() - relativedelta(months=int(text[0]))).timestamp()) * 1000
            else:
                raise GraphCreationError("developer error - Timeframe settings that should not be possible to trigger")
        else:
            raise GraphCreationError("developer error - Timeframe settings that should not be possible to trigger")

        return roundTimestampToDay(int(firstTimestamp / 1000)), roundTimestampToDay(int(lastTimestamp / 1000))

    def _prepareGraphDicts(self, transactions, selectedBases, selectedTypes, selectedComparisons, firstTimestamp, lastTimestamp):
        """ prepares all graphs that will be plotted
            args:
                transactions (dict) - 1st layer - {"allCrypto": transactions} or {"coinName": transactions}
                    transactions (dict) - 2nd layer - {"timestamp", "coin", "amount", "operation"}
                selectedBases (list of strings) - ["CZK", "USD", "BTC"]
                selectedTypes (list of strings) - ["amount", "price", "value"]
                selectedComparisons (list of strings) - ["BTC/CZK", "BTC/USD"]
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
            returns:
                graphDicts (dict) - {coin: graphType: (bases optional:) {timestamp, value}}
        """
        graphDicts = {}

        if "allCrypto" in transactions:
            # prepare value graph of all crypto combined + fiat investment graph
            graphDicts["allCrypto"], graphDicts["fiatInvestment"] = self._prepareAllCryptoValueHistory(
                transactions["allCrypto"], selectedBases, firstTimestamp, lastTimestamp
            )
        else:
            # graphs of separate coins
            for coin, coinTransactions in transactions.items():
                # add this coin to graphs dict
                graphDicts[coin] = {}

                if "amount" in selectedTypes or "value" in selectedTypes:
                    # get amount history to be used for amount or value graph accoding to selection
                    amountHistoryDaily = self._prepareAmountHistory(coin, coinTransactions, firstTimestamp, lastTimestamp)
                    if "amount" in selectedTypes:
                        graphDicts[coin]["amount"] = amountHistoryDaily

                if selectedBases:
                    for base in selectedBases:
                        if "price" in selectedTypes or "value" in selectedTypes:
                            # get price history to be used for price or value graph accoding to selection
                            priceHistoryDaily = self._preparePriceHistory(coin, base, firstTimestamp, lastTimestamp)
                            if "price" in selectedTypes:
                                if "price" not in graphDicts[coin]:
                                    graphDicts[coin]["price"] = {}
                                graphDicts[coin]["price"][base] = priceHistoryDaily

                        if "value" in selectedTypes:
                            # calculate value history if selected
                            try:
                                valueHistoryDaily = {timestamp: amountHistoryDaily[timestamp] * priceHistoryDaily[timestamp]
                                                     for timestamp in amountHistoryDaily}
                                if "value" not in graphDicts[coin]:
                                    graphDicts[coin]["value"] = {}
                                graphDicts[coin]["value"][base] = valueHistoryDaily
                            except KeyError:    # if BTC/BTC price requested, no prices are loaded
                                pass

        # add price comparison graphs
        if selectedComparisons:
            graphDicts["comparisons"] = {}

            for comparison in selectedComparisons:
                coin, base = comparison.split("/")
                # get price history to be used for price or value graph accoding to selection
                priceHistoryDaily = self._preparePriceHistory(coin, base, firstTimestamp, lastTimestamp)

                graphDicts["comparisons"][comparison] = priceHistoryDaily

        return graphDicts

    def _prepareAmountHistory(self, coin, coinTransactions, firstTimestamp, lastTimestamp):
        """ prepares amount historyfor passed coin
            args:
                coin (str) - "BTC"
                coinTransactions - (list of sqliteTransactions) - {"timestamp", "coin", "amount", "operation"}
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
            returns:
                amountHistoryDaily - (dict) of {timestamp: amount}
        """
        # dict of {timestamp: amount} amount of this coin right after this timestamp
        amountHistory = {}
        currentAmount = 0
        for transaction in coinTransactions:
            # filter out transactions out of timeframe
            timestamp = roundTimestampToDay(int(transaction["timestamp"] / 1000))
            currentAmount += transaction["amount"]
            amountHistory[timestamp] = currentAmount

        amountHistoryDaily = self._convertHistoryToDaily(amountHistory, firstTimestamp, lastTimestamp)

        return amountHistoryDaily

    def _preparePriceHistory(self, coin, base, firstTimestamp, lastTimestamp):
        """ prepares prices history for passed coin/base pair
            args:
                coin (str) - "ETH"
                base (str) - "CZK"
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
            returns:
                priceHistoryDaily - (dict) of {timestamp: price}
        """
        prices = self.dbHistory.readFromDb(
            table="priceHistory", columns=("timestamp", "price"), orderBy="timestamp ASC",
            where={"operator": "AND", "data": ("base='{}'".format(base), "coin='{}'".format(coin)), },
        )
        priceHistoryDaily = {price["timestamp"]: price["price"] for price in prices
                             if firstTimestamp <= price["timestamp"] <= lastTimestamp}

        return priceHistoryDaily

    def _convertHistoryToDaily(self, history, firstTimestamp, lastTimestamp):
        """ converts history dictionary which does not contain values from all days
            to a dictionary with the same format but containing all daily values
            args:
                history - (dict) of {timestamp: amount}
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
            returns:
                dailyHistory - (dict) of {timestamp: amount}
        """
        lastValue = 0
        # load the first value corresponding to the first timestamp
        # if the timestamp is not in history, go back in time
        for timestamp in range(firstTimestamp, common.OLDEST_TIMESTAMP, -86_400):
            if timestamp in history:
                # you found the most recent value
                lastValue = history[timestamp]
                break

        dailyHistory = {}
        for timestampDayly in range(firstTimestamp, lastTimestamp + 86_400, 86_400):
            if timestampDayly in history:
                dailyHistory[(int(timestampDayly))] = history[timestampDayly]
                lastValue = history[timestampDayly]
            else:
                dailyHistory[(int(timestampDayly))] = lastValue

        return dailyHistory

    def _prepareAllCryptoValueHistory(self, transactions, selectedBases, firstTimestamp, lastTimestamp):
        """ prepares graphs of allCrypto value history and fiat investment
            args:
                transactions (dict) - 1st layer - {"allCrypto": transactions} or {"coinName": transactions}
                    transactions (dict) - 2nd layer - {"timestamp", "coin", "amount", "operation"}
                selectedBases (list of strings) - ["CZK", "USD", "BTC"]
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
        """
        # create amount counter for each cryptoCoin
        lastAmounts = {coinName: 0 for coinName, v in common.coins.items() if v["isCrypto"] is True}
        amountHistory = {coinName: {} for coinName, v in common.coins.items() if v["isCrypto"] is True}
        # get all crypto amount changes
        for transaction in transactions:
            if common.coins[transaction["coin"]]["isCrypto"] is True:
                roundedTimestamp = roundTimestampToDay(int(transaction["timestamp"] / 1000))
                # add amount change of this coin to its amount counter
                lastAmounts[transaction["coin"]] += transaction["amount"]
                amountHistory[transaction["coin"]][roundedTimestamp] = lastAmounts[transaction["coin"]]
        # convert to every day amounts
        amountHistoryDaily = {}
        for coin, coinAmountHistory in amountHistory.items():
            amountHistoryDaily[coin] = self._convertHistoryToDaily(coinAmountHistory, firstTimestamp, lastTimestamp)

        # create amount counter for each cryptoCoin
        fiatInvestment = {coinName: 0 for coinName, v in common.coins.items() if v["isCrypto"] is False}
        fiatInvestmentHistory = {coinName: {} for coinName, v in common.coins.items() if v["isCrypto"] is False}
        # get all crypto amount changes
        for transaction in transactions:
            if common.coins[transaction["coin"]]["isCrypto"] is False:
                roundedTimestamp = roundTimestampToDay(int(transaction["timestamp"] / 1000))
                fiatInvestment[transaction["coin"]] -= transaction["amount"]
                fiatInvestmentHistory[transaction["coin"]][roundedTimestamp] = fiatInvestment[transaction["coin"]]
        # convert to every day amounts
        fiatInvestmentHistoryDaily = {}
        for coin, coinFiatInvestmentHistory in fiatInvestmentHistory.items():
            fiatInvestmentHistoryDaily[coin] = self._convertHistoryToDaily(coinFiatInvestmentHistory, firstTimestamp, lastTimestamp)

        # calculate values daily from daily amounts and prices
        valueHistoryDaily = {}
        fiatValueHistoryDaily = {}
        # do this for all selected bases
        for base in selectedBases:
            # get all base prices from prices DB
            prices = self.dbHistory.readFromDb(
                table="priceHistory", columns=("timestamp", "coin", "price"), orderBy="timestamp ASC",
                where={"operator": "AND", "data": ("base='{}'".format(base),), },
            )
            # take only prices that are made with coins that are in myPortfolio (common.coins)
            prices = [price for price in prices if price["coin"] in common.coins]

            # make a dictionary of all prices for each coin
            pricesDaily = {coinName: {} for coinName, v in common.coins.items()}
            for price in prices:
                pricesDaily[price["coin"]][price["timestamp"]] = price["price"]

            cryptoValueDaily = {timestamp: 0 for timestamp in range(firstTimestamp, lastTimestamp + 86_400, 86_400)}
            for coinName, coinsAmounts in amountHistoryDaily.items():
                for timestamp, coinAmount in coinsAmounts.items():
                    if coinName != base:
                        try:
                            cryptoValueDaily[timestamp] += coinAmount * pricesDaily[coinName][timestamp]
                        except KeyError:  # price not loaded?
                            pass
                    else:
                        # price = 1 when base = coin
                        cryptoValueDaily[timestamp] += coinAmount * 1
            valueHistoryDaily[base] = copy.deepcopy(cryptoValueDaily)

            fiatInvestmentDaily = {timestamp: 0 for timestamp in range(firstTimestamp, lastTimestamp + 86_400, 86_400)}
            for coinName, coinsAmounts in fiatInvestmentHistoryDaily.items():
                for timestamp, coinAmount in coinsAmounts.items():
                    if coinName != base:
                        try:
                            fiatInvestmentDaily[timestamp] += coinAmount * pricesDaily[coinName][timestamp]
                        except KeyError:  # price not loaded?
                            pass
                    else:
                        # price = 1 when base = coin
                        fiatInvestmentDaily[timestamp] += coinAmount * 1
            fiatValueHistoryDaily[base] = copy.deepcopy(fiatInvestmentDaily)

        return valueHistoryDaily, fiatValueHistoryDaily

    def _prepareGraphs(self, graphDicts):
        """ converts graphDicts into graphs objects
            returns:
                graphs (lists of lists of GraphData objects)"""
        graphs = []
        # add CryptoValue graph data and FiatInvestment graph data
        if "allCrypto" in graphDicts:
            for baseIdx, ((baseName, cryptoData), fiatData) in enumerate(zip(
                    graphDicts["allCrypto"].items(), graphDicts["fiatInvestment"].values())):
                graphs.append([])
                graphs[baseIdx].append(GraphData(x=list(cryptoData.keys()), y=list(cryptoData.values()), color=(0, 200 - 50 * baseIdx, 0),
                                                 text="Crypto value ({})".format(common.bases["options"][baseName]["symbol"]),
                                                 unit=common.bases["options"][baseName]["symbol"]))
                graphs[baseIdx].append(GraphData(x=list(fiatData.keys()), y=list(fiatData.values()), color=(200 - 50 * baseIdx, 0, 0),
                                                 text="Fiat investment ({})".format(common.bases["options"][baseName]["symbol"]),
                                                 unit=common.bases["options"][baseName]["symbol"]))
        # add all graph data for all coin graphs
        coinGraphDicts = {coin: data for coin, data in graphDicts.items() if coin in common.coins}
        for coinIdx, (coin, coinData) in enumerate(coinGraphDicts.items()):
            if "amount" in coinData:
                graphs.append([(GraphData(x=list(coinData["amount"].keys()),
                                          y=list(coinData["amount"].values()), color=(255 - 100 * coinIdx, 125, 125),
                                          text="{} amount".format(coin), unit=coin))])
            if "value" in coinData:
                for baseIdx, (baseName, data) in enumerate(coinData["value"].items()):
                    graphs.append([(GraphData(x=list(data.keys()), y=list(data.values()),
                                              color=(100 * coinIdx + 20, 80 * baseIdx + 50, 150),
                                              text="{} value ({})".format(coin, common.bases["options"][baseName]["symbol"]),
                                              unit=common.bases["options"][baseName]["symbol"]))])
            if "price" in coinData:
                for baseIdx, (baseName, data) in enumerate(coinData["price"].items()):
                    graphs.append([(GraphData(x=list(data.keys()), y=list(data.values()), color=(100 * coinIdx, 150, 80 * baseIdx),
                                              text="{}/{} price".format(coin, baseName)))])

        if "comparisons" in graphDicts:
            for compIdx, (comparison, data) in enumerate(graphDicts["comparisons"].items()):
                graphs.append([(GraphData(x=list(data.keys()), y=list(data.values()),
                                          color=(100 + 100 * compIdx, 100 + 100 * compIdx, 100 + 100 * compIdx),
                                          text="{} price".format(comparison)))])

        return graphs

    def _prepareXAxis(self, lastTimestamp, firstTimestamp):
        """ creates and returns TimeAxisItem with set X axis
            args:
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph
            returns:
                date_axis (TimeAxisItem)"""
        # X axis creation and definition
        date_axis = TimeAxisItem(orientation='bottom')
        # if the timeframe is larger than a year, plot years on X axis
        if lastTimestamp - firstTimestamp > 31_622_400:
            # create a list of timestamp corresponding to the first second of years 2008 - 2100
            xAxisTimestamps = [int(datetime(year=year, month=1, day=1).timestamp()) for year in range(2008, 2100)]
        else:
            # create a list of timestamp corresponding to the first second of every month in years 2008 - 2100
            xAxisTimestamps = [int(datetime(year=year, month=month, day=1).timestamp())
                               for year in range(2008, 2100)
                               for month in range(1, 12 + 1)]
        # set these timestamps as X axis ticks
        dx = [(value, datetime.utcfromtimestamp(value + 3600).strftime("%d.%m.%Y")) for value in xAxisTimestamps]
        date_axis.setTicks([dx, []])
        date_axis.setLabel("<span style='font-size: 12pt'>Time", units="<span style='font-size: 12pt'>day")

        return date_axis

    def _plotGraphs(self, graphs, lastTimestamp, firstTimestamp):
        """ create graph window and plot all graph lines from graphs objects
            args:
                graphs (lists of lists of GraphData objects)
                firstTimestamp (int 10-digit) - first timestamp that will be included in the graph
                lastTimestamp (int 10-digit) - last timestamp that will be included in the graph"""
        # create graph window as object (will be added to mainWindow for the graphWindow not to die when created)
        graphWindow = pyqtgraph.GraphicsWindow(parent=None, title="CryptoKarMa super graph!!!")
        graphWindow.resize(1200, 800)
        # prepare crosshair label above the plot window
        crosshairLabel = pyqtgraph.LabelItem(justify='right', text="Move your mouse in the graph to see some magic")
        graphWindow.addItem(crosshairLabel)
        # set x axis
        date_axis = self._prepareXAxis(lastTimestamp, firstTimestamp)

        self.plotList = []
        # go through all plots (plots mnvay contain multiple lines - investment vs value plot)
        for plotIdx, graphsThisPlot in enumerate(graphs):
            # set the Y axis
            orientation = "left" if plotIdx == 0 else "right"
            Y_axis = YAxisItem(orientation=orientation, pen=graphsThisPlot[0].color, unit=graphsThisPlot[0].unit)
            axisName = "".join(["<span style='color: rgb{}'>{}     ".format(graphsThisPlot[idx].color, graphsThisPlot[idx].text)
                                for idx in range(len(graphsThisPlot))])
            Y_axis.setLabel(text=axisName, units="", **{"font-size": "12pt"})

            # create plots
            if plotIdx == 0:
                # the first plotItem is created normally
                plotItem = graphWindow.addPlot(row=1, col=0, axisItems={'bottom': date_axis, "left": Y_axis})
                plotItem.showGrid(x=True, y=True)
                plotItem.show()
                # connect the first plot item to graphical window
                graphWindow.vb = plotItem.vb
            else:
                # other plots with separate Y axes are created as ViewBox connected to the 1st plotItem
                plotItem = pyqtgraph.ViewBox()
                self.plotList[0].layout.addItem(Y_axis, 2, 2 + plotIdx)
                self.plotList[0].scene().addItem(plotItem)
                Y_axis.linkToView(plotItem)
                plotItem.setXLink(self.plotList[0])
            # add this new plotItem to the list
            self.plotList.append(plotItem)

            # add all lines dedicated to this plot
            for lineIdx, lineData in enumerate(graphsThisPlot):
                if plotIdx == 0:
                    self.plotList[-1].plot(lineData.x, lineData.y, pen=lineData.color)
                else:
                    self.plotList[-1].addItem(pyqtgraph.PlotCurveItem(lineData.x, lineData.y, pen=lineData.color))

        # Create crosshair lines
        vLine = pyqtgraph.InfiniteLine(angle=90, movable=False)
        self.plotList[0].addItem(vLine, ignoreBounds=True)
        hLines = []
        for plotIdx, graphsThisPlot in enumerate(graphs):
            for lineData in graphsThisPlot:
                hLines.append(pyqtgraph.InfiniteLine(angle=0, movable=False, pen=lineData.color))
                self.plotList[plotIdx].addItem(hLines[-1], ignoreBounds=True)

        # reaction on mouse moving over the graph
        def mouseMoved(evt):
            """ move crosshairs to the position of the mouse
                and show the position x and y values in crosshair label"""
            position = evt[0]  # using signal proxy turns original arguments into a tuple
            if self.plotList[0].sceneBoundingRect().contains(position):
                mousePoint = graphWindow.vb.mapSceneToView(position)
                # get timestamp from x position
                xPosTimestamp = roundTimestampToDay(mousePoint.x())
                if xPosTimestamp > lastTimestamp:  # out of scope of the graph
                    xPosTimestamp = lastTimestamp
                if xPosTimestamp < firstTimestamp:  # out of scope of the graph
                    xPosTimestamp = firstTimestamp
                # get time and date from the mouse x position
                timestampIndex = graphs[0][0].x.index(roundTimestampToDay(xPosTimestamp))
                dateAndTime = datetime.fromtimestamp(int(xPosTimestamp)).strftime("%d.%m.%Y %H:%M")
                # start creating the crosshair label text from x axis
                crosshairLabelText = "<span style='font-size: 13pt'>Date, time: {}".format(dateAndTime)
                # for every line, add text to crosshair label
                for plotIdx, graphsThisPlot in enumerate(graphs):
                    for lineIdx, lineData in enumerate(graphsThisPlot):
                        crosshairLabelText += "<span style='color: rgb{}'> \t{}: {} </span>".format(
                            str(lineData.color), lineData.text, reasonableStr(lineData.y[timestampIndex]))
                crosshairLabel.setText(crosshairLabelText)
                # set positions of vline and hlines of the crosshair
                vLine.setPos(xPosTimestamp)
                hLineIdx = 0
                for plotIdx, graphsThisPlot in enumerate(graphs):
                    for lineIdx in range(len(graphsThisPlot)):
                        hLines[hLineIdx].setPos(graphs[plotIdx][lineIdx].y[timestampIndex])
                        hLineIdx += 1

        graphWindow.proxy = pyqtgraph.SignalProxy(self.plotList[0].scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)

        # limit tho zoomout to the size of the graph when first plotted
        range_ = self.plotList[0].getViewBox().viewRange()
        self.plotList[0].getViewBox().setLimits(xMin=range_[0][0], xMax=range_[0][1],
                                                yMin=range_[1][0], yMax=range_[1][1])

        # Handle view resizing
        def updateViews():
            # view has resized; update auxiliary views to match
            for plotItem in self.plotList[1:]:
                plotItem.setGeometry(self.plotList[0].vb.sceneBoundingRect())
                plotItem.linkedViewChanged(self.plotList[0].vb, plotItem.XAxis)

        updateViews()
        self.plotList[0].vb.sigResized.connect(updateViews)

        # add graphWindow to mainWindow so it is not destroyed until mainwindow is closed
        self.mainWindow.graphWindows.append(graphWindow)

    def _plotGraphClicked(self):
        """ the button Plot graph was clicked,
            gather all information about the future graph and plot it """
        try:
            transactions = self._getGraphTransactions()

            # get selected bases and types in a list
            selectedBases = [baseName for baseName, chB in self._widgets["graphs"]["bases"].items() if chB.isChecked()]
            selectedTypes = [type_.lower() for type_, chB in self._widgets["graphs"]["types"].items() if chB.isChecked()]
            selectedComparisons = [comparison for comparison, chB in self._widgets["graphs"]["comparisons"].items() if chB.isChecked()]

            self._calculateNrOfGraphLines(transactions, selectedTypes, selectedBases)
            firstTimestamp, lastTimestamp = self._calculateTimeframe(transactions)

        except GraphCreationError as e:
            errorMessage("Impossible settings",
                         "With these settings, CryptoKarMa is unable to plot a graph\nReason: {}".format(e))
            return

        graphDicts = self._prepareGraphDicts(transactions, selectedBases, selectedTypes, selectedComparisons, firstTimestamp, lastTimestamp)
        # prepare GraphData object lists
        graphs = self._prepareGraphs(graphDicts)
        # PLOT IT ALL
        self._plotGraphs(graphs, lastTimestamp, firstTimestamp)


class GraphCreationError(Exception):
    """ raise this exception when combination of graph settings is impossible to create a graph """


class YAxisItem(pyqtgraph.AxisItem):
    def __init__(self, unit, orientation, pen=None, textPen=None, linkView=None, parent=None, maxTickLength=-5, showValues=True,
                 text='', units='', unitPrefix='', **args):
        super().__init__(orientation, pen=pen, textPen=textPen, linkView=linkView, parent=parent, maxTickLength=maxTickLength,
                         showValues=showValues, text=text, units=units, unitPrefix=unitPrefix, **args)
        self.enableAutoSIPrefix(False)
        self.unit = unit

    def tickStrings(self, values, scale, spacing):
        return [(reasonableStr(value) + " " + self.unit) for value in values]


class TimeAxisItem(pyqtgraph.AxisItem):

    def __init__(self, orientation, pen=None, textPen=None, linkView=None, parent=None, maxTickLength=-5, showValues=True,
                 text='', units='', unitPrefix='', **args):
        super().__init__(orientation, pen=pen, textPen=textPen, linkView=linkView, parent=parent, maxTickLength=maxTickLength,
                         showValues=showValues, text=text, units=units, unitPrefix=unitPrefix, **args)
        self.enableAutoSIPrefix(False)

    def tickStrings(self, values, scale, spacing):
        return [datetime.fromtimestamp(value) for value in values]
