
import yagmail
import json
from datetime import datetime

from designer.CryptoKarMa_mainWindow import Ui_MainWindow
from exchanges.coinmate import CoinmateClient
from exchanges.bitfinex import BitfinexClient
from exchanges.bittrex_v3 import BittrexClient
from exchanges.binance_my import BinanceClient
from exchanges.coinbase import CoinbaseClient
from exchanges.coinbasepro import CoinbaseProClient
from exchanges.kucoin_my import KucoinClient
from exchanges.ledger import LedgerClient
from exchanges.neon import NeonClient
from exchanges.other import DummyClient
from exchanges.trezor import TrezorClient
from exchanges.cryptoCompare import CryptoCompareClient

from com.dbControll import DbHistory

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QIcon


from tabs.my_portfolio import MyPortfolio
from tabs.history import History
from tabs.notifications import Notifications
from tabs.orders import Orders
from com.common import readApiKeys, reasonableStr
import com.common as common
import com.styles as styles
from com.common import sendMailNotification


class CryptoKarMa_mainWindow(QMainWindow):
    def __init__(self):
        super(CryptoKarMa_mainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.tabWidget_main.setCurrentIndex(0)

        self.setWindowIcon(QIcon(common.PATH + "/icons/CryptoKarMa.png"))

        apiKeys = readApiKeys()
        # create object for every wallet client
        self.walletClients = self._initWalletClients(apiKeys)
        self.cryptoCompare = self._initCryptoCompare(apiKeys)

        # initialize yagMail object for notifications
        self.yagMail = self._initNotifications(apiKeys)

        # create historydb object and create its tables if not already exists
        self.dbHistory = DbHistory()
        self.dbHistory.createTransactionsTable()
        self.dbHistory.createPriceHistoryTable()
        self.dbHistory.createOpenOrdersTable()

        self.show()

        self.tabs = {
            "myPortfolio": MyPortfolio(
                "myPortfolio", "My portfolio", self.ui, self.walletClients, self.cryptoCompare, self.dbHistory, self.yagMail),
            "notifications": Notifications("notifications", "Notifications", self.ui),
            "history": History("history", "History", self, self.ui, self.walletClients, self.cryptoCompare, self.dbHistory, self.yagMail),
            "orders": Orders("orders", "Orders", self.ui, self.walletClients, self.dbHistory, self.yagMail),
        }

        self._initAutoRefresh()
        self._initDailyEvents()

        self.ui.tabWidget_main.currentChanged.connect(self._tabChanged)

    def _initWalletClients(self, apiKeys):
        """ crates an object of every wallet class and adds a pair of its name (string) and its object to dictionary """
        walletClients = {}

        # initialize wallet client based on passed keys

        # Ledger must be set to active
        if "Ledger" in apiKeys and "active" in apiKeys["Ledger"] and apiKeys["Ledger"]["active"] is True:
            # if there is a TEZOS address defined, pass it to Client constructor
            TEZOS_ADDRESS = apiKeys["Ledger"]["TEZOS_ADDRESS"] if "TEZOS_ADDRESS" in apiKeys["Ledger"] else None
            walletClients["Ledger"] = LedgerClient(TEZOS_ADDRESS)
            self._enableButton(self.ui.pushButton_loadLedgerCsv)

        # Coinmate has keys and client ID
        if ("Coinmate" in apiKeys and "CLIENT_ID" in apiKeys["Coinmate"]
                and "PUBLIC_KEY" in apiKeys["Coinmate"] and "PRIVATE_KEY" in apiKeys["Coinmate"]):
            CLIENT_ID = apiKeys["Coinmate"]["CLIENT_ID"]
            PUBLIC_KEY = apiKeys["Coinmate"]["PUBLIC_KEY"]
            PRIVATE_KEY = apiKeys["Coinmate"]["PRIVATE_KEY"]
            walletClients["Coinmate"] = CoinmateClient(CLIENT_ID, PUBLIC_KEY, PRIVATE_KEY)

        # the following exchanges have the same pattern in api keys
        exchanges = {
            "Bitfinex": BitfinexClient,
            "Bittrex": BittrexClient,
            "Coinbase": CoinbaseClient
        }
        for name, client in exchanges.items():
            if name in apiKeys and "KEY" in apiKeys[name] and "SECRET" in apiKeys[name]:
                KEY = apiKeys[name]["KEY"]
                SECRET = apiKeys[name]["SECRET"]
                walletClients[name] = client(KEY, SECRET)

        # Binance need to get self.ui to be able to controll progressBar
        if "Binance" in apiKeys and "KEY" in apiKeys["Binance"] and "SECRET" in apiKeys["Binance"]:
            KEY = apiKeys["Binance"]["KEY"]
            SECRET = apiKeys["Binance"]["SECRET"]
            walletClients["Binance"] = BinanceClient(KEY, SECRET, self.ui)

        # Coinbase Pro has PASSPHRASE and need to get self.ui to be able to controll progressBar
        if ("CB Pro" in apiKeys and "KEY" in apiKeys["CB Pro"] and "SECRET" in apiKeys["CB Pro"]
                and "PASSPHRASE" in apiKeys["CB Pro"]):
            KEY = apiKeys["CB Pro"]["KEY"]
            SECRET = apiKeys["CB Pro"]["SECRET"]
            PASSPHRASE = apiKeys["CB Pro"]["PASSPHRASE"]
            walletClients["CB Pro"] = CoinbaseProClient(KEY, SECRET, PASSPHRASE, self.ui)

        # Kucoin has PASSPHRASE
        if "Kucoin" in apiKeys and "KEY" in apiKeys["Kucoin"] and "SECRET" in apiKeys["Kucoin"] and "PASSPHRASE" in apiKeys["Kucoin"]:
            KEY = apiKeys["Kucoin"]["KEY"]
            SECRET = apiKeys["Kucoin"]["SECRET"]
            PASSPHRASE = apiKeys["Kucoin"]["PASSPHRASE"]
            walletClients["Kucoin"] = KucoinClient(KEY, SECRET, PASSPHRASE)

        # Neon must have wallet address to be initialized
        if "Neon" in apiKeys and "ADDRESS" in apiKeys["Neon"]:
            ADDRESS = apiKeys["Neon"]["ADDRESS"]
            walletClients["Neon"] = NeonClient(ADDRESS)
            self._enableButton(self.ui.pushButton_loadNeonCsv)

        # Other and trezor must be set to active to be initialized
        wallets = {"Trezor": TrezorClient, "Other": DummyClient, "Fiat": DummyClient}
        for name, client in wallets.items():
            if name in apiKeys and "active" in apiKeys[name] and apiKeys[name]["active"] is True:
                walletClients[name] = client()
                if name == "Other" and "cryptoDotComActive" in apiKeys["Other"] and apiKeys["Other"]["cryptoDotComActive"] is True:
                    self._enableButton(self.ui.pushButton_loadCryptoComCsv)
                elif name == "Trezor":
                    self._enableButton(self.ui.pushButton_loadTrezorCsv)

        return walletClients

    def _initCryptoCompare(self, apiKeys):
        """ initialize CryptoCompare client with its api key"""
        if "CryptoCompare" in apiKeys and "KEY1" in apiKeys["CryptoCompare"]:
            KEY1 = apiKeys["CryptoCompare"]["KEY1"]
            return CryptoCompareClient(KEY1)

    def _enableButton(self, button):
        """ enable pushButton and set its background color to a better one """
        button.setEnabled(True)
        button.setStyleSheet(styles.CSV_BUTTON_ENABLED)

    def _tabChanged(self, tabIdx):
        """ main tab in RLE_Input_Determination plugin was changed """
        for tab in self.tabs.values():
            if self.ui.tabWidget_main.tabText(tabIdx) == tab.humanName:
                try:
                    tab.tabGotFocus()
                    break
                except AttributeError:
                    pass    # not all tabs have to have a tabGotFocus method

####################################################################################################################################
#        ____        _ _         _____                 _
#       |  _ \  __ _(_) |_   _  | ____|_   _____ _ __ | |_ ___
#       | | | |/ _` | | | | | | |  _| \ \ / / _ \ '_ \| __/ __|
#       | |_| | (_| | | | |_| | | |___ \ V /  __/ | | | |_\__ \
#       |____/ \__,_|_|_|\__, | |_____| \_/ \___|_| |_|\__|___/
#                        |___/
####################################################################################################################################

    def _initDailyEvents(self):
        """ Initialize all components needed to enable daily events
            start 5 minute timer, connect events to slots and so on"""
        # load events from file
        common.dailyEvents = self._loadDailyEvents()
        # starts timer which timeouts every 5 minutes
        dailyEvents5mTimer = QTimer(self)
        dailyEvents5mTimer.timeout.connect(self._dailyTimeEvents)
        dailyEvents5mTimer.start(5 * 1000)
        # connect push button to manually trigger daily report notification
        self.ui.pushButton_sendDailyReport.clicked.connect(self._sendDailyReport)

    def _dailyTimeEvents(self):
        """ Triggered every 5 minutes
            Triggers various actions based on real time of the day """
        minutesSinceMidnights = datetime.now().hour * 60 + datetime.now().minute

        for event in common.dailyEvents:
            if event["triggered"] is False:
                if minutesSinceMidnights >= event["hours"] * 60 + event["minutes"]:
                    if "tab" in event:
                        getattr(self.tabs[event["tab"]], event["function"])()
                    elif "wallet" in event:
                        getattr(self.walletClients[event["wallet"]], event["function"])()
                    else:
                        getattr(self, event["function"])()

                    event["triggered"] = True

        self._saveDailyEvents(common.dailyEvents)

    def _resetDailyEvents(self):
        """ resets the triggered flag in all dailyEvents, reset daily transactions """
        for event in common.dailyEvents:
            if "triggered" in event and event["triggered"] is False:
                event["triggered"] = False

        # reset daily transactions
        common.dailyTransactions = []

    def _loadDailyEvents(self):
        """ loads dailyEvents data from json file
            sets all triggered flags (some events should be done right away, some not) """
        try:
            with open(common.PATH + "/user/safefiles/dailyEvents.json", "r", encoding="utf-8") as loadedFile:
                events = json.load(loadedFile)
                for event in events:
                    if "triggered" in event and "startupState" in event:
                        event["triggered"] = event["startupState"]
            return events
        except FileNotFoundError:
            print("dailyEvents.json file not found")
            return []
        except json.JSONDecodeError as e:
            print("Problem reading dailyEvents.json: " + str(e))
            return []

    def _saveDailyEvents(self, dailyEvents):
        """ saves dailyEvents to json file"""
        try:
            with open(common.PATH + "/user/safefiles/dailyEvents.json", "w", encoding="utf-8") as savedFile:
                # dump dictionary to the chosen file
                json.dump(dailyEvents, savedFile, indent=4)
        except PermissionError:
            print("Problem writting in dailyEvents.json, permission error")

    def _sendDailyReport(self):
        """ sends daily report as a notification (primary email) """
        # Portfolio values
        dailyReportStr = "Portfolio values:\n"
        for base in common.bases["options"]:
            dailyReportStr += "Portfolio value ({}) \tmin: {}\tmax: {}\n".format(
                base,
                reasonableStr(common.portfolioTotals["portfolioValueDailyLow"][base]),
                reasonableStr(common.portfolioTotals["portfolioValueDailyHigh"][base]),
            )

        # new transactions
        dailyReportStr += "\nNew transactions:\n"
        for transactionStr in common.dailyTransactions:
            dailyReportStr += transactionStr

        # Reported errors
        dailyReportStr += "\nReported errors:\n"
        for error in common.notReportedErrors:
            errorStr = str(error["time"]) + ": " + error["title"] + "\n " + error["text"]
            dailyReportStr += errorStr

        sendMailNotification(self.yagMail, "CryptoKarMa daily report", dailyReportStr)

####################################################################################################################################
#          _         _        ____       __               _        ___                 _   _  __ _           _   _
#         / \  _   _| |_ ___ |  _ \ ___ / _|_ __ ___  ___| |__    ( _ )    _ __   ___ | |_(_)/ _(_) ___ __ _| |_(_) ___  _ __  ___
#        / _ \| | | | __/ _ \| |_) / _ \ |_| '__/ _ \/ __| '_ \   / _ \/\ | '_ \ / _ \| __| | |_| |/ __/ _` | __| |/ _ \| '_ \/ __|
#       / ___ \ |_| | || (_) |  _ <  __/  _| | |  __/\__ \ | | | | (_>  < | | | | (_) | |_| |  _| | (_| (_| | |_| | (_) | | | \__ \
#      /_/   \_\__,_|\__\___/|_| \_\___|_| |_|  \___||___/_| |_|  \___/\/ |_| |_|\___/ \__|_|_| |_|\___\__,_|\__|_|\___/|_| |_|___/
#
####################################################################################################################################

    def _initNotifications(self, apiKeys):
        """ Initializes autoRefresh functionality and
            self.yagMail client with user data """
        # there must be 2 email addresses and password defined in apiKeys.json
        if ("notifications" in apiKeys and "user" in apiKeys["notifications"] and "password" in apiKeys["notifications"]
                and "target" in apiKeys["notifications"]
                and "@" in apiKeys["notifications"]["user"] and "." in apiKeys["notifications"]["user"]
                and "@" in apiKeys["notifications"]["target"] and "." in apiKeys["notifications"]["target"]):
            common.notificationTargetMail = apiKeys["notifications"]["target"]
            return yagmail.SMTP(user=apiKeys["notifications"]["user"], password=apiKeys["notifications"]["password"])
        else:
            return None

    def _initAutoRefresh(self):
        """ Initializes autoRefresh functionality """
        # initialize timer for auto refreshing wallets and Values of My portfolio
        self.autoRefreshTimer = QTimer()
        self.autoRefreshTimer.timeout.connect(self._autoRefreshActions)

        # connect autoRefresh start button to its slot
        self.ui.pushButton_startStopAutoSync.clicked.connect(
            lambda _, button=self.ui.pushButton_startStopAutoSync: self._startStopAutoRefreshClicked(button))

    def _startStopAutoRefreshClicked(self, button):
        """ Start or stop my portfolio auto refreshing based on button state """
        if button.text() == "Start":
            # set button to Stop and start auto refresh
            button.setText("Stop")
            button.setStyleSheet(styles.STOP_REFRESH)
            # start the timer based on spinBox time value
            time = self.ui.spinBox_autoRefresh.value() * 60_000     # minutes to ms
            self.autoRefreshTimer.start(time)
        else:
            # set button to Start and stop auto refresh
            button.setText("Start")
            button.setStyleSheet(styles.START_REFRESH)
            # stops refreshing
            self.autoRefreshTimer.stop()

    @common.timing
    def _autoRefreshActions(self):
        """ Trigger all actions that should be done in autoRefresh
            refresh portfolio balances, new transactions, open orders... """
        # for these repeated actions, showing of messageBoxes is disabled
        common.showMessageBoxes = False

        self.tabs["myPortfolio"].refreshAllClicked()
        self.tabs["history"].refreshAllHistory()
        self.tabs["orders"].refreshAllClicked()

        print("Number of errors since launch of the app: " + str(common.nrOfErrorFromAppStart))

        common.showMessageBoxes = True
